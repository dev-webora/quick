<?php

namespace Frame\Quick\Model\Config\Source;

class MappedMagentoAttributes implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;
    private $_serialize;
    private $_unserialized_map;
    private $_helper;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Magento\Framework\Serialize\Serializer\Json $serialize,
        \Frame\Quick\Helper\Data $helpers
    )
    {
        //parent::__construct($context);
        $this->_attributeFactory = $attributeFactory;
        $this->_serialize = $serialize;
        $this->_helper = $helpers;

        $map_configs = $this->_helper->getMappingsConfig();
        try {
            $this->_unserialized_map = $this->_serialize->unserialize($map_configs);
        }catch(\Exception $e){
            $this->_unserialized_map = NULL;
        }
    }

    public function toOptionArray()
    {

        $attributi_mappati = array();
        $arr = array();
        if($this->_unserialized_map) {
            foreach ($this->_unserialized_map as $key => $attributes) {
                $attributi_mappati[] = $attributes['activation_attribute'];
            }

            $attributeInfo = $this->_attributeFactory->getCollection();
            foreach ($attributeInfo as $attributes) {
                $attributeId = $attributes->getAttributeCode();
                if (in_array($attributeId, $attributi_mappati)) {
                    $attributeName = $attributes->getStoreLabel();
                    $arr[] = ['label' => $attributeName, 'value' => $attributeId];
                }
            }
            ksort($arr);
        }
        return $arr;
    }
}
