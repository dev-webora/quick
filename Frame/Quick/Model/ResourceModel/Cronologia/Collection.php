<?php
namespace Frame\Quick\Model\ResourceModel\Cronologia;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init("Frame\Quick\Model\Cronologia", "Frame\Quick\Model\ResourceModel\Cronologia");
    }

}