<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

use \Frame\Quick\Model\Config\Source\FieldsPrices;

class ActivationPrices extends \Magento\Framework\View\Element\Html\Select
{

    protected $_attributes_model;


    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        FieldsPrices $attributes_model,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_attributes_model = $attributes_model;

    }

    /**
     * @param string $value
     * @return Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Parse to html.
     *
     * @return mixed
     */
    public function _toHtml()
    {

        if (!$this->getOptions()) {
            $attributes =  $this->_attributes_model->toOptionArray();

            foreach ($attributes as $code => $label) {
                $this->addOption($code, $label);
            }
        }
        $this->setExtraParams('style="width:150px;"');

        return parent::_toHtml();
    }
}