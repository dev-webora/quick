<?php

namespace Frame\Quick\Model\ResourceModel\Logger;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init("Frame\Quick\Model\Logger", "Frame\Quick\Model\ResourceModel\Logger");
    }

}