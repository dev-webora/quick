<?php
namespace Frame\Quick\Model\Config\Source;

class OutletLevel implements \Magento\Framework\Option\ArrayInterface
{
    private $productRepository;
    private $scopeConfig;
    private $taxCalculation;

    public function __construct() {

    }
    public function toOptionArray()
    {
        $arr =  [
            ['value' => '1', 'label' => __('Categoria Outlet come livello 1')],
            ['value' => '2', 'label' => __('Categoria Outlet come livello 2')]
        ];
        ksort($arr);
        return $arr;
    }
}