<?php

namespace Frame\Quick\Model\Config\Source;

class ImportType implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory) {
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr =  [
            ['value' => 'singletable', 'label' => __('Tabella Prodotti Singola')],
            ['value' => 'multitable', 'label' => __('MultiTabella - References/Inventory')]
        ];
        return $arr;
    }
}