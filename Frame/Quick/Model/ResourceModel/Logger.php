<?php


namespace Frame\Quick\Model\ResourceModel;
use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Logger extends \Magento\Rule\Model\ResourceModel\AbstractResource{


    protected  function _construct()
    {
        $this->_init('frame_quick_logger', 'id');
    }

}