<?php

namespace Frame\Quick\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const IMPORT_MODE_CSV_FILE = 'csv_file';
    const IMPORT_MODE_JSON_FILE = 'json_file';
    const IMPORT_MODE_CSV_API = 'csv_api';
    const IMPORT_MODE_JSON_API = 'json_api';

    const XML_PATH_QUICK = 'quick/';
    protected $_storeManagerInterface;
    protected $_writer_interface;

    public function __construct(
        Context $context,
        \Magento\Framework\App\Config\Storage\WriterInterface $writerInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
    ) {
        parent::__construct($context);
        $this->_writer_interface = $writerInterface;
        $this->_storeManagerInterface = $storeManagerInterface;
    }

    protected function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function setModuleConfig($group_id, $field_id, $value)
    {
        $this->_writer_interface->save(self::XML_PATH_QUICK . $group_id . '/' . $field_id, $value);
    }

    public function getModuleConfig($group_id, $field_id, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_QUICK . $group_id . '/' . $field_id, $storeId);
    }

    public function getMappingsConfig($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_QUICK . 'descriptionfields/mappings', $storeId);
    }

    public function getConfigurableAttributeCode($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_QUICK . 'general/configurables', $storeId);
    }

    public function getCategoryMappingsConfig($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_QUICK . 'categorymapping/mappingscat', $storeId);
    }

    public function getStoreViews($withDefault=true)
    {
        $stores = $this->_storeManagerInterface->getStores($withDefault, false);
        $stores_array = [];

        foreach ($stores as $key => $store) {
            if ($key != 1) {
                if ($withDefault && $store->getCode() != 'default') {
                    $stores_array[$key] = $store->getName();
                } elseif (!$withDefault && $store->getCode() != 'default') {
                    $stores_array[$key] = $store->getName();
                }
            }
        }
        return $stores_array;
    }

    public function getFile($tableType)
    {
        $importMode = $this->getModuleConfig('api', 'import_mode');
        $fileType = 'csv';

        if ($importMode == self::IMPORT_MODE_JSON_FILE) {
            $fileType = 'json';
        }

        $fileContent = file_get_contents(BP . "/{$tableType}.{$fileType}");

        if ($importMode == self::IMPORT_MODE_JSON_FILE) {
            $fileContent = preg_replace('/[[:cntrl:]]/', '', $fileContent);
            $productArray = json_decode($fileContent, true);
        } else {
            $productArray = $this->csvToArray($fileContent);
        }

        return $productArray;
    }

    public function csvToArray(string $content, $delimiter = ';'): array
    {
        $header = null;
        $data = [];
        $csv = explode("\n", $content);

//        var_dump($content);
//        die;

        for ($i=0; $i < count($csv); $i++) {
            if (!$header) {
                $header = explode($delimiter, $csv[$i]);
                if (strlen($header[(count($header)-1)]) <= 1) {
                    unset($header[(count($header)-1)]);
                }
            } else {
                $riga = str_replace("\\r", "", $csv[$i]);
                $riga = str_replace("\\n", "\n", $riga);
                $riga = preg_replace('#([^("|;)]{1}|\)|\()\;([^("|;)]{1}|\)|\()#i', "$1,$2", $riga);
                $row = str_getcsv($riga, $delimiter);

                if (count($header) != count($row)) {
                    unset($row[count($header)]);
                }

                if (count($row) == 1) {
                    continue;
                }
                $data[] = array_combine($header, $row);
            }
        }
        if ($data[count($data)-1] == false) {
            unset($data[count($data)-1]);
        }
        return $data;
    }

}
