<?php

namespace Frame\Quick\Observer;

use Frame\Quick\Model\Export\Export as ExportAbstract;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection as OptionCollection;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use Frame\Quick\Model\ExportOrderQueueFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\Response\RedirectInterface;
use \Magento\Framework\App\Response\Http\FileFactory;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface;

class Export extends ExportAbstract implements ObserverInterface
{
	protected $_logger;

	protected $_exportOrderQueue;

	public function __construct(
		Context $context,
		Filter $filter,
		CollectionFactory $collectionFactory,
		OrderRepositoryInterface $rep,
		FileFactory $fileFactory,
		ScopeConfigInterface $scopeConfig,
		RegionFactory $region,
		InvoiceSender $invoiceSender,
        \Frame\Quick\Helper\Data $helpers,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
		ExportOrderQueueFactory $exportOrderQueue,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Frame\Quick\Model\Export\ExportCancel $exportCancel,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
	){
		parent::__construct($context,
            $collectionFactory,
            $helpers,
            $cronologiaFactory,
            $messageManager,
            $redirect,
            $productRepository,
            $exportOrderQueue,
            $groupRepository,
            $resourceConnection,
            $orderModel,
            $moduleManager,
            $objectManager);
	}


	public function execute(Observer $observer)
	{
		$order = $observer->getEvent()->getOrder();
        $this->quickOrderbyMagentoOrder($order, parent::TYPE_EXPORT);
		return $this;
	}
}
