<?php
namespace Frame\Quick\Block\Widget\Grid\Column\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Framework\Registry;

class Createdat extends AbstractRenderer
{
    protected $registry;
    protected $attributeFactory;

    public function __construct(
        Registry $registry,
        AttributeFactory $attributeFactory,
        Context $context,
        array $data = array()
    )
    {
        $this->attributeFactory = $attributeFactory;
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function _getValue(\Magento\Framework\DataObject $row)
    {
        $value =  parent::_getValue($row);
        $value = date("d/m/Y H:i:s", strtotime($value)+120*60);
        return $value;
    }
}