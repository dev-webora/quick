<?php
namespace Frame\Quick\Model\Import;

use \Magento\Framework\Exception\NoSuchEntityException;

class Product
{
	// product repository-factory
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	// to add attribute values
	protected $_attributeRepository;
    //protected $_attributeValues;
    protected $_tableFactory;
    protected $_attributeOptionManagement;
    protected $_optionLabelFactory;
    protected $_optionFactory;
	//helper funciotns
    protected $_helpers;
	//mappings array
	protected $_mappings;
	protected $_update_attributes;
	//register
	protected $_coreRegistry;
	//serialize
	protected $_serialize;
	//categorie
	protected $_categoryFactory;
	protected $_categoryRep;
	protected $_categoryModel;
	protected $_catLink;
	protected $categoryLinkRepository;
	//transactions
	protected $_saveTransaction;
	protected $_transactionFactory;
	// index
	protected $_indexManage;
	//flag import totale
	protected $_force_total;
	//directory handle
	protected $_directoryList;
	protected $_file;
	
	public function __construct(
	// product repository-factory
	\Magento\Catalog\Api\ProductRepositoryInterface $productRep, 
	\Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
	\Magento\Catalog\Model\ResourceModel\Product $productResource,
	//attribute values factory
	\Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
	\Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
	\Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
	\Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
	\Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
	//Helper functions
	\Frame\Quick\Helper\Data $helpers,
	// register
	\Magento\Framework\Registry $coreRegistry,
	//json serialize
	\Magento\Framework\Serialize\Serializer\Json $serialize,
	//Cateogries
	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
	\Magento\Catalog\Api\CategoryRepositoryInterface $categoryRep,
	\Magento\Catalog\Model\Category $categoriModel,
	\Magento\Catalog\Api\CategoryLinkManagementInterface $catLink,
	//\Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $catLink
	//Transactions 
	\Magento\Framework\DB\TransactionFactory $transactionFactory,
	//index
	\Magento\Framework\Indexer\IndexerRegistry $indexManage,
	//directory handle
	\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
	\Magento\Framework\Filesystem\Io\File $file
	){
		
		// product repository-factory
		$this->_productRep = $productRep;
		$this->_productFactory = $productFactory;
		$this->_productResource = $productResource;
		
		// to add attribute values
		$this->_attributeRepository = $attributeRepository;
        $this->_tableFactory = $tableFactory;
        $this->_attributeOptionManagement = $attributeOptionManagement;
        $this->_optionLabelFactory = $optionLabelFactory;
        $this->_optionFactory = $optionFactory;
		//helpers
        $this->_helpers = $helpers;
		//register
		$this->_coreRegistry = $coreRegistry;
		//serialize
		 $this->_serialize = $serialize;
		 //categories
		 $this->_categoryFactory = $categoryFactory;
		 $this->_categoryRep = $categoryRep;
		 $this->_categoryModel = $categoriModel;
		 $this->_catLink = $catLink;
		 //transactions
		 //$this->_saveTransaction = $transactionFactory->create();
		 $this->_transactionFactory = $transactionFactory;
		 //index
		 $this->_indexManage = $indexManage;
		 //directory handle
		 $this->_directoryList = $directoryList;
		 $this->_file = $file;
	}

	public function runImport($products_array,$force_total)
	{
		$this->_force_total = $force_total;
		
		//disable indexing realtime
		$indexerIds = array(
			'catalog_category_product',
			'catalog_product_category',
			'catalog_product_price',
			'catalog_product_attribute',
			'cataloginventory_stock',
			'catalogrule_rule',
			'catalogrule_product',
			'catalogsearch_fulltext',
		);
		
		foreach ($indexerIds as $indexerId) {
			$model = $this->_indexManage->get($indexerId);
			$model->setScheduled(true);
		}
		
		
		
		$count = 0;
		$prev = null;
		$imported = array();
		$imported_ids = array();
		$simples_sku_array = array();
		$availability = 0;
		//$sizeAttrId = $product->getResource()->getAttribute('size')->getId();
		//$sizeAttrId =  $this->getAttribute('size')->getAttributeId();
		$sizeAttrId = $this->getAttributeId($this->_helpers->getConfigurableAttributeCode());
		
		$map_configs = $this->_helpers->getMappingsConfig();
		$unserializedata = $this->_serialize->unserialize($map_configs);
		//var_dump($unserializedata);	
        foreach($unserializedata as $key => $map)
        {
            $this->_mappings[$map['name']] = $map['activation_attribute'];
			$this->_update_attributes[$map['name']] = $map['update'];
        }

			
		
		//$campi = array();
		
		/*foreach($this->_mappings as $quickc => $magc){
			$campi[] = $quickc;
		}*/
		$this->_saveTransaction = $this->_transactionFactory->create();
		//$countdown = 100;
		$f = fopen("./sku_log.log","wb");
		foreach($products_array as $product_api){
			/*if ($count > 19)
				break;*/
			
			try {
				$product = $this->_productRep->get($product_api->ID_VARIANTE);
				$isNew = false;
			} catch (NoSuchEntityException $e) {
				$product = $this->_productFactory->create();
				$product->setSku($product_api->ID_VARIANTE);
				$isNew = true;
			}
				

			if ( $prev != null && $prev->CODICE !== $product_api->CODICE ) {
				//echo "<br> CONFIGURABILE : ".$prev->CODICE;
				// create configurable
				//if ($isNew == true)
				//$sizeAttrId = $product->getResource()->getAttribute('size')->getId();
				//$configurableProduct = $this->_productFactory->create();
				//$countdown = $countdown - 1;			
				$this->createConfigurable($prev,$imported_ids,array($sizeAttrId),$availability,$photos,$category_ids);
				
				//$this->_saveTransaction->save();
				
				//if($isNew || $this->_force_total)
				//$this->_catLink->assignProductToCategories($prev->CODICE, $category_ids);// category_ids ha ancora il valore calcolato per il semplice precedente
				
				/*foreach($imported_ids as $variantsku)
					$this->_catLink->assignProductToCategories($variantsku, $category_ids);*/
				
				/*if($countdown == 0){
					$this->_saveTransaction->save();
					$this->_saveTransaction = $this->_transactionFactory->create();
				}*/
				//$this->_saveTransaction->save();
				$this->_saveTransaction = $this->_transactionFactory->create();
				$imported_ids = array();
				$imported[] = $prev->CODICE;
				$prev = null;
				$simples_sku_array = array();
				$availability = 0;
				$category_ids = array();
				
				
			}
			
			//$this->_productResource->saveAttribute($product, 'sku');
			//$product->setName($product_api->NOME); // Name of Product
			$product->setStoreId(0); // Name of Product
			//$product->setDescription($product_api->DESCRIZIONE_LUNGA);
			$product->setAttributeSetId(4); // Attribute set id
			$product->setStatus(($product_api->DISPO != 0) ? 1 : 0); // Status on product enabled/ disabled 1/0
			$product->setWeight(1); // weight of product
			$product->setVisibility(1); // visibilty of product (catalog / search / catalog, search / Not exclude individually)
			$product->setTaxClassId(0); // Tax class id
			$product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
			$product->setStockData(
									array(
										'use_config_manage_stock' => 0,
										'manage_stock' => 1,
										'is_in_stock' => 1,
										'qty' => $product_api->DISPO
									)
								);
								
			$sizeid = $this->addAttributeValue('size', $product_api->TAGLIA);
			//$sizeid = $this->addValue($product_api->TAGLIA);
			$product->setSize($sizeid);
			
			
			if( stripos($product_api->ID_VARIANTE,"½") !== false)
				$urlkey = $product_api->NOME."-".$product_api->ID_VARIANTE."-05";
			else
				$urlkey = $product_api->NOME."-".$product_api->ID_VARIANTE;
			
			//$product->setUrlKey($product_api->NOME."-".$product_api->ID_VARIANTE);
			$product->setUrlKey($urlkey);
			
			
			 
			foreach($this->_mappings as $campo_quick => $campo_mag){
				if($this->getAttributeType($campo_mag) == 'select')
					$val = $this->addAttributeValue($campo_mag, $product_api->$campo_quick);
				else 
					$val = $product_api->$campo_quick;
				if(!empty($val) && ($this->_update_attributes[$campo_quick] || $isNew == true || $this->_force_total == true))
					$product->setData($campo_mag,$val);
			}
			
			
			
			$photos = array();
			/*for($j=0;$j<8;$j++){
				$field = 'FOTO'.$j;
				if(!empty($product_api->$field))
					$photos[$field] = "http://apifantasia.teknosis.link:4567/foto?".urlencode($product_api->$field);
					//continue;
			} 
			
			if($isNew == true){
				if(count($photos) > 0 )
					$this->importImages($product,$photos,false);
			}*/
			
			$product->setIsMassupdate(true);
            $product->setExcludeUrlRewrite(true);
			
			$simples_sku_array[] = $product_api->ID_VARIANTE;
			if ($product_api->DISPO > 0) {
					$availability = 1;
				}
			
			$imported[] = $product_api->ID_VARIANTE;
			//$this->_productRep->save($product);
			//$product->save();
			$categoryPath = $product_api->MADRE."/".$product_api->MODELLO;
			$category_ids = $this->createCategory($categoryPath);
			
			$product->setCategoryIds($category_ids);
			
			$this->_saveTransaction->addObject($product);
			//$imported_ids[] = $product->getId();
			//$this->_productRep->save($product);
			$imported_ids[] = $product_api->ID_VARIANTE;
			
			fwrite($f,"sku: ".$product_api->ID_VARIANTE."\n");
					
			$prev = $product_api;
			$count++;
			
			//$categoryPath = $product_api->MADRE."/".$product_api->GR_MODELLO."/".$product_api->MODELLO;
			
			
			/*foreach($category_ids as $categoryId)
			{
				$categoryProductLink = $this->_catLink->create();
				$categoryProductLink->setSku($product_api->ID_VARIANTE);
				$categoryProductLink->setCategoryId($categoryId);
				$categoryProductLink->setPosition(0);		
				$this->getCategoryLinkRepository()->save($categoryProductLink);
			}*/
			
			//$this->_catLink->assignProductToCategories($product_api->ID_VARIANTE, $category_ids);
			
		}
	   
	    if ( $prev !== null ) {
		   
		   $this->createConfigurable($prev,$imported_ids,array($sizeAttrId),$availability,$photos,$category_ids);
		   
	   }
		$this->_saveTransaction->save();
		fclose($f);
		return "Product Imported: => ".implode(" ; ",$imported). "<br><br><br> -- Number of products retrieved: ".count($products_array);
		
	}
		
		
		
	public function importImages(&$product,$imagesUrl, $exclude){
		$imageType = [];
		/** @var string $tmpDir */
        $tmpDir = $this->getMediaDirTmpDir();
		//$tmpDir= "/home/313626.cloudwaysapps.com/ftrhjvbzms/public_html/magento/pub/media/tmp/";
        /** create folder if it is not exists */
        $this->_file->checkAndCreateFolder($tmpDir);
        /** @var string $newFileName */
		foreach($imagesUrl as $key => $imageUrl){
			//$newFileName = $tmpDir.substr($imageUrl, strpos($imageUrl,"?")+1);
			$image_name = substr($imageUrl, strpos($imageUrl,"?")+1);
			$image_name_array = str_split($image_name);
			//$newFileName = "/home/313626.cloudwaysapps.com/ftrhjvbzms/public_html/magento/pub/media/catalog/product/".$image_name_array[0]."/".$image_name_array[1]."/".$image_name;
			$newFileName = $tmpDir.substr($imageUrl, strpos($imageUrl,"?")+1);
			
			/** read file from URL and copy it to the new destination */
			//if(!file_exists($newFileName)){
			$result = $this->_file->read($imageUrl, $newFileName);
				//echo "\nqui";
			//}
			//else{
				//$result = true;
				//echo "\nqui false";
			//}
			//$result = file_put_contents($newFileName, file_get_contents($imageUrl));
			//$result = TRUE;
			//if ($result !== FALSE) {
			if ($result) {
				/** add saved file to the $product gallery */
				
				if(strtolower(trim($key)) == 'foto1')
					try {
						$product->addImageToMediaGallery($newFileName, array('image', 'small_image', 'thumbnail'), false, false);
						unlink($newFileName);
						//echo "\nqua 0";
					} catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					}
				else
					try {
						$product->addImageToMediaGallery($newFileName, $imageType, false, false); 
						unlink($newFileName);
						//echo "\nqua 1";
					} catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					}
			}
			//print($newFileName); 
			//echo "\n";
			
		}
	}
	
	protected function getMediaDirTmpDir()
    {
        return $this->_directoryList->getPath($this->_directoryList::MEDIA) . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
    }	
		
		
		 
		
		
	public function createCategory($categoryPath){
		//echo $categoryPath."-TEST\n";
		$cats = explode("/",$categoryPath);
		$ids = array();
		foreach($cats as $level => $cat)
		{
			if($level == 0)
				$parentId = \Magento\Catalog\Model\Category::TREE_ROOT_ID;
			else
				$parentId = $ids[$level-1];
			
			$parentCategory = $this->_categoryModel->load($parentId);
			
			$category = $this->_categoryFactory->create();
			//Check exist category
			$cate = $category->getCollection()
					->addAttributeToFilter('name',$cat)
					->addAttributeToFilter('parent_id',['eq'=>$parentId])
					->getFirstItem();
			
			if($cate->getId() == null) {
				
				$category->setPath($parentCategory->getPath())
					->setParentId($parentId)
					->setName($cat)
					->setUrlKey($categoryPath)
					->setIsActive(true);
				$category->save();
				$ids[] = $category->getId();
			}else{
				$ids[] = $cate->getId();
			}
						
		}		
		return $ids;
	}
	
	
	
	
	public function createConfigurable($productData,$imported_ids,$ConfAtrributeIds,$availability,$photos,$category_ids){
		//echo "\n\nSON QUI";
		$this->_saveTransaction->save();
		
		try {
			$configurableProduct = $this->_productRep->get($productData->CODICE);
			$ConfisNew = false;
		} catch (NoSuchEntityException $e) {
			$configurableProduct = $this->_productFactory->create();
			$configurableProduct->setSku($productData->CODICE);
			$ConfisNew = true;
		}
		
		//$configurableProduct ->setName($productData->NOME); // Name of Product
		//$configurableProduct ->setDescription($productData->DESCRIZIONE_LUNGA);
		$configurableProduct ->setAttributeSetId(4); // Attribute set id
		$configurableProduct ->setStatus(($availability != 0) ? 1 : 0); // Status on product enabled/ disabled 1/0
		$configurableProduct ->setWeight(1); // weight of product
		$configurableProduct ->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not exclude individually)
		$configurableProduct ->setTaxClassId(0); // Tax class id
		$configurableProduct ->setTypeId('configurable'); // type of product (simple/virtual/downloadable/configurable)
		//$configurableProduct ->setPrice(100); // price of product
		$configurableProduct ->setStockData(
								array(
									'use_config_manage_stock' => 0,
									'manage_stock' => 1,
									'is_in_stock' => ($availability != 0) ? 1 : 0
								)
							);
		$configurableProduct ->setUrlKey($productData->NOME);
		
		foreach($this->_mappings as $campo_quick => $campo_mag){
				if($this->getAttributeType($campo_mag) == 'select')
					$val = $this->addAttributeValue($campo_mag, $productData->$campo_quick);
				else 
					$val = $productData->$campo_quick;
				if(!empty($val) && ($this->_update_attributes[$campo_quick] || $ConfisNew == true || $this->_force_total == true))
					$configurableProduct->setData($campo_mag,$val);
			}
		
		$configurableProduct->setCategoryIds($category_ids);

		
		$configurableProduct ->setIsMassupdate(true);
		$configurableProduct ->setExcludeUrlRewrite(true);				
		$configurableProduct ->getTypeInstance()->setUsedProductAttributeIds($ConfAtrributeIds, $configurableProduct); //attribute ID of attribute 'size_general' in my store
		$configurableAttributesData = $configurableProduct ->getTypeInstance()->getConfigurableAttributesAsArray($configurableProduct);
		$configurableProduct ->setCanSaveConfigurableAttributes(true);
		$configurableProduct ->setConfigurableAttributesData($configurableAttributesData);
		$associated = array();
		foreach($imported_ids as $sku)
			$associated[] = $this->_productRep->get($sku)->getId();
		//$configurableProduct->setAssociatedProductIds($imported_ids); // Assign simple product id
		$configurableProduct->setAssociatedProductIds($associated); // Assign simple product id
		$configurableProduct->setCanSaveConfigurableAttributes(true);
		//$configurableProduct ->save();
		/*if($ConfisNew == true){
			if(count($photos) > 0 )
				$this->importImages($configurableProduct,$photos,false);
		}*/
		$this->_productRep->save($configurableProduct);
		//$this->_saveTransaction->addObject($configurableProduct);
		
		
	}
	
	public function getAttribute($attributeCode)
    {
        return $this->_attributeRepository->get($attributeCode);
    }

    public function addAttributeValue($attributeCode, $label)
    {
        if (strlen($label) < 1) {
            /*throw new \Magento\Framework\Exception\LocalizedException(
                __('Label for %1 must not be empty.', $attributeCode)
            );
			*/
			return "";
        }

        // Does it already exist?
        $optionId = $this->getOptionId($attributeCode, $label);

        if (!$optionId) {
            // If no, add it.

            /** @var \Magento\Eav\Model\Entity\Attribute\OptionLabel $optionLabel */
            $optionLabel = $this->_optionLabelFactory->create();
            $optionLabel->setStoreId(0);
            $optionLabel->setLabel($label);

            $option = $this->_optionFactory->create();
			$option->setValue($label);
            $option->setLabel($label);
            //$option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $this->_attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $this->getAttribute($attributeCode)->getAttributeId(),
                $option
            );

            // Get the inserted ID. Should be returned from the installer, but it isn't.
            $optionId = $this->getOptionId($attributeCode, $label, true);
        }

        return $optionId;
    }

    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            // We have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.

            /** @var \Magento\Eav\Model\Entity\Attribute\Source\Table $sourceModel */
            $sourceModel = $this->_tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }
	
	public function getAttributeType($attributeCode)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);


        return  $attribute->getFrontendInput();
    }
	
	public function getAttributeId($attributeCode)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);


        return  $attribute->getAttributeId();
    }
	
	/*protected function isDuplicateUrlKey($urlKey, $sku, $storeId)
    {
        $result = false;
        $urlKeyHtml = $urlKey . $this->getProductUrlSuffix();
        $resource = $this->getResource();
        $select = $this->_connection->select()->from(
            ['url_rewrite' => $resource->getTable('url_rewrite')],
            ['request_path', 'store_id']
        )->joinLeft(
            ['cpe' => $resource->getTable('catalog_product_entity')],
            'cpe.entity_id = url_rewrite.entity_id'
        )->where("request_path='$urlKey' OR request_path='$urlKeyHtml'")
            ->where('store_id IN (?)', $storeId)
            ->where('cpe.sku not in (?)', $sku);
        $isDuplicate = $this->_connection->fetchAssoc(
            $select
        );
        if (!empty($isDuplicate)) {
            $result = true;
        }
        return $result;
    }*/
	
	

}
?>