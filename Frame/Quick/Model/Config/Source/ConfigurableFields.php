<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class ConfigurableFields implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Value which equal Enable for Enabledisable dropdown.
     */
    const ENABLE_VALUE = 1;

    /**
     * Value which equal Disable for Enabledisable dropdown.
     */
    const DISABLE_VALUE = 0;

    /**
     * @return array
     */
	protected $_attributeFactory;
	protected $eavTypeFactory;
	
	public function __construct(
		// \Magento\Framework\View\Element\Context $context,
		// \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeFactory
		 \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
		 \Magento\Eav\Model\Entity\Attribute $attribute,
		 \Magento\Eav\Model\Entity\TypeFactory $typeFactory
    ) {
		//parent::__construct($context);
		$this->_attributeFactory = $attributeFactory;
		$this->eavTypeFactory = $typeFactory;
    }
	 
    public function toOptionArray()
    {
       /* return [
            ['value' => self::ENABLE_VALUE, 'label' => __('Enable2')],
            ['value' => self::DISABLE_VALUE, 'label' => __('Disable2')],
        ];*/
		
		$arr = [];

        $entityType = $this->eavTypeFactory->create()->loadByCode('catalog_product');        
        $collection = $this->_attributeFactory->create()->getCollection();
        $collection->addFieldToFilter('entity_type_id', $entityType->getId());
        $collection->setOrder('attribute_code');

        /** @var Attribute $attribute */
        foreach ($collection as $attribute) {
			if(stripos($attribute->getAttributeCode(),"size") !== false || stripos($attribute->getAttributeCode(),"color") !== false || stripos($attribute->getAttributeCode(),"taglia") !== false)
				$arr[$attribute->getAttributeCode()] = $attribute->getAttributeCode();
			
        }
		ksort($arr);
		return $arr;
		
    }
}
