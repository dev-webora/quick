<?php


namespace Frame\Quick\Model\Export;

use Frame\Quick\Model\ExportOrderQueueFactory;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection as OptionCollection;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\Response\RedirectInterface;
use \Magento\Framework\App\Response\Http\FileFactory;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Customer\Api\GroupRepositoryInterface;
use Frame\Quick\Cron\ReservedQty;

abstract class Export
{

    const TYPE_EXPORT = 1;
    const TYPE_CANCEL = 2;
    const TYPE_RESERVED = 3;

    protected $_ordini_tab;
    protected $_detordini_tab;
    protected $_api;
    protected $messageManager;
    protected $_helpers;
    protected $_safe_key_export;
    protected $_safe_key;
    protected $_product_repo;
    protected $_prefisso;
    protected $_cronologia;
    protected $_cronologia_data;
    protected $collectionFactory;
    protected $rep;
    protected $scopeConfig;
    protected $_exportOrderQueue;
    protected $resultFactory;
    protected $_redirect;
    protected $_nweb_map;
    protected $_idsuper_map;
    protected $_mis_map;
    protected $_price_map;
    protected $_price_shipping_map;
    protected $_shipping_data_map;
    protected $_active;
    protected $_id_cronologia;
    protected $_enable_notifica_errori;
    protected $_notifica_mail;
    protected $_nordinePrefissoMarket;
    protected $_nweb_prefisso;
    protected $_nweb_text;

    protected $_resourceConnection;
    protected $_orderModel;

    protected $_groupRepository;
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        \Frame\Quick\Helper\Data $helpers,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        ProductRepositoryInterface $productRepository,
        ExportOrderQueueFactory $exportOrderQueue,
        GroupRepositoryInterface $groupRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        $this->resultFactory = $context->getResultFactory();
        $this->_redirect = $redirect;
        $this->collectionFactory = $collectionFactory;
        $this->_exportOrderQueue = $exportOrderQueue;
        $this->_helpers = $helpers;
        $this->_cronologia = $cronologiaFactory;
        $this->messageManager = $messageManager;
        $this->_product_repo = $productRepository;
        $this->_groupRepository = $groupRepository;
        $this->_resourceConnection = $resourceConnection;
        $this->_orderModel = $orderModel;
        $this->_moduleManager = $moduleManager;
        $this->_objectManager = $objectManager;

        //mapping
        $this->_ordini_tab =  $this->_helpers->getModuleConfig("export","ordiniweb");
        $this->_detordini_tab = $this->_helpers->getModuleConfig("export","detordiniweb");
        $this->_api = $this->_helpers->getModuleConfig("api","url");
        $this->_safe_key = $this->_helpers->getModuleConfig("api","safetykey");
        $this->_safe_key_export = $this->_helpers->getModuleConfig("export","safe_key_export");
        if($this->_safe_key_export)
            $this->_api .= "/table?safekey=".$this->_safe_key;
        $this->_prefisso = $this->_helpers->getModuleConfig("export","prefisso");
        $this->_active = $this->_helpers->getModuleConfig("export","active");
        $this->_nweb_map = $this->_helpers->getModuleConfig("export","nweb_map");
        $this->_idsuper_map = $this->_helpers->getModuleConfig("export","idsuper_map");
        $this->_mis_map = $this->_helpers->getModuleConfig("export","mis_map");
        $this->_price_map = $this->_helpers->getModuleConfig("export","price_map");
        $this->_price_shipping_map = $this->_helpers->getModuleConfig("export","price_shipping_map");
        $this->_shipping_data_map = $this->_helpers->getModuleConfig("export","shipping_data_map");
        $this->_enable_notifica_errori = $this->_helpers->getModuleConfig("notifica_errori","enable_notifica_errori");
        if($this->_enable_notifica_errori)
            $this->_notifica_mail = $this->_helpers->getModuleConfig("notifica_errori","notifica_mail");
        $this->_nweb_prefisso = $this->_helpers->getModuleConfig("export","prefisso_nweb");
        $this->_nweb_text = $this->_helpers->getModuleConfig("export","prefisso_text");
        $this->_nordinePrefissoMarket = $this->_helpers->getModuleConfig("export","prefisso_nordine_market");
    }

    //re-invio ordini falliti e inseriti nella coda
    public function elaborateOrderQueue(){
        $queueOrdersId = $this->getOrderQueue();
        $this->flushOrderQueue();
        foreach ($queueOrdersId as $order)
            $this->generateQuickOrder(array($order['id']), $order['type']);
    }

    private function getOrderQueue() {
        $exportQueue = $this->_exportOrderQueue->create();
        $collection = $exportQueue->getCollection();
        $orderIds = array();
        foreach ($collection as $order){
            $queue = $order->getData();
            $orderIds[] = array(
                "id" => $queue['magento_order_number'],
                "type" => $queue['other']
            );
        }
        return $orderIds;
    }

    //Eliminazione degli ordini in errore dalla coda
    private function flushOrderQueue(){
        $resource = $this->_resourceConnection;
        $connection = $resource->getConnection();
        $sql = "TRUNCATE `order_queue`";
        $connection->query($sql);
    }

    //observer -- automatico
    public function quickOrderbyMagentoOrder($order, $typeId) {
        if(!$this->_active){
            return false;
        }
        $this->elaborateOrderQueue();
        $marketplace_order = $this->checkMarketplaceOrder($order);
        $this->addCronologia($order, $typeId);
        $order_info =  $this->fetchMagentoOrder($order, $marketplace_order);
        $orderComponent[$this->_ordini_tab][] = $order_info;
        $NORDINE = $order_info['NORDINE'];
        $items = $order->getAllItems();
        foreach ($items as $item) {
            $prod = $this->_product_repo->get($item['sku']);
            if ($item ['product_type'] == "configurable"){
                $prices[$prod->getId()] =  "".$item->getBaseRowTotalInclTax();
            }
			if ($marketplace_order['marketplace'] != "ebay" && $item ['product_type'] == "simple" || ($marketplace_order['marketplace'] == "ebay" && $item ['product_type'] == "configurable")) {
				$codice = $item['sku'];
				$IDSUPER = $prod->getData($this->_idsuper_map);
				//$taglia = $item->getProduct()->getData($this->_mis_map);

                $taglia =  $prod->getResource()
                    ->getAttribute($this->_mis_map)
                    ->setStoreId(0)
                    ->getFrontend()
                    ->getValue($prod);

				$qty = intval($item->getQtyOrdered());

                // MOD FRANCESCO PRICES

                if ($qty > 1){
                    $tmp_price = $item->getBaseRowTotalInclTax();
                    $price = $tmp_price / $qty;
                }else if(isset($prices[$prod->getId()])){
                    $price = $prices[$prod->getId()];
                } else{
                    $price = $item->getBaseRowTotalInclTax();
                    if($price == null) $item[$this->_price_map];
                    if($price == null) $price = "".$prod->getFinalPrice();
                }

				$CODICE_ART = $IDSUPER . "-" . $taglia;

				$detordineweb = array(
					"NORDINE" => $NORDINE,
					"CODICE_ART" => $CODICE_ART
				);
				if ($typeId == 1) {
					$detordineweb["IDSUPER"] = $IDSUPER;
					$detordineweb["MIS"] = $taglia;
					$detordineweb["PREZZO"] = "".$price;
					$detordineweb["QNT_ORD"] = "{$qty}";
				} else if ($typeId == 2) {
					$detordineweb["STATO"] = "A";
				}
				$orderComponent[$this->_detordini_tab][] = $detordineweb;
			}
        }

        $orderComponent = $this->assignCouponIfExists($order, $NORDINE, $orderComponent);

        $price = $order->getBaseShippingInclTax();

        $price = $this->CashOnDelivery($order, $price);

        $CODICE_ART = "SPESE TRASPORTO";
        $IDSUPER = "8";
        $detordineweb = array(
            "NORDINE" => $NORDINE,
            "CODICE_ART" => $CODICE_ART
        );
        if ($typeId == 1) {
            $detordineweb["IDSUPER"] = $IDSUPER;
            $detordineweb["PREZZO"] = "".$price;
        } else if ($typeId == 2) {
            $detordineweb["STATO"] = "A";
        }
        $orderComponent[$this->_detordini_tab][] = $detordineweb;

        $this->makePostCall($orderComponent , $typeId , $order);

    }

    //typeid 1=>Export 2=>Cancel 3=>ReseverdQty
    public function generateQuickOrder($orderIds, $typeId) {
        if(!$this->_active){
            return false;
        }
        foreach ($orderIds as $ordine) {
            $orderComponent = array();
            $order = false;
            if($typeId != self::TYPE_RESERVED) {

                $order = $this->_orderModel->load($ordine);
                if (!$order->getIncrementId()) {
                    $order = $this->_orderModel->loadByIncrementId($ordine);
                }

                $this->addCronologia($order, $typeId);
                $marketplace_order = $this->checkMarketplaceOrder($order);
                $order_info =  $this->fetchMagentoOrder($order, $marketplace_order);
                $NORDINE = $order_info['NORDINE'];
            } else {
                $this->addCronologia($ordine, $typeId);
            }
            switch ($typeId){
                case self::TYPE_EXPORT:
                    $orderComponent[$this->_ordini_tab][] = $order_info;
                    break;
                case self::TYPE_CANCEL:
                    $orderComponent[$this->_ordini_tab][] = array("NORDINE" => $NORDINE, "STATO" => "A");
                    break;
                case self::TYPE_RESERVED:
                    $orderComponent = $this->fetchReservedQtyOrder($ordine);
                    break;
                default:
                    return false;
            }

            if($typeId != self::TYPE_RESERVED) {
                //Recupero il dettaglio dell'ordine
                $items = $order->getAllItems();
                foreach ($items as $item) {
                    $prod = $this->_product_repo->get($item['sku']);
                    if ($item ['product_type'] == "configurable"){
                        $prices[$prod->getId()] =  "".$item->getBaseRowTotalInclTax();
                    }
                    if ($marketplace_order['marketplace'] != "ebay" && $item ['product_type'] == "simple" || ($marketplace_order['marketplace'] == "ebay" && $item ['product_type'] == "configurable")) {
                        $codice = $item['sku'];
                        $IDSUPER = $prod->getData($this->_idsuper_map);
                        //$taglia = $item->getProduct()->getData($this->_mis_map);

                        $taglia =  $prod->getResource()
                            ->getAttribute($this->_mis_map)
                            ->setStoreId(0)
                            ->getFrontend()
                            ->getValue($prod);

                        $qty = intval($item->getQtyOrdered());

                        if ($qty > 1){
                            $tmp_price = $item->getBaseRowTotalInclTax();
                            $price = $tmp_price / $qty;
                        }else if(isset($prices[$prod->getId()])){
                            $price = $prices[$prod->getId()];
                        } else{
                            $price = $item->getBaseRowTotalInclTax();
                            if($price == null) $item[$this->_price_map];
                            if($price == null) $price = $prod->getFinalPrice();
                        }

                        $CODICE_ART = $IDSUPER . "-" . $taglia;

                        $detordineweb = array(
                            "NORDINE" => $NORDINE,
                            "CODICE_ART" => $CODICE_ART
                        );
                        if ($typeId == 1) {
                            $detordineweb["IDSUPER"] = $IDSUPER;
                            $detordineweb["MIS"] = $taglia;
                            $detordineweb["PREZZO"] = "" . $price;
                            $detordineweb["QNT_ORD"] = "{$qty}";
                        } else if ($typeId == 2) {
                            $detordineweb["STATO"] = "A";
                        }

                        $orderComponent[$this->_detordini_tab][] = $detordineweb;
                    }
                }

                $orderComponent = $this->assignCouponIfExists($order, $NORDINE, $orderComponent);

                $price = $order->getBaseShippingInclTax();

                $price = $this->CashOnDelivery($order, $price);

                $CODICE_ART = "SPESE TRASPORTO";
                $IDSUPER = "8";
                $detordineweb = array(
                    "NORDINE" => $NORDINE,
                    "CODICE_ART" => $CODICE_ART
                );
                if ($typeId == 1) {
                    $detordineweb["IDSUPER"] = $IDSUPER;
                    $detordineweb["PREZZO"] = "".$price;
                } else if ($typeId == 2) {
                    $detordineweb["STATO"] = "A";
                }
                $orderComponent[$this->_detordini_tab][] = $detordineweb;
            }
            $this->makePostCall($orderComponent , $typeId , $order);
        }
        return true;
    }

    protected function checkMarketplaceOrder($order){
        $resource = $this->_resourceConnection;;
        $connection = $resource->getConnection();
        $m2epro_order = $resource->getTableName('m2epro_order');
        $is_ebay = false;
        $is_amazon = false;
        $m2epro_amazon = $resource->getTableName('m2epro_amazon_order');
        $m2epro_ebay = $resource->getTableName('m2epro_ebay_order');
        $query_amazon = "SELECT `amazon_order_id` FROM `{$m2epro_amazon}` WHERE `order_id` = ".
            "(SELECT `id` FROM `{$m2epro_order}` WHERE `magento_order_id` = '".$order->getId()."' )";

        $query_ebay = "SELECT `ebay_order_id` FROM `{$m2epro_ebay}` WHERE `order_id` = ".
            "(SELECT `id` FROM `{$m2epro_order}` WHERE `magento_order_id` = '".$order->getId()."' )";


        $result = $connection->fetchAll($query_amazon);
        if (count($result) == 1){
            $is_amazon = true;
            $marketplace_order = $result[0]['amazon_order_id'];
        }
        if(!$is_amazon) {
            $result = $connection->fetchAll($query_ebay);
            if (count($result) == 1) {
                $is_ebay = true;
                $marketplace_order = $result[0]['ebay_order_id'];
            }
        }
        $return = array();
        if($is_amazon){
            $return['id'] = $marketplace_order;
            $return['marketplace'] = "amazon";
        } else if($is_ebay){
            $return['id'] = $marketplace_order;
            $return['marketplace'] = "ebay";
        } else {
            $return['id'] = $order->getIncrementId();
            $return['marketplace'] = "magento";
        }
        $return['id'] = $order->getIncrementId();
        $return['marketplace'] = "magento";
        return $return;
    }


    public function fetchMagentoOrder($order, $marketplace_order)
    {
        $groupId = $order->getCustomerGroupId();
        $marketplaceName = $this->_groupRepository->getById($groupId)->getCode();
        //Customer -- Cliente data
        $formatedShippingAddress = $order->getShippingAddress()->getData();
        $T_MODPAG = $order->getPayment()->getMethodInstance()->getTitle();
        $T_EMAIL = $formatedShippingAddress['email'];
        $SPED_NOME = ucfirst($order->getShippingAddress()->getFirstname());
        $SPED_COGN = ucfirst($order->getShippingAddress()->getLastname());
        $SPED_IND = $formatedShippingAddress['street'];
        $SPED_CAP = $formatedShippingAddress['postcode'];
        $SPED_CAP = str_replace("-",  "", $SPED_CAP);
        $SPED_CITT = $formatedShippingAddress['city'];
        $SPED_PROV = $formatedShippingAddress['region'];//not required
        $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,ñ,¡,ś,ę,ě,Č,ň,ą,ń");
        $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,n,!,s,e,e,C,n,a,n");
        $SPED_PROV = str_replace($search, $replace, $SPED_PROV);
        $SPED_NOME = str_replace($search, $replace, $SPED_NOME);
        $SPED_COGN = str_replace($search, $replace, $SPED_COGN);
        $SPED_IND = str_replace($search, $replace, $SPED_IND);
        $SPED_CITT = str_replace($search, $replace, $SPED_CITT);
        $SPED_PROV = str_replace($search, $replace, $SPED_PROV);
        $SPED_TEL = $formatedShippingAddress['telephone'];
        $SPED_TIPO = substr($order->getShippingDescription() , 0 , 10);
        $SPED_NAZ = $formatedShippingAddress['country_id'];
        $locale = new \Zend_Locale('en_GB');
        $countries = $locale->getTranslationList('Territory', $locale->getLanguage(), 2);
        $SPED_NAZ = $countries[$SPED_NAZ];

        $CODCLI = $order->getData('customer_id');
        $email = $order->getData('customer_email');
        $T_CODFISC = $order->getData('customer_taxvat');

        if ($this->_shipping_data_map) {
            $T_CLIENTE = ucfirst(strtolower($SPED_NOME)) . " " . ucfirst(strtolower($SPED_COGN));
            $T_INDIRIZ = $SPED_IND;
            $T_CITTA = $SPED_CITT;
            $T_CAP = $SPED_CAP;
            $T_PROV = $SPED_PROV;
            $T_PIVA = " ";
            $T_CODFISC = $order->getData('customer_taxvat');
        } else {

            if ($order->getCustomerIsGuest()){
                $firstname = $order->getBillingAddress()->getFirstname();
                $lastname = $order->getBillingAddress()->getLastname();
                $name = ucfirst(strtolower($firstname));
                if ($order->getBillingAddress()->getMiddlename()){
                    $name .= " " . ucfirst(strtolower($order->getBillingAddress()->getMiddlename()));
                }
                $name .=  " " . ucfirst(strtolower($lastname));
                $T_CLIENTE = $name;
            }else{
                $nome = $order->getCustomerFirstname();
                $cognome = $order->getCustomerLastname();
                $T_CLIENTE = ucfirst(strtolower($nome)) . " " . ucfirst(strtolower($cognome));
            }

            $address_data = $order->getBillingAddress()->getData();
            $T_INDIRIZ = $address_data["street"];
            $T_CITTA = $address_data["city"];
            $T_CAP = $address_data["postcode"];
            $T_CAP = str_replace("-",  "", $T_CAP);
            $T_PROV = $address_data["region"];
            $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u,ñ,¡,ś,ę,ě,Č,ň,ą,ń");
            $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u,n,!,s,e,e,C,n,a,n");
            $T_PROV = str_replace($search, $replace, $T_PROV);
            $T_CLIENTE = str_replace($search, $replace, $T_CLIENTE);
            $T_INDIRIZ = str_replace($search, $replace, $T_INDIRIZ);
            $T_CITTA = str_replace($search, $replace, $T_CITTA);
            $T_PIVA = $address_data["vat_id"];
        }
        if (empty($T_CODFISC)) $T_CODFISC = " ";
        if (empty($T_PIVA)) $T_PIVA = " ";
        switch ($this->_nweb_map) {
            case 'magento':
                $NWEB = $order->getIncrementId();
                break;
            default:
                $NWEB = $marketplace_order['id'];
                break;
        }

        $nweb_prefisso = "";
        switch ($this->_nweb_prefisso){
            case 'customer_group':
                $groupId = $order->getCustomerGroupId();
                $marketplaceName = $this->_groupRepository->getById($groupId)->getCode();
                $nweb_prefisso = $marketplaceName;
                break;
            case 'shipping':
                if(stripos($SPED_TIPO, "eBay") !== false){
                    $nweb_prefisso = "eBay_";
                } else if(stripos($SPED_TIPO, "Amazon") !== false){
                    $nweb_prefisso = "Amazon_";
                } else if(stripos($SPED_TIPO, "spartoo") !== false){
                    $nweb_prefisso = "Spartoo_";
                } else if(stripos($SPED_TIPO, "cdiscount") !== false){
                    $nweb_prefisso = "CDiscount_";
                }
                break;
            case 'text':
                $nweb_prefisso = $this->_nweb_text;
                break;
        }
        $nordine_prefisso = $this->_prefisso;
        switch ($this->_nordinePrefissoMarket){
            case 'customer_group':
                $groupId = $order->getCustomerGroupId();
                $marketplaceName = $this->_groupRepository->getById($groupId)->getCode();
                $nordine_prefisso .= $marketplaceName."_";
                break;
            case 'shipping':
                if(stripos($SPED_TIPO, "eBay") !== false){
                    $nordine_prefisso .= "eBay_";
                } else if(stripos($SPED_TIPO, "Amazon") !== false){
                    $nordine_prefisso .= "Amazon_";
                } else if(stripos($SPED_TIPO, "spartoo") !== false){
                    $nordine_prefisso .= "Spartoo_";
                } else if(stripos($SPED_TIPO, "cdiscount") !== false){
                    $nordine_prefisso .= "CDiscount_";
                }
                break;
        }
        if (!empty($nordine_prefisso)) $marketplace_order['id'] = $nordine_prefisso . $marketplace_order['id'];
        $ordineweb = array("NORDINE" => $marketplace_order['id'], "NWEB" => $nweb_prefisso.$NWEB, "DATA" => date('Y/m/d H:i:s'),
            "T_CLIENTE" => $T_CLIENTE, "T_INDIRIZZO" => $T_INDIRIZ, "T_CITTA" => $T_CITTA, "T_CAP" => $T_CAP, "T_PROV" => $T_PROV, "T_CODFISC" => $T_CODFISC, "T_PIVA" => $T_PIVA,
            "T_MODPAG" => $T_MODPAG, "T_EMAIL" => $T_EMAIL, "SPED_NOME" => $SPED_NOME, "SPED_COGNOME" => $SPED_COGN, "SPED_IND" => $SPED_IND, "SPED_CAP" => $SPED_CAP, "SPED_CITTA" => $SPED_CITT,
            "SPED_PROV" => $SPED_PROV, "SPED_TEL" => $SPED_TEL, "SPED_TIPO" => $SPED_TIPO, "SPED_NAZIONE" => $SPED_NAZ);
        if(!empty($CODCLI)) $ordineweb["CODCLIWEB"] = $CODCLI;
        return $ordineweb;
    }

    /**
     * @param $ordine
     * @param array $orderComponent
     * @return array
     */
    public function fetchReservedQtyOrder($ordine)
    {
        $orderComponent = array();
        $NORDINE = $ordine['marketplace_order_id'];
        if (!empty($this->_prefisso)) $NORDINE = $this->_prefisso . $ordine['marketplace_order_id'];
        $sku = $ordine['sku'];
        $id_super = $ordine['id_super'];
        $size = $ordine['size'];
        $qty = $ordine['qty'];
        $prezzo = $ordine['price'];
        $stato = $ordine['status'];

        $ordineweb = array("NORDINE" => $NORDINE);
        if ($stato == ReservedQty::STATE_CANCELED) {
            $ordineweb["STATO"] = "A";
        }
        $orderComponent[$this->_ordini_tab][] = $ordineweb;

        $detordineweb = array(
            "NORDINE" => $NORDINE,
            "CODICE_ART" => $sku,
            "IDSUPER" => $id_super,
            "MIS" => $size,
            "PREZZO" => "".$prezzo,
            "QNT_ORD" => $qty
        );
        if ($stato == ReservedQty::STATE_CANCELED) {
            $detordineweb["STATO"] = "A";
        }
        $orderComponent[$this->_detordini_tab][] = $detordineweb;
        return $orderComponent;
    }

    protected function makePostCall($fieldToSend , $type , $order){

        $processedItems = 0;
        $success = true;
        foreach ($fieldToSend as $table => $fields){

            if ($processedItems == 0 && $table == 'frame_detordiniweb') {
                $success = false;
                break;
            }

            foreach ($fields as $field) {
                $link = $this->_api."&NOME=".$table;
                $this->updateCronologia("JSON Export: ".json_encode($field));
                $this->updateCronologia("Link: ".$link);
                $try = 0;


                do {
                    $try++;

                    if ($try>1) {
                        sleep(1);
                    }
                    list($token, $status) = $this->execCurl($link, $field);
                    $response = json_decode($token , true);

                    $this->updateCronologia("Tentativo n ". $try);


                } while ($try<=5 && $this->isResponseNotValid($response));

                if ($this->isResponseNotValid($response) && $try>5) {

                    $success = false;
                    $this->updateCronologia("Errore : ".$token);

                    //inserisce l'ordine nella coda
                    $model = $this->_exportOrderQueue->create();
                    $row = array(
                        'magento_order_id' => (string) $order->getIncrementId(),
                        'magento_order_number' =>  (string)$order->getIncrementId(),
                        'http_error_code' => trim($status),
                        'created_at' => date('Y/m/d H:i:s'),
                        'status' => 'error '.trim($status),
                        'other' => $type
                    );

                    $model->addData($row);
                    $saveData = $model->save();
                    $this->updateCronologia("Ordine non esportato correttamente");
                    $this->errorDebug($field, $token, $status, $table);
                    break;
                } else {
                    $processedItems++;
                    $this->updateCronologia("Response: $token");

                }
            }
        }
        if ($success) {
            if ($type !== self::TYPE_RESERVED){
                $this->updateCronologia("Ordine esportato con Successo: ".$order->getIncrementId());
            }
        }

        return true;
    }

    /**
     * @param string $link
     * @param $field
     * @return array
     */
    protected function execCurl(string $link, $field): array
    {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($field));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($field))));
        $token = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array($token, $status);
    }

    /**
     * @param $response
     * @return bool
     */
    protected function isResponseNotValid($response): bool
    {
        if (!is_array($response)) {
            return true;
        }

        return $response["RESULT"] != "INSERT OK"
            && $response["RESULT"] != "EDIT OK"
            && $response["RESULT"] != "DELETE OK";
    }


    public function addCronologia($order, $typeId)
    {
        $data = date('Y/m/d H:i:s');
        if($typeId == self::TYPE_EXPORT){
            $tipo = "Export - Order";
            $contenuto = array(array("time" => $data, "messaggio" => "Starting Export Order ID: " . $order->getIncrementId()));
        } else if($typeId == self::TYPE_CANCEL){
            $tipo = "Export - Cancel Order";
            $contenuto = array(array("time" => $data, "messaggio" => "Cancel Export Order ID: " . $order->getIncrementId()));
        } else {
            $tipo = "Export - Reserved Qty";
            $contenuto = array(array("time" => $data, "messaggio" => "Riservo ".$order['qty']." quantità Sku: ".$order['sku']));
        }
        //creo cronologia
        $cronologia = $this->_cronologia->create();
        $this->_cronologia_data = array(
            "updated_at" => $data,
            "tipo" => $tipo,
            "contenuto" => json_encode($contenuto)
        );
        $cronologia->addData($this->_cronologia_data);
        $saveData = $cronologia->save();
        if ($saveData) $this->_id_cronologia = $cronologia->getId();
        else $this->_id_cronologia = null;
    }

    public function updateCronologia($messaggio)
    {
        if($this->_id_cronologia == null) return false;
        $data = date('Y/m/d H:i:s');
        $cronologia = $this->_cronologia->create()->load($this->_id_cronologia);
        $value = $cronologia->getData();
        $contenuto_old = json_decode($value['contenuto'], true);
        $contenuto_old[] = array("time" => $data, "messaggio" => $messaggio);
        $contenuto = json_encode($contenuto_old);
        $this->_cronologia_data['updated_at'] = $data;
        $this->_cronologia_data['contenuto'] = $contenuto;
        return $cronologia->addData($this->_cronologia_data)->setId($this->_id_cronologia)->save();
    }

    public function errorDebug($post, $response, $status, $tabella)
    {
        if($this->_enable_notifica_errori){
            $response = json_decode($response, true);
            if(is_array($response) && !array_key_exists("RESULT", $response) || !(strpos($response["RESULT"], 'OK') !== false) || trim($status) !== '200'){
                $headers = "Reply-To: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
                $headers .= "Return-Path: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
                $headers .= "From: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
                $headers .= "Organization: ".$_SERVER['HTTP_HOST']."\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
                mail($this->_notifica_mail, "Errore Ordine Cliente ".$_SERVER['HTTP_HOST'],
                    "Errore ordine ".$_SERVER['HTTP_HOST']."\n
                    Nome Tabella: ".$tabella."\n
                    Dati Inviati: ".json_encode($post)."\n
                    Risposta: ".json_encode($response)."\n
                    HTTP Status: ".$status, $headers);
                $this->updateCronologia("Mail Notifica Inviata");
            }
        }
    }

    /**
     * @param $order
     * @param $NORDINE
     * @param array $orderComponent
     */
    protected function assignCouponIfExists($order, $NORDINE, array $orderComponent)
    {
        if (!empty($order->getCouponCode())) {
            $discount_amount = $order->getBaseDiscountAmount();
            $discount_amount = number_format($discount_amount, 2);
            $detordineweb = array(
                "NORDINE" => $NORDINE,
                "CODICE_ART" => "SCONTO VALORE",
                "DESCRIZIONE" => empty($order->getDiscountDescription()) ? 'Sconto Generico' : $order->getDiscountDescription(),
                "QNT_ORD" => 1,
                "PREZZO" => (string)$discount_amount
            );
            $orderComponent[$this->_detordini_tab][] = $detordineweb;
        }
        return $orderComponent;
    }

    /**
     * @param $order
     * @param $price
     * @return mixed|string
     */
    protected function CashOnDelivery($order, $price)
    {
        if ($this->_moduleManager->isEnabled('Amasty_CashOnDelivery')) {
            if ($order->getPayment()->getMethod() === \Magento\OfflinePayments\Model\Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE) {
                $amastyModel = $this->_objectManager->get('\Amasty\CashOnDelivery\Model\PaymentFee');
                $fee = $amastyModel->load($order->getQuoteId(), 'quote_id');
                $price += $fee->getAmount();
                $price = number_format($price, 2);
            }
        }
        return $price;
    }
}