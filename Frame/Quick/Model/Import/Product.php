<?php

namespace Frame\Quick\Model\Import;

require_once BP . "/lib/internal/FrameImport/frame/magmiimport.php";

use Frame\Quick\Model\LoggerFactory;
use \Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Store\Model\StoreManagerInterface;
use Frame\Quick\Model\Import\Resize;

class Product
{
    // product repository-factory
    protected $_productRep;
    protected $_resource;
    protected $_connection;
    protected $_productFactory;
    protected $_productResource;
    protected $_storeManager;
    // to add attribute values
    protected $_attributeRepository;
    //protected $_attributeValues;
    protected $_tableFactory;
    protected $_attributeOptionManagement;
    protected $_optionLabelFactory;
    protected $_optionFactory;
    //helper funciotns
    protected $_helpers;
    //mappings array
    protected $_stores_array;
    protected $_unserializedata;
    protected $_mappings = array();
    protected $_mappings_prices = array();
    protected $_mappings_languages;
    protected $_is_import_multiview;
    //protected $_update_attributes;
    //register
    protected $_coreRegistry;
    //serialize
    protected $_serialize;
    //categorie
    protected $_categoryFactory;
    protected $_categoryRep;
    protected $_categoryModel;
    protected $_catLink;
    protected $categoryLinkRepository;
    protected $_enable_outlet;
    protected $_outlet_level;
    protected $_outlet_quick_field;
    protected $_outlet_quick_value;
    protected $_outlet_cat_name;
    protected $_outlet_combine_name;
    protected $_outlet_build_tree;

    //attribute set
    protected $_attribute_set;
    //transactions
    protected $_saveTransaction;
    protected $_transactionFactory;
    // index
    protected $_indexManage;
    //flag import totale
    protected $_force_total;
    //directory handle
    protected $_directoryList;
    protected $_file;
    //array of items
    protected $_items_to_import;

    protected $_dir;
    protected $_mode;


    //conf variables
    protected $_api_url_foto;
    protected $_qty_map;
    protected $_price_map;
    protected $_sku_conf_map;
    protected $_sku_simple_map;
    protected $_conf_attribute_map;
    protected $_web_map;
    protected $_photo_map;
    protected $photo_map_unserialized;
    protected $_mapping_categorie;
    protected $_web_content;
    protected $_foto_main;
    protected $_foto_small;
    protected $_foto_thumbnail;
    protected $_foto_max_width;
    protected $_foto_max_height;
    protected $_foto_custom_link_enabled;
    protected $_foto_custom_link;
    protected $_foto_quality;
    protected $_fotoToSimple;
    protected $_total_mapping;
    protected $_logging_active;
    protected $_logging_days;
    protected $_logging_attributes;
    protected $_loggerFactory;

    //cat variables
    protected $_unisex_map_configs;
    protected $_unisex_map;
    protected $_unisex_enable_configs;
    protected $_category_unserializedata;
    protected $_category_map_configs;
    protected $_category_mappings;

    //title composition
    protected $_composetitle_map_configs;
    protected $_composetitle_unserializedata;
    protected $_composetitle_mappings;

    protected $_product_collection_factory;
    protected $_traduzioni;
    protected $_eventManager;
    protected $_dizionario_enabled;
    protected $_dizionario_unserialized;

    protected $_enable_new_products;
    protected $_new_product_duration;

    protected $_cronologia;
    protected $_cronologia_id;
    protected $_cronologia_data;

    protected $_additional_prices_map;
    protected $_additional_prices_unserializedata;
    protected $_taxClass;

    protected $descriptive_fields_flag;
    protected $descriptive_fields_views_flag;
    protected $categories_flag;
    protected $photos_flag;
    protected $compose_title_flag;
    protected $_attribute;

    // Marketplace function
    protected $_marketplace_enable;
    protected $_marketplace_campo_quick;
    protected $_marketplace_allmarket_value;
    protected $_marketplace_serialized;

    public function __construct(
        // product repository-factory
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        EventManager $eventManager,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRep,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        //attribute values factory
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        //Helper functions
        \Frame\Quick\Helper\Data $helpers,
        // register
        \Magento\Framework\Registry $coreRegistry,
        //json serialize
        \Frame\Quick\Helper\Serializer $serialize,
        //Cateogries
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRep,
        \Magento\Catalog\Model\Category $categoriModel,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $catLink,
        //\Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $catLink
        //Transactions
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        //index
        \Magento\Framework\Indexer\IndexerRegistry $indexManage,
        //directory handle
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem\Io\File $file,
        LoggerFactory $loggerFactory,
        \Magento\Eav\Model\Entity\Attribute $attribute
    )
    {
        $this->_product_collection_factory = $productCollectionFactory;
        $this->_resource = $resource;
        $this->_connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->_eventManager = $eventManager;
        $this->_storeManager = $storeManager;
        // product repository-factory
        $this->_productRep = $productRep;
        $this->_productFactory = $productFactory;
        $this->_productResource = $productResource;
        $this->_cronologia = $cronologiaFactory;
        // to add attribute values
        $this->_attributeRepository = $attributeRepository;
        $this->_tableFactory = $tableFactory;
        $this->_attributeOptionManagement = $attributeOptionManagement;
        $this->_optionLabelFactory = $optionLabelFactory;
        $this->_optionFactory = $optionFactory;
        //helpers
        $this->_helpers = $helpers;
        //register
        $this->_coreRegistry = $coreRegistry;
        //serialize
        $this->_serialize = $serialize;
        //categories
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryRep = $categoryRep;
        $this->_categoryModel = $categoriModel;
        $this->_catLink = $catLink;
        //transactions
        //$this->_saveTransaction = $transactionFactory->create();
        $this->_transactionFactory = $transactionFactory;
        //index
        $this->_indexManage = $indexManage;
        //directory handle
        $this->_directoryList = $directoryList;
        $this->_file = $file;

        $this->_loggerFactory = $loggerFactory;

        $this->_items_to_import = array();

        $this->_dir = BP . "/pub/media/catalog/product/f/o/";

        $this->_mode = "";

        $this->_attribute = $attribute;

        $this->_attribute_set = $this->_helpers->getModuleConfig("general", "magattributeset");
        $this->_is_import_multiview = $this->_helpers->getModuleConfig("descriptionfields", "enable_mapping_views");
        $this->_stores_array = $this->mapStoreCode(array($this->_helpers->getStoreViews(false)))[0];
        $map_configs = $this->_helpers->getMappingsConfig();
        $this->_unserializedata = $this->mapStoreCode($this->_serialize->unserialize($map_configs));

        $this->photo_map_unserialized = $this->_serialize->unserialize($this->_helpers->getModuleConfig("descriptionfields", "foto_mapping"));
        $this->_photo_map = array();
        $this->_mapping_categorie = ($this->_helpers->getModuleConfig("categorymapping", "mappingscat"));
        $this->_traduzioni = $this->_helpers->getModuleConfig("traduzioni", "enable_translate");
        $this->_web_content = $this->_helpers->getModuleConfig("basicfields", "web_content");
        $this->_foto_main = $this->_helpers->getModuleConfig("descriptionfields", "foto_main");
        $this->_foto_small = $this->_helpers->getModuleConfig("descriptionfields", "foto_small");
        $this->_foto_thumbnail = $this->_helpers->getModuleConfig("descriptionfields", "foto_thumbnail");
        $this->_foto_max_width = $this->_helpers->getModuleConfig("descriptionfields","foto_max_width");
        $this->_foto_max_height = $this->_helpers->getModuleConfig("descriptionfields","foto_max_height");
        $this->_foto_quality = $this->_helpers->getModuleConfig("descriptionfields","foto_quality");
        $this->_foto_custom_link_enabled = $this->_helpers->getModuleConfig("descriptionfields","custom_img_link_enabled");
        if($this->_foto_custom_link_enabled)
            $this->_foto_custom_link = $this->_helpers->getModuleConfig("descriptionfields","custom_img_link");
        $this->_taxClass = $this->_helpers->getModuleConfig("descriptionfields","prod_tax_class");
        $this->_fotoToSimple = $this->_helpers->getModuleConfig("descriptionfields","simple_foto");
        $this->_logging_active = $this->_helpers->getModuleConfig("logger", "enable_logger");
        $this->_logging_days = $this->_helpers->getModuleConfig("logger", "days");
        $this->_logging_attributes = explode(",", $this->_helpers->getModuleConfig("logger", "attributes"));
        $this->_enable_outlet = $this->_helpers->getModuleConfig("categorymapping", "enableoutletcat");
        $this->_outlet_level = $this->_helpers->getModuleConfig("categorymapping", "outletcatlevel");
        $this->_outlet_quick_field = $this->_helpers->getModuleConfig("categorymapping", "outletcatquickfield");
        $this->_outlet_quick_value = $this->_helpers->getModuleConfig("categorymapping", "outletcatquickvalue");
        $this->_outlet_cat_name = $this->_helpers->getModuleConfig("categorymapping", "outletcatname");
        $this->_outlet_combine_name = $this->_helpers->getModuleConfig("categorymapping", "outletcatnamecombine");
        $this->_outlet_build_tree = $this->_helpers->getModuleConfig("categorymapping", "outletcattree");
        $this->_dizionario_enabled = $this->_helpers->getModuleConfig("dizionario", "enable");
        if ($this->_dizionario_enabled) {
            $this->_dizionario_unserialized = $this->_serialize->unserialize($this->_helpers->getModuleConfig("dizionario", "dizionario_list"));
        }
        $this->_category_map_configs = $this->_helpers->getCategoryMappingsConfig();
        $this->_category_unserializedata = $this->mapStoreCode($this->_serialize->unserialize($this->_category_map_configs));
        $this->_unisex_enable_configs = $this->_helpers->getModuleConfig("categorymapping", "enable_mapping_unisex");
        $this->_unisex_map_configs = $this->_helpers->getModuleConfig("categorymapping", "mapping_unisex");
        $this->_unisex_map = explode(',', $this->_unisex_map_configs);
        $this->_enable_new_products = $this->_helpers->getModuleConfig("new_products", "enableoption");
        $this->_new_product_duration = $this->_helpers->getModuleConfig("new_products", "time");
        $this->_composetitle_map_configs = $this->_helpers->getModuleConfig("descriptionfields", "composetitle");
        $this->_composetitle_unserializedata = $this->mapStoreCode($this->_serialize->unserialize($this->_composetitle_map_configs));
        $this->_additional_prices_map = $this->_helpers->getModuleConfig("basicfields", "additionalprices");
        $this->_additional_prices_unserializedata = $this->_serialize->unserialize($this->_additional_prices_map);
        $api_pieces['url'] = $this->_helpers->getModuleConfig("api", "url");
        $this->_api_url_foto = $api_pieces['url'] . '/foto?';
        $this->_qty_map = $this->_helpers->getModuleConfig("basicfields", "qty");
        $this->_sku_conf_map = $this->_helpers->getModuleConfig("basicfields", "skuconf");
        $this->_sku_simple_map = $this->_helpers->getModuleConfig("basicfields", "skusimple");
        $this->_price_map = $this->_helpers->getModuleConfig("basicfields", "price");
        $this->_web_map = $this->_helpers->getModuleConfig("basicfields", "web");
        $this->_conf_attribute_map = $this->_helpers->getModuleConfig("basicfields", "confattributemap");

        // MARKETPLACE FUNCTION

        $this->_marketplace_enable = $this->_helpers->getModuleConfig("marketplaces", "enable_marketplaces");
        $this->_marketplace_campo_quick = $this->_helpers->getModuleConfig("marketplaces", "marketplace_field_name");
        $this->_marketplace_allmarket_value = $this->_helpers->getModuleConfig("marketplaces", "marketplace_field_value");
        $this->_marketplace_serialized = $this->_serialize->unserialize($this->_helpers->getModuleConfig("marketplaces", "marketplace_mapping"));

    }

    public function runImport($products_array, $mode, $cronologia_id, $import_info = array("type" => "", "functions" => array()))
    {
        $this->_cronologia_id = $cronologia_id;
        if (count($products_array) <= 100) $num_prod_for_1perc = 1;
        else $num_prod_for_1perc = ceil(0.01 * count($products_array));
        $this->updatePercentage(0);
        //$f1 = fopen(BP . "/import_log.log", "a");
        $data_inizio = date("Y-m-d H:i:s");
        if (!$products_array) {
            //fwrite($f1, "[$data_inizio] Non ci sono prodotti da elaborare\n");
            //fclose($f1);
            return false;
        }
        $count = count($products_array);
        //fwrite($f1, "[$data_inizio] Product elaboration - " . $count . " products to import\n");
        if ($products_array && count($products_array) >= 1 && $products_array[0]['DATETIME'] != 'null') {

            $this->_mode = $mode;
            //$this->_force_total = $force_total;
            $data_inizio = date("Y-m-d H:i:s");
            //fwrite($f1, "[$data_inizio] Product elaboration - Disabling magento indexing\n");
            //disable indexing realtime
            $indexerIds = array(
                'catalog_category_product',
                'catalog_product_category',
                'catalog_product_price',
                'catalog_product_attribute',
                'cataloginventory_stock',
                'catalogrule_rule',
                'catalogrule_product',
                'catalogsearch_fulltext',
            );

            foreach ($indexerIds as $indexerId) {
                $model = $this->_indexManage->get($indexerId);
                $model->setScheduled(true);
            }
            $data_inizio = date("Y-m-d H:i:s");
            $inizio_import = $data_inizio;
            //fwrite($f1, "[$data_inizio] Product elaboration - End Disabling magento indexing\n");

            $count = 0;
            $prev = null;
            $imported = array();
            $imported_ids = array();
            $simples_sku_array = array();
            $availability = 0;

//            foreach ($this->_unserializedata as $key => $attributes){
//                if($attributes['update'] == 1)
//                    $this->_total_mapping[$attributes['activation_attribute']] = $attributes['name'];
//            }

            $this->getAdditionalPricesListings(); // generating $this->_mappings_prices
            //if($this->_mode !== 'update') {
            $this->descriptive_fields_flag = false;
            $this->descriptive_fields_views_flag = false;
            $this->categories_flag = false;
            $this->photos_flag = false;
            $this->compose_title_flag = false;

            switch ($import_info["type"]) {
                case "all":
                    $this->getDescriptionFieldsMappings(); // $this->_mappings, $this->_mappings_languages
                    $this->descriptive_fields_flag = true;
                    $this->descriptive_fields_views_flag = true;
                    $this->getCategoriesMappings(); // $this->_category_mappings
                    $this->categories_flag = true;
                    $this->getPhotosMappings(); // $this->_photo_map
                    $this->photos_flag = true;
                    $this->compose_title_flag = true;
                    break;
                case "selectedfields":
                    $this->getDescriptionFieldsMappings(true); // $this->_mappings, $this->_mappings_languages
                    $this->descriptive_fields_flag = true;
                    if (in_array("views", $import_info["functions"])) {
                        $this->descriptive_fields_views_flag = true;
                    }
                    if (in_array("categories", $import_info["functions"])) {
                        $this->getCategoriesMappings(); // $this->_category_mappings
                        $this->categories_flag = true;
                    }
                    if (in_array("images", $import_info["functions"])) {
                        $this->getPhotosMappings(); // $this->_category_mappings
                        $this->photos_flag = true;
                    }
                    if (in_array("title", $import_info["functions"])) {
                        $this->compose_title_flag = true;
                    }
                    break;
                case "stockprice":
                    break;

            }

            $photos = array();
            $gallery = array();

            $category_str = "";
            $curr_num = 0;
            $old_perc = 0;
            $data_inizio = date("Y-m-d H:i:s");
            //fwrite($f1, "[$data_inizio] Product elaboration - Loop on products\n");
            $this->updateCronologia("Inizio Conversione dei prodotti");

            foreach ($products_array as $product_api) {
                $curr_num++;
                $new_perc = (int)($curr_num / count($products_array) * 50);
                if ($old_perc != 0 || $new_perc != $old_perc) {
                    $this->updatePercentage($new_perc);
                    $old_perc = $new_perc;
                }

                if ($prev != null && $prev[$this->_sku_conf_map] !== $product_api[$this->_sku_conf_map]) {

                    $data_conf = $this->createConfigurable($prev, $imported_ids, $availability, $gallery, $category_str);
                    $this->_items_to_import[] = $data_conf;

                    if ($this->_mode !== 'update') {
                        if ($this->_is_import_multiview)
                            $this->generateViewImport($prev, $data_conf);
                    }
                    $imported_ids = array();
                    $imported[] = $prev[$this->_sku_conf_map];
                    $prev = null;
                    $simples_sku_array = array();
                    $availability = 0;
                }

                $item = array(
                    "attribute_set" => $this->_attribute_set,
                    "store" => "admin",
                    "sku" => $product_api[$this->_sku_simple_map],
                    "type" => "simple",
                    "visibility" => 1,
                    //"status" 					=> $status, //da sostituire in caso di import con references
                    "status" => 1 ,//(intval($product_api[$this->_qty_map]) > 0) ? 1 : 2,
                    "tax_class_id" => (int) $this->_taxClass,
                    "configurable_attributes" => "", ### for configurable "color,taglia",
                    "qty" => $product_api[$this->_qty_map],
                    "is_in_stock" => ($product_api[$this->_qty_map] > 0) ? 1 : 0, #qty > 0
                    "manage_stock" => 1,
                    "use_config_manage_stock" => 0,
                    "price" => $product_api[$this->_price_map],
                    "weight" => 1,
                    $this->_helpers->getConfigurableAttributeCode() => $product_api[$this->_conf_attribute_map],
                    "simples_skus" => ""
                );

                if (array_key_exists($this->_web_map, $product_api)) { // se c'è il campo "web" si/no nella risposta API
                    if (intval($product_api[$this->_qty_map]) > 0 && $product_api[$this->_web_map] == $this->_web_content)
                        $status = 1;
                    else
                        $status = 2;
                    $item['status'] = $status;
                }
                //$item['weight'] = 1;

                // additional prices listings map
                foreach ($this->_mappings_prices as $campo_quick => $campo_mag) {
                    if (is_array($campo_mag)) {
                        foreach ($campo_mag as $campo_magento) {
                            $item[$campo_magento] = $product_api[$campo_quick];
                        }
                    } else {
                        $item[$campo_mag] = $product_api[$campo_quick];
                    }
                }

                //if ($this->_mode !== 'update') {
                if ($this->categories_flag) {
                    $category_str = $this->importCategories($product_api);
                    $item['categories'] = $category_str;
                }
                if ($this->compose_title_flag) {
                    $title_str = $this->composeTitle($product_api);
                    $item['name'] = $title_str;
                }

                if ($this->photos_flag) {
                    $results = $this->getImagesFields($product_api);
                    $photos = $results["photos"];
                    $gallery = $results["gallery"];
                    //decomment these two lines if you want images are downloaded by the module instead of magmi
                    $this->importImages($photos, false);
                    $gallery_str = implode(";", $gallery);
                    //$gallery_str = implode(";", $photos);
                    if($this->_fotoToSimple){
                        $item['image'] = !empty($product_api[$this->_foto_main]) ? "" . $this->_dir . "foto_" . strtolower($product_api[$this->_foto_main]) : "__MAGMI_IGNORE__";
                        $item['small_image'] = !empty($product_api[$this->_foto_small]) ? "" . $this->_dir . "foto_" . strtolower($product_api[$this->_foto_small]) : "__MAGMI_IGNORE__";
                        $item['thumbnail'] = !empty($product_api[$this->_foto_thumbnail]) ? "" . $this->_dir . "foto_" . strtolower($product_api[$this->_foto_thumbnail]) : "__MAGMI_IGNORE__";
                        $item['media_gallery'] = $gallery_str;
                    }
                    if (count($photos) == 0) unset($item['media_gallery']);
                }

                if ($this->descriptive_fields_flag) {
                    foreach ($this->_mappings as $campo_quick => $campo_mag) {
                        if (is_array($campo_mag)) {
                            foreach ($campo_mag as $campo_magento) {
                                if ($this->_dizionario_enabled) {
                                    $product_api[$campo_quick] = $this->usaDizionario($campo_mag, $product_api[$campo_quick]);
                                }
                                $item[$campo_magento] = $product_api[$campo_quick];
                            }
                        } else {
                            if ($this->_dizionario_enabled) {
                                $product_api[$campo_quick] = $this->usaDizionario($campo_mag, $product_api[$campo_quick]);
                            }
                            $item[$campo_mag] = $product_api[$campo_quick];
                        }
                    }
                }

                // MARKETPLACE FUNCTION

                if ($this->_marketplace_enable) {

                    if (isset($product_api[$this->_marketplace_campo_quick]) && !is_null($product_api[$this->_marketplace_campo_quick])){

                        if (stripos($product_api[$this->_marketplace_campo_quick] , $this->_marketplace_allmarket_value) !== FALSE){
                            foreach ( $this->_marketplace_serialized as $marketplace ) {
                                $item[$marketplace['marketplace_magento_attribute']] = 1;
                            }
                        }else {
                            foreach ( $this->_marketplace_serialized as $marketplace ) {

                                if (stripos($product_api[$this->_marketplace_campo_quick] , $marketplace['marketplace_value']) !== FALSE){
                                    $item[$marketplace['marketplace_magento_attribute']] = 1;
                                }else {
                                    $item[$marketplace['marketplace_magento_attribute']] = "0";
                                }
                            }
                        }
                    }else{
                        foreach ( $this->_marketplace_serialized as $marketplace ) {
                            $item[$marketplace['marketplace_magento_attribute']] = "0";
                        }
                    }

                }

                $base_item = $item;
                $simples_sku_array[] = $product_api[$this->_sku_simple_map];
                if ($product_api[$this->_qty_map] > 0) {
                    $availability = 1;
                }
//                    $imported[] = $product_api[$this->_sku_simple_map];
//                    $imported_ids[] = $product_api[$this->_sku_simple_map];
                //}$this->_mode !== 'update'
                $imported[] = $product_api[$this->_sku_simple_map];
                $imported_ids[] = $product_api[$this->_sku_simple_map];

                $prev = $product_api;
                $count++;
                $this->_items_to_import[] = $item;

                if ($this->_logging_active) {
                    $this->log($item);
                }

                //if($this->_mode !== 'update') {
                if ($this->descriptive_fields_flag && $this->descriptive_fields_views_flag) {
                    //generation of view translations
                    if ($this->_is_import_multiview)
                        $this->generateViewImport($product_api, $base_item);
                }
            }

            if ($prev !== null) {
                $data_conf = $this->createConfigurable($prev, $imported_ids, $availability, $photos, $category_str);
                $this->_items_to_import[] = $data_conf;
                //if($this->_mode !== "update")
                if ($this->descriptive_fields_flag && $this->descriptive_fields_views_flag) {
                    if ($this->_is_import_multiview)
                        $this->generateViewImport($prev, $data_conf);
                }

                if ($this->_enable_new_products) {
                    $this->applyNewProducts($this->getLastConfigurableIds($data_inizio));
                }

                $data_inizio = date("Y-m-d H:i:s");
                //fwrite($f1, "[$data_inizio] Product elaboration - End Loop on products\n");
                $data_inizio = date("Y-m-d H:i:s");
                //fwrite($f1, "[$data_inizio] End Product elaboration\n");
                $data_inizio = date("Y-m-d H:i:s");
                //fwrite($f1, "[$data_inizio] Product import\n");
                $this->updateCronologia("Fine Conversione, Inizio Importazione");

                ImportProducts($this->_items_to_import, $this->_mode);
                $this->updatePercentage(100);
                $this->updateCronologia("Fine Importazione Prodotti");
                $this->deleteExpiredLog();
                if ($this->_traduzioni) {
                    $nuoviId = $this->takeNewProductsId($inizio_import);
                    if (count($nuoviId) > 0) {
                        $this->_eventManager->dispatch('frame_save_new_products', $nuoviId);
                    }
                }
                $data_inizio = date("Y-m-d H:i:s");
                //fwrite($f1, "[$data_inizio] End Product import\n");
                //fclose($f1);

                //return "Product Imported: => ".implode(" ; ",$imported). "<br><br><br> -- Number of products retrieved: ".count($products_array);
                return "done";
            } else
                return "Product Imported: => No products imported. A query from Magento to QuickWeb failed or returned no records";
        }
    }

    public function applyNewProducts($ids)
    {

        foreach ($ids as $id) {

            $product = $this->_productRep->loadById($id);


            $product->setNewsFromDate(strtotime("now"));

            $product->setNewsToDate(strtotime("now +$this->_new_product_duration days"));

            $this->_productResource->saveAttribute($product, "news_from_date");
            $this->_productResource->saveAttribute($product, "news_to_date");

        }

    }

    private function getLastConfigurableIds($data)
    {

        $collection = $this->_product_collection_factory->create();
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToFilter('created_at', array('gteq' => $data));
        $collection->addAttributeToFilter('type_id', 'configurable');
        $collection->setOrder('created_at', 'DESC');
        return $collection->getAllIds();

    }

    public function generateViewImport($productData, $base_item)
    {
        $view_item = $base_item;
        //generation of view translations
        $cat_array = array();
        if ($this->categories_flag) {
            $category_array_str = array();
            foreach ($this->_category_mappings as $store_code => $fields_data) {
                ksort($fields_data);
                $categories_array = array();
                foreach ($fields_data as $level => $campo_quick_view) {
                    if ($campo_quick_view)
                        if ($productData[$campo_quick_view])
                            $categories_array[] =
                                $productData[$campo_quick_view] . "::[" . $productData[$this->_category_mappings['admin'][$level]] . "]";
                        elseif ($productData[$this->_category_mappings['admin'][$level]])
                            $categories_array[] = $productData[$this->_category_mappings['admin'][$level]];
                        //$categories_array[] = null;
                        else
                            break;
                    elseif ($productData[$this->_category_mappings['admin'][$level]])
                        $categories_array[] = $productData[$this->_category_mappings['admin'][$level]];
                    else
                        break;
                }

                if (count($categories_array) == 0)
                    $category_array_str[$store_code] = "__MAGMI_IGNORE__";
                else
                    $category_array_str[$store_code] = implode(">", $categories_array);

                if ($this->_enable_outlet) {
                    if ($productData[$this->_outlet_quick_field] == $this->_outlet_quick_value
                        and count($categories_array) !== 0) {
                        $category_array_str[$store_code] = $this->importOutletCategory($category_array_str[$store_code], $categories_array, true);
                    }
                }

                if ($this->_unisex_enable_configs) {
                    $category_str_array = array();
                    foreach ($this->_unisex_map as $cat_path) {
                        $category_str_array[] = str_ireplace("UNISEX", $cat_path, $category_array_str[$store_code]);
                    }
                    $category_array_str[$store_code] = implode(";;", $category_str_array);
                }

                /*if($store_code == "en"){
                    print_r($category_array_str[$store_code]);
                    die();
                }*/
            }
            $cat_array = $category_array_str;
        }

        if ($this->compose_title_flag) {
            $title_array_str = array();
            foreach ($this->_composetitle_mappings as $store_code => $fields_data) {
                ksort($fields_data);
                $title_array = array();
                foreach ($fields_data as $pos => $campo_quick_view) {
                    if ($campo_quick_view)
                        if ($productData[$campo_quick_view])
                            $title_array[] = $productData[$campo_quick_view];
                }
                if (count($title_array) == 0)
                    $title_array_str[$store_code] = "__MAGMI_IGNORE__";
                else
                    $title_array_str[$store_code] = implode(" ", $title_array);

                /*if($store_code == "it"){
                    print_r($title_array_str[$store_code]);
                    die();
                }*/
            }
        }

        if ($this->_mappings_languages && count($this->_mappings_languages) >= 1)
            foreach ($this->_mappings_languages as $store_code => $fields_data) {
                // $view_item = $base_item;
                $view_item['image'] = $view_item['small_image'] = $view_item['thumbnail'] = $view_item['media_gallery'] =
                    "__MAGMI_IGNORE__";
                $view_item['store'] = $store_code;
                foreach ($fields_data as $campo_quick => $campo_quick_view) {
                    if ($campo_quick_view) {
                        if (is_array($this->_mappings[$campo_quick])) {
                            foreach ($this->_mappings[$campo_quick] as $campo) {
                                if ($this->_dizionario_enabled) {
                                    $productData[$campo_quick_view] = $this->usaDizionario($this->_mappings[$campo], $productData[$campo_quick_view]);
                                }
                                $view_item[$campo] = $productData[$campo_quick_view];
                            }
                        } else {
                            if ($this->_dizionario_enabled) {
                                $productData[$campo_quick_view] = $this->usaDizionario($this->_mappings[$campo_quick], $productData[$campo_quick_view]);
                            }
                            $view_item[$this->_mappings[$campo_quick]] =
                                ($productData[$campo_quick_view]) ? $productData[$campo_quick_view] : "__MAGMI_IGNORE__";
                            if($view_item[$this->_mappings[$campo_quick]] != "__MAGMI_IGNORE__"){
                                $this->_attribute->loadByCode('catalog_product', $this->_mappings[$campo_quick]);
                                $attributeType = $this->_attribute->getFrontendInput();
                                if($attributeType=="select"){
                                    $view_item[$this->_mappings[$campo_quick]] .= "::[".$productData[$campo_quick]."]";
                                }
                            }
                        }
                    } else {
                        $view_item[$this->_mappings[$campo_quick]] = "__MAGMI_IGNORE__";
                    }
                }
                if ($this->categories_flag)
                    $view_item['categories'] = $cat_array[$store_code];
                if ($this->compose_title_flag)
                    $view_item['name'] = $title_array_str[$store_code];
//                else
//                    $view_item['name'] = "";
                $this->_items_to_import[] = $view_item;
            }
    }

    public function getImagesFields($product_api)
    {
        $photos = array();
        $gallery = array();
        foreach ($this->_photo_map as $key => $nome_foto) {
            if ( isset($product_api[$nome_foto]) && !empty($product_api[$nome_foto]) && !is_null($product_api[$nome_foto])) {
                $photos[$nome_foto] = $this->_api_url_foto . $product_api[$nome_foto];
                $gallery[$nome_foto] = $this->_dir . "foto_" . strtolower($product_api[$nome_foto]);
            }
        }
        return array("photos" => $photos, "gallery" => $gallery);
    }

    public function importImages($imagesUrl, $exclude)
    {
        $imageType = [];
        $tmpDir = $this->_dir;
        /** create folder if it is not exists */
        $this->_file->checkAndCreateFolder($tmpDir);
        /** @var string $newFileName */
        foreach ($imagesUrl as $key => $imageUrl) {
            //$newFileName = $tmpDir.substr($imageUrl, strpos($imageUrl,"?")+1);
            $image_name = substr($imageUrl, strpos($imageUrl, "?") + 1);

            $image_name_array = str_split($image_name);
            $newFileName = $tmpDir . "foto_" . strtolower($image_name);
            if($this->_foto_custom_link_enabled)
                $imageUrl = $this->_foto_custom_link.$image_name;
            if (!file_exists($newFileName) || filesize($newFileName) < 15360){
                //$result = $this->_file->read($imageUrl, $newFileName);
                $file_headers = @get_headers($imageUrl);
                if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgcontent = "";
                }else {
                    $imgcontent = file_get_contents($imageUrl);
                }
                file_put_contents($newFileName, $imgcontent);
                $resizeObj = new Resize($newFileName);
                if($resizeObj->width > $this->_foto_max_width || $resizeObj->height > $this->_foto_max_height){
                    //ridimenziona l'immagine mantenendo l'aspect ratio
                    $resizeObj->resizeImage($this->_foto_max_width, $this->_foto_max_height, 'auto');
                    $resizeObj->saveImage($newFileName, $this->_foto_quality);
                }
            }
        }
    }

    public function getPhotosMappings()
    {
        foreach ($this->photo_map_unserialized as $key => $value) {
            $this->_photo_map[$value["order"]] = $value['name'];
        }
        ksort($this->_photo_map);
    }

    public function getCategoriesMappings()
    {
        foreach ($this->_category_unserializedata as $key => $map) {
            $this->_category_mappings["admin"][$map['category_level']] = $map['name']; //name -> campo quick
            if ($this->_is_import_multiview && count($this->_stores_array) >= 1)
                foreach ($this->_stores_array as $code => $name) {
                    $this->_category_mappings[$code][$map['category_level']] = $map[$code];
                }

        }
        ksort($this->_category_mappings["admin"]);
    }

    public function getDescriptionFieldsMappings($only_selected = false)
    {
        foreach ($this->_unserializedata as $key => $map) {
            if (!$only_selected || ($only_selected && $map["update"] == 1)) {
                if (array_key_exists($map['name'], $this->_mappings)) {
                    if (is_array($this->_mappings[$map['name']]) && !in_array($map['activation_attribute'], $this->_mappings[$map['name']])) {
                        array_push($this->_mappings[$map['name']], $map['activation_attribute']);
                    } else if ($this->_mappings[$map['name']] != $map['activation_attribute']) {
                        $temp = $this->_mappings[$map['name']];
                        $this->_mappings[$map['name']] = array($temp, $map['activation_attribute']);
                    }
                } else
                    $this->_mappings[$map['name']] = $map['activation_attribute'];
                if ($this->_is_import_multiview && count($this->_stores_array) >= 1)
                    foreach ($this->_stores_array as $code => $name) {
                        $this->_mappings_languages[$code][$map['name']] = $map[$code];
                    }
            }
        }
    }

    public function getAdditionalPricesListings()
    {
        foreach ($this->_additional_prices_unserializedata as $key => $map) {
            if (array_key_exists($map['name'], $this->_mappings_prices)) {
                if (is_array($this->_mappings_prices[$map['name']]) && !in_array($map['activation_attribute'], $this->_mappings_prices[$map['name']])) {
                    array_push($this->_mappings_prices[$map['name']], $map['activation_attribute']);
                } else if ($this->_mappings_prices[$map['name']] != $map['activation_attribute']) {
                    $temp = $this->_mappings_prices[$map['name']];
                    $this->_mappings_prices[$map['name']] = array($temp, $map['activation_attribute']);
                }
            } else
                $this->_mappings_prices[$map['name']] = $map['activation_attribute'];
        }
    }

    public function composeTitle($product_api)
    {
        foreach ($this->_composetitle_unserializedata as $key => $map) {
            $this->_composetitle_mappings["admin"][$map['field_pos']] = $map['name']; //name -> campo quick
            if ($this->_is_import_multiview && count($this->_stores_array) >= 1)
                foreach ($this->_stores_array as $code => $name)
                    $this->_composetitle_mappings[$code][$map['field_pos']] = $map[$code];
        }
        ksort($this->_composetitle_mappings["admin"]);
        $title_array = array();
        foreach ($this->_composetitle_mappings["admin"] as $pos => $campo_quick)
            if ($campo_quick)
                if ($product_api[$campo_quick])
                    $title_array[] = html_entity_decode($product_api[$campo_quick]);
                else
                    continue;
            else
                continue;
        if (count($title_array) == 0)
            $title_str = $product_api[$this->_sku_simple_map];
        else
            $title_str = implode(" - ", $title_array);
        return $title_str;
    }

    public function importCategories($product_api)
    {
        $categories_array = array();
        //$n_levels = count(array_keys($this->_category_mappings["admin"]));
        foreach ($this->_category_mappings["admin"] as $level => $campo_quick)
            if ($campo_quick)
                if ($product_api[$campo_quick])
                    $categories_array[] = $product_api[$campo_quick];
                else
                    break;
            else
                break;
        if (count($categories_array) == 0)
            $category_str = "__MAGMI_IGNORE__";
        else
            $category_str = implode(">", $categories_array);

        // outlet category
        if ($this->_enable_outlet) {
            if ($product_api[$this->_outlet_quick_field] == $this->_outlet_quick_value
                and count($categories_array) !== 0) {
                $category_str = $this->importOutletCategory($category_str, $categories_array);
            }
        }

        if ($this->_unisex_enable_configs) {
            $category_str_array = array();
            foreach ($this->_unisex_map as $cat_path) {
                $category_str_array[] = str_ireplace("unisex", $cat_path, $category_str);
            }
            $category_str = implode(";;", $category_str_array);
        }
        return $category_str;
    }

    public function importOutletCategory($category_str, $categories_array, $multiview = false)
    {

        switch ($this->_outlet_level) {
            case 1:
                // liv1
                if ($this->_outlet_build_tree) {
                    $category_str .= ";;" . $this->_outlet_cat_name . ">" . $category_str;
                } else
                    $category_str .= ";;" . $this->_outlet_cat_name;
                break;
            case 2:
                if ($this->_outlet_combine_name) {
                    if ($multiview) {
                        $liv_1_name_arr = explode("::[", $categories_array[0]);
                        $liv_1_name_arr[1] = $this->_outlet_cat_name . " " . $liv_1_name_arr[1];
                        $liv_1_name = implode("::[", $liv_1_name_arr);
                    } else
                        $liv_1_name = $categories_array[0];

                    $outlet_cat_name = $this->_outlet_cat_name . " " . $liv_1_name;
                    //$outlet_cat_name = $this->_outlet_cat_name." ".$categories_array[0];
                } else
                    $outlet_cat_name = $this->_outlet_cat_name;
                // liv2
                if ($this->_outlet_build_tree) {
                    $outlet_tree = array($categories_array[0], $outlet_cat_name,
                        implode(">", array_slice($categories_array, 1)));
                    $category_str .= ";;" . implode(">", $outlet_tree);
                } else {
                    $outlet_tree = array($categories_array[0], $outlet_cat_name);
                    $category_str .= ";;" . implode(">", $outlet_tree);
                }

                break;
        }

        return $category_str;
    }


    public function createCategory($categoryPath)
    {
        //echo $categoryPath."-TEST\n";
        $cats = explode("/", $categoryPath);
        $ids = array();
        foreach ($cats as $level => $cat) {
            if ($level == 0)
                $parentId = \Magento\Catalog\Model\Category::TREE_ROOT_ID;
            else
                $parentId = $ids[$level - 1];

            $parentCategory = $this->_categoryModel->load($parentId);

            $category = $this->_categoryFactory->create();
            //Check exist category
            $cate = $category->getCollection()
                ->addAttributeToFilter('name', $cat)
                ->addAttributeToFilter('parent_id', ['eq' => $parentId])
                ->getFirstItem();

            if ($cate->getId() == null) {

                $category->setPath($parentCategory->getPath())
                    ->setParentId($parentId)
                    ->setName($cat)
                    ->setUrlKey($categoryPath)
                    ->setIsActive(true);
                $category->save();
                $ids[] = $category->getId();
            } else {
                $ids[] = $cate->getId();
            }

        }
        return $ids;
    }


    public function createConfigurable($productData, $imported_skus, $availability, $gallery_array, $category_str)
    {
        /*try {
            $configurableProduct = $this->_productRep->get($productData['CODE']);
            $ConfisNew = false;
        } catch (NoSuchEntityException $e) {
            //$configurableProduct = $this->_productFactory->create();
            //$configurableProduct->setSku($productData['CODE']);
            $ConfisNew = true;
        }*/
        /*$titolo = array(); // index => nomecampo
        $name = "";
        foreach ($this->_composetitle_unserializedata as $attr){
            $titolo[$attr['field_pos']] = $attr['name'];
        }
        for ($i=1; $i<=count($titolo); $i++){
            $name .= $productData[$titolo[$i]]." ";
        }*/

        try{
            $confProduct = $this->_productRep->get($productData[$this->_sku_conf_map]);
            if($confProduct && $confProduct->getTypeId() == "configurable"){
                $children = $confProduct->getTypeInstance()->getUsedProducts($confProduct);
                foreach ($children as $child){
                    $sku = $child->getSku();
                    if(!in_array($sku, $imported_skus)){
                        $imported_skus[] = $sku;
                    }
                }
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e){

        }

        $data = array(
            "attribute_set" => $this->_attribute_set,
            "sku" => $productData[$this->_sku_conf_map],
            "type" => "configurable",
            "visibility" => 4,  #4 for configurable
            "status" => 1 , //($availability != 0) ? 1 : 2,
            "tax_class_id" => (int) $this->_taxClass,
            "configurable_attributes" => $this->_helpers->getConfigurableAttributeCode(), ### for configurable "color,taglia",
            "qty" => "0",//berto mod - error sql sul valore decimale
            "is_in_stock" => ($availability != 0) ? 1 : 0,
            "manage_stock" => 1,
            "price" => $productData[$this->_price_map],
            //"weight" => 1,
            "use_config_manage_stock" => 1,
            "simples_skus" => implode(",", $imported_skus)
        );

        // additional prices listings map
        foreach ($this->_mappings_prices as $campo_quick => $campo_mag) {
            if (is_array($campo_mag)) {
                foreach ($campo_mag as $campo_magento) {
                    $data[$campo_magento] = $productData[$campo_quick];
                }
            } else {
                $data[$campo_mag] = $productData[$campo_quick];
            }
        }

        // if ($this->_mode !== 'update') {
        if ($this->categories_flag) {
            $category_str = $this->importCategories($productData);
            $data['categories'] = $category_str;
        }
        if ($this->compose_title_flag) {
            $title_str = $this->composeTitle($productData);
            $data['name'] = $title_str;
        }

        if ($this->photos_flag) {
            $gallery = implode(";", $gallery_array);
            $data['image'] = !empty($productData[$this->_foto_main]) ? $this->_dir . "foto_" . strtolower($productData[$this->_foto_main]) : "__MAGMI_IGNORE__";
            $data['small_image'] = !empty($productData[$this->_foto_small]) ? $this->_dir . "foto_" . strtolower($productData[$this->_foto_small]) : "__MAGMI_IGNORE__";
            $data['thumbnail'] = !empty($productData[$this->_foto_thumbnail]) ? $this->_dir . "foto_" . strtolower($productData[$this->_foto_thumbnail]) : "__MAGMI_IGNORE__";
            $data['media_gallery'] = $gallery;
            if (count($gallery_array) == 0)
                unset($data['media_gallery']);
            //$data['media_gallery'] = "__MAGMI_IGNORE__";
        }

        /*$categorie = array();
        foreach ($this->_serialize->unserialize($this->_mapping_categorie) as $key => $map) {
            $categorie[$map["category_level"]] = $map['name'];
        }
        $data['categories'] = "";
        for ($i = 1; $i < count($categorie) + 1; $i++) {
            $data['categories'] .= $productData[$categorie[$i]];
            if ($i != count($categorie)) {
                $data['categories'] .= ">";
            }
        }*/
        $data['weight'] = 1;

        $base_item = $data;
        if ($this->descriptive_fields_flag) {
            foreach ($this->_mappings as $campo_quick => $campo_mag) {
                if (is_array($campo_mag)) {
                    foreach ($campo_mag as $campo_magneto) {
                        if (!(stripos($campo_magneto, "barcode") !== FALSE)) {
                            if (!array_key_exists($campo_magneto, $data) || empty($data[$campo_magneto])) {
                                $data[$campo_magneto] = $productData[$campo_quick];
                            }
                        } else {
                            $data[$campo_magneto] = "__MAGMI_IGNORE__";
                        }
                    }
                } else {
                    if (!array_key_exists($campo_mag, $data) || empty($data[$campo_mag])) {
                        $data[$campo_mag] = $productData[$campo_quick];
                    }
                    if (stripos($campo_mag, "barcode") !== FALSE)
                        $data[$campo_mag] = "__MAGMI_IGNORE__";
                }
            }
        }
        // } $this->_mode !== 'update'
        $data[$this->_helpers->getConfigurableAttributeCode()] = "__MAGMI_IGNORE__";

        // MARKETPLACE FUNCTION

        if ($this->_marketplace_enable) {

            if (isset($productData[$this->_marketplace_campo_quick]) && !is_null($productData[$this->_marketplace_campo_quick])){

                if (stripos($productData[$this->_marketplace_campo_quick] , $this->_marketplace_allmarket_value) !== FALSE){
                    foreach ( $this->_marketplace_serialized as $marketplace ) {
                        $data[$marketplace['marketplace_magento_attribute']] = 1;
                    }
                }else {
                    foreach ( $this->_marketplace_serialized as $marketplace ) {

                        if (stripos($productData[$this->_marketplace_campo_quick] , $marketplace['marketplace_value']) !== FALSE){
                            $data[$marketplace['marketplace_magento_attribute']] = 1;
                        }else {
                            $data[$marketplace['marketplace_magento_attribute']] = "0";
                        }
                    }
                }
            }else{
                foreach ( $this->_marketplace_serialized as $marketplace ) {
                    $data[$marketplace['marketplace_magento_attribute']] = "0";
                }
            }

        }

//        print_r($data);
//        die();
        return $data;
    }

    private function takeNewProductsId($data_inizio)
    {
        $collection = $this->_product_collection_factory->create();
        $collection->addAttributeToSelect('sku');
        $collection->addAttributeToFilter('created_at', array('gteq' => $data_inizio));
        $collection->setOrder('created_at', 'DESC');
        return $collection->getAllIds();
    }

    /**
     * @param array $item
     */
    public function log(array $item)
    {
        $old_log_attr = array();
        $new_log_attr = array();
        $new_qty = $item['qty']; //attributo base magento
        $new_price = $item['price'];

        foreach ($this->_logging_attributes as $attr) {
            $new_log_attr[$attr] = $item[$attr];
        }
        try {
            $product = $this->_productRep->get($item['sku']);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            //prodotto non esiste
            $product = false;
        }
        if ($product) {
            $old_qty = $product->getQty();
            $old_price = $product->getData("price");
            foreach ($this->_logging_attributes as $attr) {
                $old_log_attr[$attr] = $product->getData($attr);
            }
        } else {
            $old_qty = " (Nuovo) ";
            $old_price = " (Nuovo) ";
        }

        $qty = json_encode(array("da" => $old_qty, "a" => $new_qty));
        $price = json_encode(array("da" => $old_price, "a" => $new_price));
        $altro = array();
        foreach ($this->_logging_attributes as $attr) {
            if (!$product) {
                $da = " (Nuovo) ";
            } else {
                $da = addslashes($old_log_attr[$attr]);
            }
            $altro[$attr] = array("da" => $da, "a" => $new_log_attr[$attr]);
        }
        $row = array(
            'sku' => $item['sku'],
            'qty' => $qty,
            'price' => $price,
            'altro' => json_encode($altro)
        );
        $model = $this->_loggerFactory->create();
        $model->addData($row);
        $saveData = $model->save();
    }

    private function deleteExpiredLog()
    {
        $date = date("Y-m-d");
        $expiration_date = date('Y-m-d', strtotime($date) - (24 * 3600 * $this->_logging_days));
        $sql = "DELETE FROM `frame_quick_logger` WHERE `created_at` <= '{$expiration_date}'";
        try {
            $result1 = $this->_connection->fetchAll($sql);
        } catch (\Exception $e) {

        }
    }

    private function usaDizionario($campo_mag, $value)
    {
        foreach ($this->_dizionario_unserialized as $index => $sub) {
            if ($campo_mag == $sub['attributo']) {
                $find = $sub['find'];
                if($find == "\\n"){
                    $find = "\n";
                }
                $replace = $sub['replace'];
                $value = str_replace($find, $replace, $value);
            }
        }
        return $value;
    }

    private function updateCronologia($messaggio)
    {
        if ($this->_cronologia_id == null) return false;
        $data = date('Y/m/d H:i:s');
        $cronologia = $this->_cronologia->create()->load($this->_cronologia_id);
        $value = $cronologia->getData();
        $contenuto_old = json_decode($value['contenuto'], true);
        $contenuto_old[] = array("time" => $data, "messaggio" => $messaggio);
        $contenuto = json_encode($contenuto_old);
        $this->_cronologia_data['updated_at'] = $data;
        $this->_cronologia_data['contenuto'] = $contenuto;
        return $cronologia->addData($this->_cronologia_data)->setId($this->_cronologia_id)->save();
    }

    private function updatePercentage($percentage)
    {
        if ($this->_cronologia_id == null) return false;
        $data = date('Y/m/d H:i:s');
        $cronologia = $this->_cronologia->create()->load($this->_cronologia_id);
        $value = $cronologia->getData();
        $contenuto_old = json_decode($value['contenuto'], true);
        $contenuto_old["percentage"] = $percentage;
        $contenuto = json_encode($contenuto_old);
        $this->_cronologia_data['updated_at'] = $data;
        $this->_cronologia_data['contenuto'] = $contenuto;
        return $cronologia->addData($this->_cronologia_data)->setId($this->_cronologia_id)->save();
    }

    private function mapStoreCode($serializedMapping){
        $newMapping = array();
        foreach ($serializedMapping as $key => $mapping) {
            $newMapping[$key] = array();
            foreach ($mapping as $storeId => $value){
                $storeCode = $storeId;
                if(is_numeric($storeId)){
                    $storeCode = (string)$this->_storeManager->getStore($storeId)->getCode();
                }
                $newMapping[$key][$storeCode] = $value;
            }
        }
        return $newMapping;
    }
}

?>