<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2021-02-09
 * Time: 13:12
 */

namespace Frame\Quick\Console\Command;

use Frame\Quick\Model\ImportJob;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadAndImportProductsCommand extends Command
{
    const REFERENCE_URL = 'reference_url';
    const INVENTORY_URL = 'inventory_url';
    const SEASON = 'season';
    const GENDER = 'gender';

    protected $importJob;
    /**
     * @var \Frame\Quick\Helper\Data
     */
    protected $_quickHelper;

    public function __construct(
        ImportJob $importJob,
        \Frame\Quick\Helper\Data $quickHelper,
        $name = null
    ) {
        $this->importJob = $importJob;
        $this->_quickHelper = $quickHelper;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('webora:import:products');
        $this->setDescription('Webora import products');
        $array = [
            new InputOption(self::SEASON, null, InputOption::VALUE_OPTIONAL, ''),
            new InputOption(self::GENDER, null, InputOption::VALUE_OPTIONAL, ''),
        ];
        $this->setDefinition(
            new InputDefinition($array)
        );
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $season = $input->getOption(self::SEASON);
        $gender = $input->getOption(self::GENDER);

        $stagioni = [
            200,
            201,
            217,
            393,
            7329,
            7331,
            7332,
            7395
        ];

        $generi = [
            'UOMO',
            'DONNA',
            'BAMBINO'
        ];

        $base_url = $this->_quickHelper->getModuleConfig("api", "url") . "/table?safekey=" . $this->_quickHelper->getModuleConfig("api", "safetykey");

        foreach ($stagioni as $stagione) {
            foreach ($generi as $genere) {
                $api_url_final[0]['url'] = $base_url . '&NOME=FRAME_references_all&where="IDSEASON=' . $stagione . " AND SEX='$genere'\"";
                $api_url_final[1]['url'] = $base_url . '&NOME=FRAME_inventory_all&where="IDSEASON=' . $stagione . " AND STOCK_QNT > 0\"";
                $this->importJob->loadAndImportProducts($api_url_final, $stagione, $genere);
            }
        }

        $output->writeln("<info>Comando eseguito correttamente</info>");

    }
}
