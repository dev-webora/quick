<?php
namespace Frame\Quick\Controller\Adminhtml\MassOrder;

use Frame\Quick\Model\Export\ExportCancel;
use Frame\Quick\Model\ExportOrderQueueFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\Response\Http\FileFactory;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Controller\ResultFactory;

class MassOrderCancel extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    private $_export_cancel;
    private $_invoiceSender;
    private $rep;
    private $fileFactory;
    private $scopeConfig;
    private $region;

    public function __construct(Context $context,
                                Filter $filter,
                                CollectionFactory $collectionFactory,
                                OrderRepositoryInterface $rep,
                                FileFactory $fileFactory,
                                ScopeConfigInterface $scopeConfig,
                                RegionFactory $region,
                                InvoiceSender $invoiceSender,
                                ExportCancel $export_cancel)
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->_invoiceSender = $invoiceSender;
        $this->rep = $rep;
        $this->fileFactory = $fileFactory;
        $this->scopeConfig = $scopeConfig;
        $this->region = $region;

        $this->_export_cancel = $export_cancel;
    }

    protected function massAction(AbstractCollection $collection)
    {
        $orderIdsList = $collection->getAllIds();
        return $this->_export_cancel->cancel($orderIdsList);
    }
}