<?php

namespace Frame_Quick_Model_System_Config_Backend_Serialized_Grid;

class Mapping extends \Magento\Framework\App\Config\Value
{
    protected function _beforeSave()
    {
        // Clean given value by removing "__empty" and incomplete sub values
		
		$label = $this->getData('field_config/label');
        $value = $this->getValue();
		
		
		
        if ($value == '') {
            throw new \Magento\Framework\Exception\ValidatorException(__($label . ' has to be filled or removed.'));
        }
		
		
        if (is_array(($value))) {
            unset($value['__empty']);
			
            foreach ($value as $key => $mapping) {
                if (trim($mapping['quick']) == '' || trim($mapping['magento']) == '') {
                    unset($value[$key]);
                }
            }
			
        } else {
            $value = array();
        }
        
        $this->setValue($value);
        parent::_beforeSave();
    }
	
	protected function _afterLoad()
    {
		if (!is_array($this->getValue())) {
            $value = $this->getValue();
			$this->setValue(empty($value) ? false : unserialize($value));
        }
    }
}