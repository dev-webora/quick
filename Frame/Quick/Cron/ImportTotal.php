<?php

namespace Frame\Quick\Cron;

//use False\MyClass;

ini_set("memory_limit", "5000M");
set_time_limit(18000);
ini_set("max_input_time",-1);
ini_set("post_max_size","122M");

class ImportTotal extends Import
{
    protected $_importer;
    protected $_helpers;
    protected $_custom_query_enabled;
    protected $_save_custom_query_time;
    protected $_custom_query_ref;
    protected $_custom_query_inv;


    public function __construct(
        \Frame\Quick\Model\Import\Product $importer,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Frame\Quick\Helper\Data $helpers
    )
    {
        parent::__construct($importer, $cronologiaFactory, $helpers);
        $this->_resource = $resource;
    }

    public function execute(\Magento\Cron\Model\Schedule $schedule = null, $is_totale = false)
    {
        if (!$this->_enabled_cron) {
            return $this;
        }

        $this->newCronologia("Inizio import Totale", "Import Totale");

        $data_inizio = date("Y-m-d H:i:s");
        $time_start = strtotime($data_inizio);

        $products_array = array();
        $skipapi = false;

        $import_info = [];
        $import_info["type"] = "all";

        if($skipapi == false){
            $api_url_final = $this->composeApiInfo(false);
            $products = $this->runApiCall($api_url_final, $import_info);
            if($this->_source_type == "multitable"){
                $products_array = $this->extractProductsFromMultiTable($products);
            }elseif ($this->_source_type == "singletable"){
                $products_array = $products;
            }
        }else{
            $products_array = $this->extractProductsFromMultiTable(NULL, $skipapi);
        }

        if(count($products_array) == 0){
            $this->updateCronologia("Errore: 0 prodotti da Importare!");
            return $this;
        }
        $this->updateCronologia("Inizio Importazione Totale di ".count($products_array). " prodotti");
        $import_info = array("type" => "all", "functions" => array());
        $res = $this->_importer->runImport($products_array,"create", $this->_cronologia_id, $import_info);
        $data_inizio = date("Y-m-d H:i:s");
        if ($this->_custom_query_enabled && $this->_save_custom_query_time){
            $time_end = strtotime($data_inizio);
            $time_diff = round(abs($time_end - $time_start)/60);//minuti
            $this->_helpers->setModuleConfig("custom_query", "enable", 0);
            $this->_helpers->setModuleConfig("custom_query", "last_time", $time_diff);
            //fwrite($f, "[$data_inizio] - Custom Query Time: $time_diff\n");
        } else {
            $this->_helpers->setModuleConfig("custom_query", "last_time", 0);
        }
        try {
            $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
            if(stripos($url->getCurrentUrl(),"///") !== false){
                $schedule->setMessages("IMPORTED");
                $schedule->save();
            }
        }catch (\Exception $e){
            //scrivi nel log
        }
        //fclose($f);

        return $this;
    }
}
