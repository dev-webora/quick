<?php

namespace Frame\Quick\Cron;

use Frame\Quick\Helper\Data;

ini_set("memory_limit", "8000M");
set_time_limit(3600);
ini_set("max_input_time", -1);
error_reporting(E_ALL);
ini_set('display_errors', 1);

abstract class Import
{
    protected $_source_type;
    protected $_cacheTypeList;
    protected $_importer;
    protected $_helpers;
    protected $_codifica;
    protected $_custom_query_enabled;
    protected $_disable_custom_query;
    protected $_custom_query_ref;
    protected $_custom_query_inv;
    protected $_last_time_custom_query;
    protected $_sku_conf_map;
    protected $_api_pieces = [];
    protected $_cronologia;
    protected $_cronologia_id;
    protected $_cronologia_data;
    protected $_enabled_cron;
    protected $_resource;
    protected $_enable_notifica_errori;
    protected $_notifica_mail;

    /**
     * Import constructor.
     * @param \Frame\Quick\Model\Import\Product $importer
     * @param \Frame\Quick\Helper\Data $helpers
     */
    public function __construct(
        $importer,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        $helpers
    ) {
        $this->_importer = $importer;
        $this->_cronologia = $cronologiaFactory;
        $this->_helpers = $helpers;
        $this->_source_type =  $this->_helpers->getModuleConfig("general", "import_type");
        $this->_codifica = $this->_helpers->getModuleConfig("api", "encoding");
        $this->_custom_query_enabled =  $this->_helpers->getModuleConfig("custom_query", "enable");
        $this->_disable_custom_query = $this->_helpers->getModuleConfig("custom_query", "disable_custom_query");
        $this->_custom_query_ref =  $this->_helpers->getModuleConfig("custom_query", "query_ref");
        $this->_custom_query_inv =  $this->_helpers->getModuleConfig("custom_query", "query_inv");
        $this->_api_pieces['url'] = $this->_helpers->getModuleConfig("api", "url");
        $this->_api_pieces['safetykey'] = $this->_helpers->getModuleConfig("api", "safetykey");
        $this->_api_pieces['tableinventory'] = $this->_helpers->getModuleConfig("api", "tableinventory");
        $this->_api_pieces['tablereferences'] = $this->_helpers->getModuleConfig("api", "tablereferences");
        $this->_last_time_custom_query = $this->_helpers->getModuleConfig("custom_query", "last_time");
        $this->_sku_conf_map = $this->_helpers->getModuleConfig("basicfields", "skuconf");
        $this->_enabled_cron = $this->_helpers->getModuleConfig("general", "enable_cron_import");
        $this->_enable_notifica_errori = $this->_helpers->getModuleConfig("notifica_errori", "enable_notifica_errori");
        if ($this->_enable_notifica_errori) {
            $this->_notifica_mail = $this->_helpers->getModuleConfig("notifica_errori", "notifica_mail");
        }
    }

    /**
     * @param $api_url_final
     * @param int $max_calls
     * @return array
     * @throws \Exception
     */
    public function callReferenceApi($api_url_final, int $max_calls): array
    {
        $api_references = $api_url_final;
        $i = 0;
        do {
            $this->updateCronologia("Chiamata API: " . $api_references);

            list($token_ref, $status_ref) = $this->doCurl($api_references);

            if ($status_ref != 200 || $i == $max_calls && $status_ref != 200) {
                $this->updateCronologia("Errore: Risposta: " . $token_ref);
                $this->updateCronologia("Errore: Stato Risposta: " . $status_ref);
                $this->errorDebug($status_ref, $api_references, $token_ref);
                throw new \Exception('Reference call failed too many times');
            }
            $i++;
        } while (empty(trim($token_ref)));
        return [$token_ref, $status_ref];
    }

    /**
     * @param $api_url_final
     * @return array
     */
    public function doCurl($api_url_final): array
    {
        $toReplace = [' ', '<', '"'];
        $replacements = ['%20', '%3C', '%22'];
        $api_url_final = str_replace($toReplace, $replacements, $api_url_final);

        $ch = curl_init($api_url_final);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ordineweb));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TRANSFERTEXT, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($ordineweb))));
        $token = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return [$token, $status];
    }

    /**
     * @param $importMode
     * @param $token
     * @return array|mixed
     */
    public function getProductsFromToken($token)
    {
        $importMode = $this->_helpers->getModuleConfig("api", "import_mode");
        if ($importMode == Data::IMPORT_MODE_CSV_API) {
            $products = $this->_helpers->csvToArray($token);
        } else {
            $json = $this->applyEncoding($token);
            $json = preg_replace('/[[:cntrl:]]/', '', $json);
            $products = json_decode($json, true);
        }
        return $products;
    }

    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     * @param bool $is_totale
     */
    abstract protected function execute(\Magento\Cron\Model\Schedule $schedule = null, $is_totale = false);

    /**
     * @param bool $incrementale
     * @param bool $inventory_only
     * @return array|string
     */
    public function composeApiInfo($incrementale = true, $inventory_only = false)
    {
        switch ($this->_source_type) {
            case "singletable":
                return $this->composeSingleTableApiInfo($incrementale);
            case "multitable":
                return $this->composeMultiTableApiInfo($incrementale, $inventory_only);
        }
    }

    /**
     * @param bool $incrementale
     * @return string
     */
    private function composeSingleTableApiInfo($incrementale = true)
    {
        $importMode = $this->_helpers->getModuleConfig("api", "import_mode");
        if ($this->_custom_query_enabled) {
            $api_url_final = $this->_custom_query_inv;
        } else {
            $last_query_time = $this->_helpers->getModuleConfig("query", "last_time_ok");
            $data = date("d-m-Y H:i:s");
            $timestamp = strtotime($data);
            if ($last_query_time != null) {
                $diff = ceil(abs($last_query_time - $timestamp)/60);
            } else {
                $diff = "10";
            }
            if ($this->_last_time_custom_query != null && $this->_last_time_custom_query != 0) {
                $diff += $this->_last_time_custom_query;
            }
            $api_url_final = $this->_api_pieces['url'] . '/table?NOME=' . $this->_api_pieces['tableinventory'];
            if ($importMode == Data::IMPORT_MODE_CSV_API) {
                $api_url_final .= "&tipo=csv";
            }
            if ($incrementale) {
                $api_url_final = $api_url_final . '&WHERE="DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=' . $diff . '"';
            }
            if ($this->_api_pieces['safetykey']) {
                $api_url_final = $api_url_final . '&safekey=' . $this->_api_pieces['safetykey'];
            }
        }
        //$api_url_final = $api_url_final.'&first=2';
        return $api_url_final;
    }

    /**
     * @param bool $incrementale
     * @param bool $inventory_only
     * @return array
     */
    private function composeMultiTableApiInfo($incrementale = true, $inventory_only = false)
    {
        $importMode = $this->_helpers->getModuleConfig("api", "import_mode");

        if ($this->_custom_query_enabled) {
            $api_url_final[]['url'] = $this->_custom_query_ref;
            $api_url_final[]['url'] = $this->_custom_query_inv;
        } else {
            $api_url_final_inventory = $this->_api_pieces['url'] . '/table?NOME=' . $this->_api_pieces['tableinventory'];
            $api_url_final_references = $this->_api_pieces['url'] . '/table?NOME=' . $this->_api_pieces['tablereferences'];

            if ($importMode == Data::IMPORT_MODE_CSV_API) {
                $api_url_final_inventory .= "&tipo=csv";
                $api_url_final_references .= "&tipo=csv";
            }
            $data = date("d-m-Y H:i:s");
            $timestamp = strtotime($data);
            $last_query_time = $this->_helpers->getModuleConfig("query", "last_time_ok");
            if ($last_query_time != null) {
                $diff = ceil(abs($last_query_time - $timestamp)/60);
            } else {
                $diff = "10";
            }
            if ($incrementale) {
                $api_url_final_inventory = $api_url_final_inventory . '&WHERE="DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=' . $diff . '"';
                $api_url_final_references = $api_url_final_references . '&WHERE="DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=' . $diff . '"';
            }
            if ($this->_api_pieces['safetykey']) {
                $api_url_final_inventory = $api_url_final_inventory . '&safekey=' . $this->_api_pieces['safetykey'];
                $api_url_final_references = $api_url_final_references . '&safekey=' . $this->_api_pieces['safetykey'];
            }
            //$api_url_final_references = $api_url_final_references . '&first=1';
            if ($inventory_only) {
                $api_url_final[]['url'] = "";
            } else {
                $api_url_final[]['url'] = $api_url_final_references;
            }
            $api_url_final[]['url'] = $api_url_final_inventory;
        }
        return $api_url_final;
    }

    public function runApiCall($api_url_final, $import_info)
    {
        $this->deleteOldCronologia();
        $time = strtotime(date("d-m-Y H:i:s"));
        if ($this->_source_type == "singletable") {
            $this->updateCronologia("Chiamata API: " . $api_url_final);
            list($token, $status) = $this->doCurl($api_url_final);
            if ($status == 200) {
                //salvo il timestamp se la risposta è andata a buon fine
                $this->_helpers->setModuleConfig("query", "last_time_ok", $time);
            } else {
                $this->errorDebug($status, $api_url_final, $token);
            }
            $this->updateCronologia("Fine Chiamata Status: " . $status);

            return $this->getProductsFromToken($token);
        } elseif ($this->_source_type == "multitable") {
            //chiamata references
            $max_calls = 5;
            $token_ref = "";
            $status_ref = null;
            $token_inv = "";
            $status_inv = null;
            $products_references = [];
            $products_inventory = [];

            if ($import_info['type'] != 'stockprice') {
                list($token_ref, $status_ref) = $this->callReferenceApi($api_url_final[0]['url'], $max_calls);

                $products_references = $this->getProductsFromToken($token_ref);
            } else {
                $status_ref = 200;
                $token_ref = false;
                $products_references = [];
            }
            if ($status_ref == 200) {
                $this->updateCronologia("Fine Chiamata References");
                //chiamata inventory
                $api_inventory = $api_url_final[1]['url'];
                $i=0;
                do {
                    $this->updateCronologia("Chiamata API: " . $api_inventory);

                    list($token_inv, $status_inv) = $this->doCurl($api_inventory);

                    if ($status_inv != 200 || $i == $max_calls) {
                        $this->updateCronologia("Stato Risposta: " . $status_inv);
                        $this->errorDebug($status_ref, $api_inventory, $token_ref);
                        throw new \Exception('Inventory call failed too many times');
                    }
                    $i++;
                } while (empty(trim($token_inv)) || !$token_inv);
                if ($status_inv == 200) {
                    //salvo il timestamp se la risposta è andata a buon fine
                    $this->_helpers->setModuleConfig("query", "last_time_ok", $time);
                    //disable custom query
                    if ($this->_disable_custom_query == 1) {
                        $this->_helpers->setModuleConfig("custom_query", "enable", 0);
                    }
                }
                $this->updateCronologia("Fine Chiamata Inventory");
                //$token = curl_multi_getcontent($ch[0]);

                //$token = curl_multi_getcontent($ch[1]);

                $products_inventory = $this->getProductsFromToken($token_inv);
            }
            //close the handles
            //curl_multi_remove_handle($mh, $ch[0]);
            //curl_multi_remove_handle($mh, $ch[1]);
            //curl_multi_close($mh);
            return ["references"=>$products_references, "inventory"=>$products_inventory];
        }
    }

    protected function extractProductsFromMultiTable($tables_results, $skip_api = false, $inventory_only = false)
    {
        $products_references = null;
        $products_inventory = null;
        if ($skip_api == false) {
            $products_references = $tables_results['references'];
            $products_inventory = $tables_results['inventory'];
        } else { // for testing purpose
            $products_inventory = $this->_helpers->getFile('inventory');

            if (!$inventory_only) {
                $products_references = $this->_helpers->getFile('references');

            }
        }
        if (!$products_references || !$products_inventory) {
            if ($inventory_only && $products_inventory) {
                $products_array = $products_inventory;
            } else {
                $products_array = null;
            }
        } else {
            $products_references_dict = [];
            $header_references = '';
            foreach ($products_references as $index=>$prod) {
                $products_references_dict[$prod[$this->_sku_conf_map]] = $prod;
                if ($header_references == '') {
                    $header_references = array_keys($prod);
                }
            }

            $products_array = [];
            foreach ($products_inventory as $index => $prod) {
                if (array_key_exists(trim($prod[$this->_sku_conf_map]), $products_references_dict)) {
                    $additional_fields = array_combine($header_references, $products_references_dict[$prod[$this->_sku_conf_map]]);
                    //$additional_fields = array_combine($header_references, $products_references_dict[$prod['CODE']]);
                    $products_array[] = $prod + $additional_fields;
                }
            }
        }

        return $products_array;
    }

    /**
     * @param $token
     * @return string
     *
     * $token is the result of Api Call in json format
     * return $token + encoding if required
     */
    public function applyEncoding($token)
    {
        if ($this->_codifica != "no_conv") {
            return  mb_convert_encoding($token, $this->_codifica);
        } else {
            return $token;
        }
    }

    public function newCronologia($text, $importType) {
        $data = date('Y/m/d H:i:s');
        $cronologia = $this->_cronologia->create();
        $contenuto = [["time" => $data, "messaggio" => $text]];
        $this->_cronologia_data = [
            "updated_at" => $data,
            "tipo" => $importType,
            "contenuto" => json_encode($contenuto)
        ];
        $cronologia->addData($this->_cronologia_data);
        $cronologia->save();
        $this->_cronologia_id = $cronologia->getId();
        return $cronologia->getId();
    }

    public function updateCronologia($messaggio)
    {
        if ($this->_cronologia_id == null) {
            return false;
        }
        $data = date('Y/m/d H:i:s');
        $cronologia = $this->_cronologia->create()->load($this->_cronologia_id);
        $value = $cronologia->getData();
        $contenuto_old = json_decode($value['contenuto'], true);
        $contenuto_old[] = ["time" => $data, "messaggio" => $messaggio];
        $contenuto = json_encode($contenuto_old);
        $this->_cronologia_data['updated_at'] = $data;
        $this->_cronologia_data['contenuto'] = $contenuto;
        return $cronologia->addData($this->_cronologia_data)->setId($this->_cronologia_id)->save();
    }

    public function deleteOldCronologia()
    {
        $expireDay = 3;
        $date = date('Y/m/d H:i:s');
        $model = $this->_cronologia->create();
        $cronologia = $model->getCollection();
        foreach ($cronologia as $item) {
            $data = $item->getData();
            $created_at = $data['created_at'];
            $id = $data['id'];
            $expiration_date = date('Y-m-d', strtotime($date) - (24*3600*$expireDay));
            if (strtotime($expiration_date) >= strtotime($created_at)) {
                $model->load($id)->delete();
            }
        }
    }

    public function errorDebug($status, $link, $messaggio)
    {
        /*if($this->_enable_notifica_errori){
            $headers = "Reply-To: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
            $headers .= "Return-Path: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
            $headers .= "From: ".$_SERVER['HTTP_HOST']." <sender@sender.com>\r\n";
            $headers .= "Organization: ".$_SERVER['HTTP_HOST']."\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
            $headers .= "X-Priority: 3\r\n";
            $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
            mail($this->_notifica_mail, "FANTASIA-API-4567",
                "Api Server Error ".$_SERVER['HTTP_HOST']."\n
                Api Call: ".$link."\n
                Message: ".$messaggio."\n
                HTTP Status: ".$status, $headers);
        }*/
    }
    

}
