<?php
namespace Frame\Quick\Block;
class Import extends \Magento\Framework\View\Element\Template
{
	protected $_coreRegistry;
	
	public function __construct(\Magento\Framework\View\Element\Template\Context $context,\Magento\Framework\Registry $coreRegistry)
	{
		$this->_coreRegistry = $coreRegistry;
		parent::__construct($context);
	}

	public function sayImport()
	{
		$msg = $this->_coreRegistry->registry('res');
		return __('After Import! - res: '.$msg);
	}
	
	
}