<?php

  namespace Frame\Quick\Controller\Adminhtml\Home;
  //include dirname(__FILE__) . '/../../../Model/Import/Product.php';
  // use \Frame\Quick\Model\Import\Product;

  class HeavyImport extends \Magento\Backend\App\Action
  {
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	protected $resultPageFactory;
	protected $_importer;
	protected $_coreRegistry;
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	protected $dp;
	protected $_heavyimporter;
    protected $_import_type;
    protected $_light_importer;
    //helper funciotns
    protected $_helpers;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Frame\Quick\Model\Import\Product $importer,
		//Product $importer,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $coreRegistry,
        //Helper functions
        \Frame\Quick\Helper\Data $helpers,
        \Frame\Quick\Cron\HeavyImporter $heavyimporter,
        \Frame\Quick\Cron\LightImporter $lightimporter
	) {
		 parent::__construct($context);
		 $this->resultPageFactory = $resultPageFactory;
		 //$this->_products_importer = new Product($productRep,$productFactory,$productResource);
		 $this->_importer = $importer;
		 $this->_coreRegistry = $coreRegistry;
		 $this->_heavyimporter = $heavyimporter;
         //helpers
         $this->_helpers = $helpers;
         $this->_light_importer = $lightimporter;
         $this->_import_type =  $this->_helpers->getModuleConfig("general","import_type");

	}

	/**
	 * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute() {
		if($this->_import_type == "inventory"){
		    $this->_heavyimporter->execute(null);
		    //$this->_light_importer->execute(null, true);
        } else {
            $this->_heavyimporter->execute();
            //$this->_heavyimporter->execute();
        }
        return  $resultPage = $this->resultPageFactory->create();
	}
  }
