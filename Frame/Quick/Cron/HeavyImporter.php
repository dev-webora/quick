<?php

namespace Frame\Quick\Cron;

use False\MyClass;

ini_set("memory_limit", "5000M");
set_time_limit(18000);
ini_set("max_input_time",-1);
ini_set("post_max_size","122M");

class HeavyImporter 
{
    protected $_logger;
    protected $_importer;
    protected $_coreRegistry;
    //helper funciotns
    protected $_helpers;
    protected $_custom_query_enabled;
    protected $_custom_query_ref;
    protected $_custom_query_inv;


    public function __construct(
       \Psr\Log\LoggerInterface $logger,
	   \Frame\Quick\Model\Import\Product $importer,
      //Helper functions
       \Frame\Quick\Helper\Data $helpers,
       \Magento\Framework\Registry $coreRegistry
   ) 
    {
        $this->_logger = $logger;
        $this->_importer = $importer;
        $this->_coreRegistry = $coreRegistry;
        //helpers
        $this->_helpers = $helpers;
        $this->_custom_query_enabled =  $this->_helpers->getModuleConfig("custom_query", "enable");
        $this->_custom_query_ref =  $this->_helpers->getModuleConfig("custom_query", "query_ref");
        $this->_custom_query_inv =  $this->_helpers->getModuleConfig("custom_query", "query_inv");
    }

   public function execute(\Magento\Cron\Model\Schedule $schedule = null)
   {
       $api_pieces['url'] = $this->_helpers->getModuleConfig("api", "url");
       $api_pieces['safetykey'] = $this->_helpers->getModuleConfig("api", "safetykey");
       $api_pieces['tableinventory'] = $this->_helpers->getModuleConfig("api", "tableinventory");
       $api_pieces['timediff'] = $this->_helpers->getModuleConfig("api", "timediff");
       $api_pieces['tablereferences'] = $this->_helpers->getModuleConfig("api", "tablereferences");
       $sku_conf_map = $this->_helpers->getModuleConfig("basicfields", "skuconf");


       $api_url_final_inventory = $api_pieces['url'] . '/table?NOME=' . $api_pieces['tableinventory'];
       $api_url_final_references = $api_pieces['url'] . '/table?NOME=' . $api_pieces['tablereferences'];
       if ($api_pieces['safetykey']) {
           $api_url_final_inventory = $api_url_final_inventory . '&safekey=' . $api_pieces['safetykey'];
           $api_url_final_references = $api_url_final_references . '&safekey=' . $api_pieces['safetykey'];
       }
       $f = fopen("/home/framenet/public_html/fantasia_marketplace/m234/import_log.log", "a");
       $data_inizio = date("Y-m-d H:i:s");
       //$api_url_final_references = $api_url_final_references . '&first=1';


       //$f = fopen("heavy.log","wb");
       $data_inizio = date("Y-m-d H:i:s");
       echo "inizio: ".$data_inizio;
       fwrite($f, "[$data_inizio] - Complete (heavy) Import\n");

       $data_inizio = date("Y-m-d H:i:s");
       fwrite($f, "[$data_inizio] downloading\n");

       /*$inventory_file = file_get_contents("inventory.json");
       $references_file = file_get_contents("references.json");

       $json_i = $inventory_file;
       $json_i = preg_replace('/[[:cntrl:]]/', '', $json_i);
       $products_inventory = json_decode($json_i, true);

       $json_r = $references_file;
       $json_r = preg_replace('/[[:cntrl:]]/', '', $json_r);
       $products_references = json_decode($json_r, true);*/

       $skipapi = false;
       if($skipapi !== true){
           $api_info = array();

           //$api_info[]['url'] = "http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_references_day&WHERE=DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=2";
           //$api_info[]['url'] = "http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_references_day";
           //$api_info[]['url'] = "http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_inventory_day&WHERE=DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=2";
           //$api_info[]['url'] = "http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_inventory_day";
           if($this->_custom_query_enabled){
                //$api_info[]['url'] = "http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_references_day&WHERE=DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=2";
               $api_info[]['url'] = $this->_custom_query_ref;
               $api_info[]['url'] = $this->_custom_query_inv;
           } else {
               $api_info[]['url'] = $api_url_final_references;
               $api_info[]['url'] = $api_url_final_inventory;
           }
           $ch = array();
           $mh = curl_multi_init();
           foreach ($api_info as $id => $data) {
               $ch[$id] = curl_init();
               curl_setopt($ch[$id], CURLOPT_URL, $data['url']);
               curl_setopt($ch[$id], CURLOPT_CUSTOMREQUEST, "GET");
               curl_setopt($ch[$id], CURLOPT_HEADER, 0);
               curl_setopt($ch[$id], CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch[$id], CURLOPT_TRANSFERTEXT, true);
               curl_setopt($ch[$id], CURLOPT_BINARYTRANSFER, true);
               curl_multi_add_handle($mh, $ch[$id]);
           }

           do {
               try {
                   $status = curl_multi_exec($mh, $active);
                   //echo $active."<br>";
                   if ($active) {
                       curl_multi_select($mh);
                   }
               } catch (\Exception $e) {
                   //echo "request error";
                   return $this;
               }

               //} while ($active && $status == CURLM_OK);
           } while ($active);

           $token = curl_multi_getcontent($ch[0]);
           $json = $token;
           $json = str_replace('\'', '', $json);
           $json = str_replace('\\"', ' ', $json);
           $json = str_replace('\\r', ' ', $json);
           $json = str_replace('\\n', ' ', $json);
           $json = str_replace('\\', '/', $json);
           $json = preg_replace('/[[:cntrl:]]/', '', $json);
           $products_references = json_decode($json, true);
           fwrite($f, "[$data_inizio] REF: ".count($products_references)."\n");
           echo "ref count: ".count($products_references)."<br>";
           //echo count($products_references);
           //echo $json;
           $token = curl_multi_getcontent($ch[1]);
           $json = $token;
           $json = preg_replace('/[[:cntrl:]]/', '', $json);
           $products_inventory = json_decode($json, true);
           $data_inizio = date("Y-m-d H:i:s");
           fwrite($f, "[$data_inizio] INV:".count($products_inventory)."\n");
           echo "inv count: ".count($products_inventory)."<br>";
           fwrite($f, "[$data_inizio] end downloading\n");

           //close the handles
           curl_multi_remove_handle($mh, $ch[0]);
           curl_multi_remove_handle($mh, $ch[1]);
           curl_multi_close($mh);
       } else {
           //TODO: ELIMINARE || SOLO DEBUG
           $json_ref = file_get_contents("app/code/Frame/Quick/Cron/references.json");
           $json_ref = json_decode($json_ref, true);
           $products_references = $json_ref;
           if(count($products_references) != 0){
               if(stripos($this->_custom_query_ref, "200") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 200\n");
                   echo $new_ref = str_replace("200", "201", $this->_custom_query_ref);
                   echo $new_inv = str_replace("200", "201", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "201") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 201\n");
                   echo $new_ref = str_replace("201", "217", $this->_custom_query_ref);
                   echo $new_inv = str_replace("201", "217", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "217") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 217\n");
                   echo $new_ref = str_replace("217", "393", $this->_custom_query_ref);
                   echo $new_inv = str_replace("217", "393", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "393") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 393\n");
                   echo $new_ref = str_replace("393", "7329", $this->_custom_query_ref);
                   echo $new_inv = str_replace("393", "7329", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "7329") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 7329\n");
                   echo $new_ref = str_replace("7329", "7331", $this->_custom_query_ref);
                   echo $new_inv = str_replace("7329", "7331", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "7331") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 7331\n");
                   echo $new_ref = str_replace("7331", "7332", $this->_custom_query_ref);
                   echo $new_inv = str_replace("7331", "7332", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "7332") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 7332\n");
                   echo $new_ref = str_replace("7332", "7395", $this->_custom_query_ref);
                   echo $new_inv = str_replace("7332", "7395", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else if(stripos($this->_custom_query_ref, "7395") !== false){
                   fwrite($f, "[$data_inizio] IDSEASON 7395\n");
                   echo $new_ref = str_replace("7395", "7396", $this->_custom_query_ref);
                   echo $new_inv = str_replace("7395", "7396", $this->_custom_query_inv);
                   echo "<br>";
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", $new_ref);
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", $new_inv);
               } else {
                   fwrite($f, "[$data_inizio] IDSEASON 7396\n");
                   $this->_helpers->setModuleConfig("custom_query", "query_ref", "");
                   $this->_helpers->setModuleConfig("custom_query", "query_inv", "");
               }
           }
           $json_inv = file_get_contents("app/code/Frame/Quick/Cron/inventory.json");
           $json_inv = json_decode($json_inv, true);
           $products_inventory = $json_inv;
       }
		
		$data_inizio = date("Y-m-d H:i:s");
		fwrite($f, "[$data_inizio] Merging tables\n");
		if(!$products_references || !$products_inventory){
			$products_array = NULL;
		}else{
			$products_references_dict = array();
			$header_references = '';
			foreach($products_references as $index=>$prod){
				$products_references_dict[$prod[$sku_conf_map]] = $prod;
				//$products_references_dict[$prod['CODE']] = $prod;
				if($header_references == '')
					$header_references = array_keys($prod);
			}

			$products_array = array();
			foreach($products_inventory as $index => $prod){
				if(array_key_exists(trim($prod[$sku_conf_map]), $products_references_dict)){
				//if(array_key_exists(trim($prod['CODE']), $products_references_dict)){
					$additional_fields = array_combine($header_references, $products_references_dict[$prod[$sku_conf_map]]);
					//$additional_fields = array_combine($header_references, $products_references_dict[$prod['CODE']]);
					$products_array[] = $prod + $additional_fields;
				}
			}

		}
		$data_inizio = date("Y-m-d H:i:s");
		fwrite($f, "[$data_inizio] End Merging tables\n");

		/*if(!$products_array){
            fwrite($f, "[$data_inizio] no products to import - it might be an error in getting products from Quickweb - \n");
            //fwrite($f, "QuickWeb answer:".print_r($json,2));
            fclose($f);
            return $this;
		}*/

		try{
            $res = $this->_importer->runImport($products_array,"create");
        } catch (\Exception $e){
            $data_inizio = date("Y-m-d H:i:s");
            fwrite($f, "[$data_inizio] ".$e->getMessage()."\n");
        }

        $data_inizio = date("Y-m-d H:i:s");
        fwrite($f, "[$data_inizio] End Complete Import\n");
       return $this;
        try {
            $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
            if(stripos($url->getCurrentUrl(),"///") !== false){
                $schedule->setMessages("IMPORTED");
                //$schedule->setMessages("Complete import successfully executed");
                $schedule->save();
            }
        }catch (\Exception $e){
            //scrivi nel log
            return $this;
        }
		fclose($f);

       return $this;
   }
}