<?php

namespace Frame\Quick\Model\Config\Source;

class Encoding implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory)
    {
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr = array(
        array(
            'value' => 'no_conv',
            'label' => 'Non Convertire',
        ),
        array(
            'value' => 'UTF-8',
            'label' => 'UTF-8',
        ),

        array(
            'value' => 'UTF-16',
            'label' => 'UTF-16',
        ),

        array(
            'value' => 'UTF-32',
            'label' => 'UTF-32',
        ),
        array(
            'value' => 'UTF-7',
            'label' => 'UTF-7',
        ),
        array(
            'value' => 'ASCII',
            'label' => 'ASCII',
        ),
        array(
            'value' => 'ISO-8859-1',
            'label' => 'ISO-8859-1',
        ),
        array(
            'value' => 'ISO-8859-2',
            'label' => 'ISO-8859-2',
        ),
        array(
            'value' => 'ISO-8859-3',
            'label' => 'ISO-8859-3',
        ),
        array(
            'value' => 'ISO-8859-4',
            'label' => 'ISO-8859-4',
        ),
        array(
            'value' => 'ISO-8859-5',
            'label' => 'ISO-8859-5',
        ),
        array(
            'value' => 'ISO-8859-6',
            'label' => 'ISO-8859-6',
        ),
        array(
            'value' => 'ISO-8859-7',
            'label' => 'ISO-8859-7',
        ),
        array(
            'value' => 'ISO-8859-8',
            'label' => 'ISO-8859-8',
        ),

        array(
            'value' => 'ISO-8859-9',
            'label' => 'ISO-8859-9',
        ),

        array(
            'value' => 'ISO-8859-10',
            'label' => 'ISO-8859-10',
        ),

        array(
            'value' => 'ISO-8859-13',
            'label' => 'ISO-8859-13',
        ),

        array(
            'value' => 'ISO-8859-14',
            'label' => 'ISO-8859-14',
        ),
        array(
            'value' => 'ISO-8859-15',
            'label' => 'ISO-8859-15',
        ),
        array(
            'value' => 'BASE64',
            'label' => 'BASE64',
        ));
        return $arr;
    }
}