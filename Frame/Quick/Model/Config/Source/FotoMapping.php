<?php

namespace Frame\Quick\Model\Config\Source;

class FotoMapping implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;
    private $_serialize;
    private $_helpers;
    private $_foto_name;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory) {
        $object_manager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_serialize = $object_manager->get('\Magento\Framework\Serialize\Serializer\Json');
        $this->_helpers = $object_manager->get('\Frame\Quick\Helper\Data');
        try{
            $this->_foto_name = $this->_serialize->unserialize($this->_helpers->getModuleConfig("descriptionfields","foto_mapping"));
        }catch(\Exception $e){
            $this->_foto_name = NULL;
        }
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr = array();
        if(!empty($this->_foto_name)){
            foreach ($this->_foto_name as $key => $value){
                //$images_order[$value['order']] = $value['name'];
                $arr[$value['order']] = ['value' => $value['name'], 'label' => $value['name']];
            }
        }
        /*foreach ($images_order as $key => $val){
            $arr[] = ['value' => $val, 'label' => $val];
        }*/

        /*
        $arr =  [
            ['value' => 'FOTO', 'label' => __('FOTO')],
            ['value' => 'PHOTO', 'label' => __('PHOTO')]
        ];*/
        return $arr;
    }
}