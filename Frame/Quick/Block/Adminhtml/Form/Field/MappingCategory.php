<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

class MappingCategory extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * @var $_attributesRenderer \Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    protected $_activation;
    private $_helper;


    protected function _getActivationRenderer()
    {
        if (!$this->_activation) {
            $this->_activation = $this->getLayout()->createBlock(
                '\Frame\Quick\Block\Adminhtml\Form\Field\ActivationCategory',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->_activation;
   }

    protected function _prepareToRender()
    {
        $this->addColumn('name', ['label' => __('Campo Categoria Quick'), 'style' => 'width: 100px']);
        $this->addColumn(
            'category_level',
            [
                'label' => __('Campo Magento'),
                'renderer' => $this->_getActivationRenderer()
            ]
        );
        $object_manager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_helper = $object_manager->get('\Frame\Quick\Helper\Data');
        $stores_array = $this->_helper->getStoreViews(false);

        foreach ($stores_array as $code => $name){
            $this->addColumn($code, ['label' => __($name), 'style' => 'width: 100px']);
        }

        //$this->addColumn('update', ['label' => __('Update') , 'renderer' => $this->_getActivationRendererCheckbox()]);
		//$this->addColumn('update', ['label' => __('Update') , 'renderer' => $this->_getActivationRendererUpdate()]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object.
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $customAttribute = $row->getData('category_level');

        $key = 'option_' . $this->_getActivationRenderer()->calcOptionHash($customAttribute);
        $options[$key] = 'selected="selected"';
        $row->setData('option_extra_attrs', $options);
    }
}