<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

class MarketplaceAttribute extends \Magento\Framework\View\Element\Html\Select
{

    protected $_attributeFactory;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        array $data = [],
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory
    )
    {

        parent::__construct($context, $data);

        $this->_attributeFactory = $attributeFactory;

    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function _toHtml()
    {

        if (!$this->getOptions()) {

            $boolean_list = array();

            $attributeInfo = $this->_attributeFactory->getCollection();

            foreach ($attributeInfo as $attribute) {

                if ($attribute->getFrontendInput() == "boolean") {
                    $boolean_list[] = ['value' => $attribute->getAttributeCode() , 'label'=> $attribute->getFrontendLabel()];
                }

            }

            foreach ($boolean_list as $attr) {
                $this->addOption($attr['value'], $attr['label']);
            }

        }

        $this->setExtraParams('style="width:150px;"');

        return parent::_toHtml();
    }
}