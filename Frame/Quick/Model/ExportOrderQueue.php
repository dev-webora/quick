<?php
namespace Frame\Quick\Model;
class ExportOrderQueue extends \Magento\Framework\Model\AbstractModel{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct() {
        parent::_construct();
        $this->_init('Frame\Quick\Model\ResourceModel\ExportOrderQueue');
        $this->setIdFieldName('id');
    }
}
 ?>