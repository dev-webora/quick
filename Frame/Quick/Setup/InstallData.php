<?php
namespace Frame\Quick\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

	public function __construct()
	{
        
	}

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install messages https://devdocs.magento.com/videos/fundamentals/add-a-new-table-to-database/
         */
        $data = [
            ['magento_order_id' => 1],
            ['magento_order_number' => 1],
            ['http_error_code' => 404],
            ['created_at' => date('Y/m/d H:i:s')],
            ['status' => 'error']
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
            ->insertForce($setup->getTable('order_queue'), $bind);
        }
}
}