<?php

namespace Frame\Quick\Model;
use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

class Cronologia extends AbstractModel {

    protected $_id;
    protected $_created_at;
    protected $_updated_at;
    protected $_tipo;
    protected $_contenuto;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct() {
        parent::_construct();
        $this->_init('Frame\Quick\Model\ResourceModel\Cronologia');
        $this->setIdFieldName('id');
    }
}
