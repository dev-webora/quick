<?php

namespace Frame\Quick\Console\Command;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

require_once BP . "/lib/internal/FrameImport/frame/magmiimport.php";

class ImportImages extends Command
{
    const CSV = 'csvfile';
    const SEPARATOR = 'separator';
    /**
     * @var \Frame\Quick\Helper\Data
     */
    protected $_quickHelper;
    protected $_productRepository;
    protected $_imageProcessor;
    protected $_appState;

    public function __construct(
        \Frame\Quick\Helper\Data $quickHelper,
        ProductRepository $productRepository,
        \Magento\Catalog\Model\Product\Gallery\Processor $imageProcessor,
        State $appState,
        $name = null
    ) {
        $this->_quickHelper = $quickHelper;
        $this->_productRepository = $productRepository;
        $this->_imageProcessor = $imageProcessor;
        $this->_appState = $appState;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('webora:import:images');
        $this->setDescription('Webora import Images');
        $array = [
            new InputOption(self::CSV, null, InputOption::VALUE_REQUIRED, 'File CSV da elaborare'),
            new InputOption(self::SEPARATOR, null, InputOption::VALUE_REQUIRED, 'Separatore di campo')
        ];
        $this->setDefinition(
            new InputDefinition($array)
        );
        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $csv_file = BP . "/" . $input->getOption(self::CSV);
        $delimiter = $input->getOption(self::SEPARATOR);

        $idSuper_array = $this->csvToIdSuper($csv_file, $delimiter);

        $this->_appState->emulateAreaCode(
            \Magento\Framework\App\Area::AREA_ADMINHTML,
            [$this, 'ExecuteCallback'],
            [$idSuper_array , $output]
        );
    }

    public function ExecuteCallback($idSuper_array, $output)
    {
        $this->removeImagesFromProducts($idSuper_array);

        $quick_array = $this->callToQuick($idSuper_array);

        ImportProducts($quick_array, "update");

        $output->writeln("<info>Immagini importate con successo</info>");
    }

    private function removeImagesFromProducts(array $idSuper_array)
    {
        foreach ($idSuper_array as $idSuper) {
            $product = $this->_productRepository->get($idSuper, true);

            foreach ($product->getTypeInstance()->getUsedProducts($product) as $simple) {
                $mediaGallery = $simple->getMediaGalleryEntries();
                foreach ($mediaGallery as $key => $entry) {
                    unset($mediaGallery[$key]);
                    print_r($entry->getFile() . "\n");
                    if (file_exists(BP . "/pub/media/catalog/product" . $entry->getFile())) {
                        unlink(BP . "/pub/media/catalog/product" . $entry->getFile());
                    }
                }
                $simple->setMediaGalleryEntries($mediaGallery);
                $simple->save();
            }

            $mediaGallery = $product->getMediaGalleryEntries();

            foreach ($mediaGallery as $key => $entry) {
                unset($mediaGallery[$key]);
                print_r($entry->getFile() . "\n");
                if (file_exists(BP . "/pub/media/catalog/product" . $entry->getFile())) {
                    unlink(BP . "/pub/media/catalog/product" . $entry->getFile());
                }
            }
            $product->setMediaGalleryEntries($mediaGallery);
            $product->save();
        }
    }

    private function csvToIdSuper(string $csv_file, string $delimiter) : array
    {
        $handle = fopen($csv_file, "r");

        $result_array = [];

        while (($line = fgetcsv($handle, 1024, $delimiter)) !== false) {
            $result_array[] = $line[0];
        }

        return $result_array;
    }

    private function callToQuick(array $idSuper_array) : array
    {
        $baseQuickUrl = $this->_quickHelper->getModuleConfig("api", "url");
        $safekey = $this->_quickHelper->getModuleConfig("api", "safetykey");
        //$referencesTable = $this->_quickHelper->getModuleConfig("api", "tablereferences");
        $referencesTable = "FRAME_references_all";

        $attribute_set = $this->_quickHelper->getModuleConfig("general", "magattributeset");
        $configurale_attribute_code = $this->_quickHelper->getConfigurableAttributeCode();

        $item = [];

        foreach ($idSuper_array as $idSuper) {
            $finalUrl = $baseQuickUrl . "/table?NOME=" . $referencesTable . "&safekey=" . $safekey . "&WHERE=CODE=" . $idSuper;

            $jsonArray = $this->getJsonFromQuick($finalUrl);

            foreach ($jsonArray as $product) {
                $prodotto = $this->_productRepository->get($product['CODE']);

                $gallery = $this->getPhotos($product);

                $simples_skus = [];

                foreach ($prodotto->getTypeInstance()->getUsedProducts($prodotto) as $simple) {
                    $simples_skus[] = $simple->getSku();
                    $item[] = [
                        'attribute_set' => $attribute_set,
                        'sku' => $simple->getSku(),
                        'type' => 'simple',
                        'image' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                        'small_image' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                        'thumbnail' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                        'media_gallery' => $gallery
                    ];
                }

                $item[] = [
                    'attribute_set' => $attribute_set,
                    'sku' => $prodotto->getSku(),
                    'type' => 'configurable',
                    'image' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                    'small_image' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                    'thumbnail' => (!empty($product['PHOTO1']) && !is_null($product['PHOTO1'])) ? BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($product['PHOTO1']) : '__MAGMI_IGNORE__',
                    'media_gallery' => $gallery,
                    "configurable_attributes" => $configurale_attribute_code,
                    'simples_skus' => implode(",", $simples_skus)
                ];
            }
        }

        return $item;
    }

    private function getPhotos(array $prodotto) : string
    {
        $photos = [];

        for ($i = 1; $i <=8; $i++) {
            if (isset($prodotto["PHOTO$i"]) && !is_null($prodotto["PHOTO$i"])) {
                $tmpImg = BP . "/pub/media/catalog/product/f/o/foto_" . strtolower($prodotto["PHOTO$i"]);
                $this->downloadImage($prodotto["PHOTO$i"], $tmpImg);
                $photos[] = $tmpImg;
            }
        }

        return implode(";", $photos);
    }

    private function getJsonFromQuick(string $url) : array
    {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TRANSFERTEXT, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

        $references = curl_exec($ch);

        $references = preg_replace('/[[:cntrl:]]/', '', $references);

        return json_decode($references, true);
    }

    private function downloadImage($image, $destination)
    {
        $url = "http://apifantasia.teknosis.link:4567/foto?" . $image;

        $file_headers = @get_headers($url);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $imgcontent = "";
        } else {
            $imgcontent = file_get_contents($url);
        }
        file_put_contents($destination, $imgcontent);
    }
}
