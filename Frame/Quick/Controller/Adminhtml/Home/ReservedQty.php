<?php

namespace Frame\Quick\Controller\Adminhtml\Home;

class ReservedQty extends \Magento\Backend\App\Action
{
    protected $_reserved_qty;
    protected $_helpers;
    private $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Frame\Quick\Cron\ReservedQty $reservedQty
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_reserved_qty = $reservedQty;

    }

    public function execute()
    {
        $this->_reserved_qty->execute();
        return $resultPage = $this->resultPageFactory->create();
    }
}
?>