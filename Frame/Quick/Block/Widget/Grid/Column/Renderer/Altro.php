<?php

namespace Frame\Quick\Block\Widget\Grid\Column\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Framework\Registry;

class Altro extends AbstractRenderer
{
    protected $registry;
    protected $attributeFactory;

    public function __construct(
        Registry $registry,
        AttributeFactory $attributeFactory,
        Context $context,
        array $data = array()
    )
    {
        $this->attributeFactory = $attributeFactory;
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function _getValue(\Magento\Framework\DataObject $row)
    {
        $value =  parent::_getValue($row);
        $attr = json_decode($value, true);

        $html = "<div>
                    <a href=\"#\" id=\"{$row->getId()}\">
                        <button type=\"button\" class=\"action\">Cambiamenti</button>
                    </a>
                </div>
                <div id=\"popup-{$row->getId()}\" style=\"display:none;\">
                    <!--<h1>Nome Attributo - Attributo vecchio - Attributo Nuovo</h1>-->
                    <table>
                    <tbody>
                    <tr>
                        <td style='width: 400px; border: 1px solid #ccc;'><h1 style='text-align: center;'>Nome Attributo</h1></td>
                        <td style=\"width: 400px; border: 1px solid #ccc;\"><h1 style=\"color:darkred; text-align: center;\">Valore Vecchio</h1></td>
                        <td style=\"width: 400px; border: 1px solid #ccc;\"><h1 style=\"color:blue; text-align: center;\">Valore Nuovo</h1></td>
                    </tr>";
        foreach ($attr as $nome_attr => $val) {
            $html .= "<tr>";
            $html .= "<td style='border: 1px solid #ccc;'><h2 style='text-align: center;margin-top: 10px;'>".$nome_attr."</h2></td>";
            $html .= " <td style=\"border: 1px solid #ccc;\"><h2 style=\"color:darkred; text-align: center;margin-top: 10px;\">".$val['da']."</h2></td>";
            $html .= " <td style=\"border: 1px solid #ccc;\"><h2 style=\"color:blue; text-align: center;margin-top: 10px;\">".$val['a']."</h2></td>";
            $html .= "</tr>";
        }
        $html .= "</tbody></table></div>";

        $html .= "<script>
                require(
                    [
                        'jquery',
                        'Magento_Ui/js/modal/modal'
                    ],
                    function(
                        $,
                        modal
                    ) {
                        var options = {
                            type: 'popup',
                            responsive: true,
                            innerScroll: true,
                            title: 'Altri Campi',
                            buttons: [{
                                text: $.mage.__('Ok'),
                                class: '',
                                click: function () {
                                    this.closeModal();
                                }
                            }]
                        };
            
                        var popup = modal(options, $('#popup-{$row->getId()}'));
                        $(\"#{$row->getId()}\").on('click',function(){ 
                            $(\"#popup-{$row->getId()}\").modal(\"openModal\");
                        });
            
                    }
                );
            </script>";



        return $html;
    }
}