<?php
namespace Frame\Quick\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container;

class Logger extends Container {

    protected $_controller;
    protected $_blockGroup;
    protected $_headerText;
    protected $_addButtonLabel;

    protected function _construct()
    {
        $this->_controller = 'adminhtml_logger';
        $this->_blockGroup = 'quickimport';
        $this->_headerText = __('Logger');
        //$this->_addButtonLabel = __('Aggiungi');
        parent::_construct();
        $this->buttonList->remove('add'); //to remove add button
    }
}