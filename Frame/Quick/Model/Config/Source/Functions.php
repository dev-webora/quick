<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class Functions implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct() {

    }

    public function toOptionArray()
    {
        $arr = array();

        $arr[] = ['label' => "Import Categorie" , 'value' => "categories"];
        $arr[] = ['label' => "Import Immagini" , 'value' => "images"];
        $arr[] = ['label' => "Import MultiViste" , 'value' => "views"];
        $arr[] = ['label' => "Componi Titolo" , 'value' => "title"];

        return $arr;
    }
}
