<?php
namespace Frame\Quick\Block;
class Display extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function sayHello()
	{
		return __('Welcome!');
	}
	
	/*public function importButton()
	{
		//$button = '<input class="button" style="margin-left:150px" type="button" value="import" onclick="window.location.reload()">';
		$url = $this->getUrl('quickimport/import');
		//$button = '<input class="button" style="margin-left:150px" type="button" value="import" onclick="setLocation("'.$url.'")>';
		$button = '<input class="button" style="margin-left:150px" type="button" value="import"  onclick="window.location.href='.$this->escapeHtml($url).'">';
		return __($button);
	}*/
	
	 public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
	
}