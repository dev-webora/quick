<?php

namespace Frame\Quick\Controller\Adminhtml\Cronologia;

use \Magento\Framework\App\Response\RedirectInterface;

class Delete extends \Magento\Backend\App\Action
{
    protected $_pagefactory;
    private $_cronologia;
    protected $resultFactory;
    protected $_messageManager;
    private $coreRegistry;
    protected $_redirect;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        RedirectInterface $redirect,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory

    )
    {
        //$this->resultFactory = $context->getResultFactory();
        $this->_pagefactory = $resultPageFactory;
        $this->_cronologia = $cronologiaFactory;
        $this->coreRegistry = $coreRegistry;
        $this->_redirect = $redirect;
        $this->_messageManager = $messageManager;
        return parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/quickimport_logger_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $row = $this->getRequest()->getPostValue();
        $ids = $row['id'];
        foreach ($ids as $id){
            $this->_cronologia->create()->load($id)->delete();
        }
        if(count($ids) == 1){
            $this->_messageManager->addSuccess(__("Eliminata 1 riga!"));
        } else {
            $this->_messageManager->addSuccess(__("Eliminate ").count($ids).__(" righe!"));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        //$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
