<?php

  namespace Frame\Quick\Controller\Adminhtml\Home;

  class ImportTotal extends \Magento\Backend\App\Action
  {
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	protected $resultPageFactory;
	protected $_importer;
	protected $_coreRegistry;
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	protected $dp;
    protected $_import_type;
    //helper funciotns
    protected $_helpers;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $coreRegistry,
        //Helper functions
        \Frame\Quick\Helper\Data $helpers,
        \Frame\Quick\Cron\ImportTotal $importer
	) {
		 parent::__construct($context);
		 $this->resultPageFactory = $resultPageFactory;
		 $this->_importer = $importer;
		 $this->_coreRegistry = $coreRegistry;
         //helpers
         $this->_helpers = $helpers;
	}

	/**
	 * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute() {
	    $this->_importer->execute();

        return  $resultPage = $this->resultPageFactory->create();
	}
  }
