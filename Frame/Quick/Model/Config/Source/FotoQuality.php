<?php
namespace Frame\Quick\Model\Config\Source;

class FotoQuality implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $arr = array();
        for($i=10; $i<=100; $i+=10){
            $arr[] = ['value' => $i, 'label' =>$i."%"];
        }
        return $arr;
    }
}