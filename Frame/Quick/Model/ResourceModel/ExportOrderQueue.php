<?php
namespace Frame\Quick\Model\ResourceModel;
class ExportOrderQueue extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct(){
        $this->_init("order_queue","queue_id");
    }
}
?>