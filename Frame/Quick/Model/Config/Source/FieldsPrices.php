<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;


class FieldsPrices implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @return array
     */

    protected $_attributeFactory;
    protected $eavTypeFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory,
        \Magento\Eav\Model\Entity\Attribute $attribute,
        \Magento\Eav\Model\Entity\TypeFactory $typeFactory
    ) {
        //parent::__construct($context);
        $this->_attributeFactory = $attributeFactory;
        $this->eavTypeFactory = $typeFactory;
    }

    public function toOptionArray()
    {
        $arr = [];

        $entityType = $this->eavTypeFactory->create()->loadByCode('catalog_product');
        $collection = $this->_attributeFactory->create()->getCollection();
        $collection->addFieldToFilter('entity_type_id', $entityType->getId());
        $collection->setOrder('attribute_code');

        /** @var Attribute $attribute */
        foreach ($collection as $attribute) {
            if ($attribute->getFrontendInput() == "price")
                $arr[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }
        ksort($arr);
        return $arr;

    }
}
