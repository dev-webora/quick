<?php

namespace Frame\Quick\Controller\ExportOrder;
use Frame\Quick\Model\ExportOrderQueueFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\Response\Http\FileFactory;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Controller\ResultFactory;
//use \Magento\Directory\Model\ResourceModel\Country\Collection;
class Delete extends \Magento\Framework\App\Action\Action {

    protected $fileFactory;	
	private $_ordini_tab;
	private $_detordini_tab;
	private $_api;
	protected $_objectManager;

    public function __construct(
        Context $context,
		CollectionFactory $collectionFactory,
		OrderRepositoryInterface $rep,
		FileFactory $fileFactory,
		ScopeConfigInterface $scopeConfig,
		RegionFactory $region,
		InvoiceSender $invoiceSender,
		ExportOrderQueueFactory $exportOrderQueue
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
		$this->_invoiceSender = $invoiceSender;  
		$this->rep = $rep;  
		$this->fileFactory = $fileFactory;
		$this->scopeConfig = $scopeConfig;
		$this->region = $region;
		$this->_exportOrderQueue = $exportOrderQueue;
		$this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$conf = $this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
		$prova = $conf->getValue('quick/export/ordiniweb');

		$this->_ordini_tab = $conf->getValue('quick/export/ordiniweb');
		$this->_detordini_tab = $conf->getValue('quick/export/detordiniweb');
		$this->_api =$conf->getValue('quick/api/url');
    }

    public function execute() {
        
    }

}
