<?php

namespace Frame\Quick\Controller\Adminhtml\Logger;

class Index extends \Magento\Backend\App\Action
{
    protected $_pagefactory;
    protected $_loggerFactory;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Frame\Quick\Model\Logger $loggerFactory

    )
    {
        $this->_pagefactory = $resultPageFactory;
        $this->_loggerFactory = $loggerFactory;
        return parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/quickimport_logger_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $resultPage = $this->_pagefactory->create();
        /*$resultPage->getConfig()->getTitle()->prepend(__('Grid Title'));
        $resultPage->setActiveMenu('Frame_Quick::gridList_name');
        $resultPage->addBreadcrumb(__('Grid Name Process'), __('Grid Name List'));
        $this->_addContent($this->_view->getLayout()->createBlock('Frame\Quick\Block\Adminhtml\Logger\Grid'));
        $this->_view->renderLayout();*/
        return $resultPage;
    }
}
