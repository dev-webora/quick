<?php

namespace Frame\Quick\Controller\Adminhtml\MassOrder;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Frame\Quick\Model\Export\ExportOrder;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class MassOrder extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    private $_export_order;
    protected $collectionFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ExportOrder $export_order
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->_export_order = $export_order;
        parent::__construct($context, $filter);
    }

    protected function massAction(AbstractCollection $collection) {
        $orderIdsList = $collection->getAllIds();
        return $this->_export_order->export($orderIdsList);
    }
}