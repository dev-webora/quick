<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class IncrementalType implements \Magento\Framework\Option\ArrayInterface
{


    public function __construct() {

    }

    public function toOptionArray()
    {
        $arr = array();

        $arr[] = ['label' => "Crea nuovi prodotti e aggiorna quelli esistenti" , 'value' => "create"];
        $arr[] = ['label' => "Aggiorna solo prodotti esistenti" , 'value' => "update"];
        $arr[] = ['label' => "Crea solo nuovi prodotti" , 'value' => "xcreate"];

        return $arr;
    }
}
