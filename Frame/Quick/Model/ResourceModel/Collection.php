<?php
namespace Frame\Quick\Model\ResourceModel\ExportOrderQueue;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("Frame\Quick\Model\ExportOrderQueue","Frame\Quick\Model\ResourceModel\ExportOrderQueue");
	}
}