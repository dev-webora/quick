<?php
namespace Frame\Quick\Block\Adminhtml\Form\Field;

class MappingDizionario extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * @var $_attributesRenderer \Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    protected $_activation;
    private $_helper;

    protected function _getActivationRenderer() {
        if (!$this->_activation) {
            $this->_activation = $this->getLayout()->createBlock(
                '\Frame\Quick\Block\Adminhtml\Form\Field\SelectAttrDizionario',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_activation;
    }


    protected function _prepareToRender()
    {
        $this->addColumn(
            'attributo',
            [
                'label' => __('Attributo Magento'),
                'renderer' => $this->_getActivationRenderer()
            ]
        );
        $this->addColumn('find', ['label' => __('Trova'), 'style' => 'width: 200px']);
        $this->addColumn('replace', ['label' => __('Sostituisci con'), 'style' => 'width: 200px']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object.
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $customAttribute = $row->getData('attributo');

        $key = 'option_' . $this->_getActivationRenderer()->calcOptionHash($customAttribute);
        $options[$key] = 'selected="selected"';
        $row->setData('option_extra_attrs', $options);
    }
}