<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class MagentoAttributeSet implements \Magento\Framework\Option\ArrayInterface
{
	protected $_attributsetFactory;

	public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $collectionFactory
    ) {
        $this->_attributsetFactory = $collectionFactory;
    }
	 
    public function toOptionArray()
    {
        $type = 4; // products
		$arr = [];
        $collection = $this->_attributsetFactory->create()->addFieldToFilter('entity_type_id', $type);
        $attributesets = $collection->getItems();
        //print_r($collection);
        foreach ($attributesets as $attributeset){
            $arr[] = ['label' => $attributeset->getAttributeSetName(), 'value' => $attributeset->getAttributeSetName()];
        }
		ksort($arr);
		return $arr;
    }
}
