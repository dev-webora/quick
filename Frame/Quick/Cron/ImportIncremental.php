<?php

namespace Frame\Quick\Cron;

use Frame\Quick\Model\ImportJob;

ini_set("memory_limit", "8000M");
set_time_limit(3600);
ini_set("max_input_time", -1);
error_reporting(E_ALL);
ini_set('display_errors', 1);

class ImportIncremental extends Import
{
    protected $_cacheTypeList;
    protected $_coreRegistry;
    protected $_source_type;
    protected $_magmi_mode;
    protected $_incremental_type_fields;
    protected $_inventory_only;
    protected $_selected_functions;
    protected $importJob;

    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Frame\Quick\Model\Import\Product $importer,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        \Frame\Quick\Helper\Data $helpers,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resource,
        ImportJob $importJob
    ) {
        parent::__construct($importer, $cronologiaFactory, $helpers);
        $this->_resource = $resource;
        $this->_source_type =  $this->_helpers->getModuleConfig("general", "import_type");
        $this->_magmi_mode = $this->_helpers->getModuleConfig("general", "incremental_type_magmi");
        $this->_incremental_type_fields = $this->_helpers->getModuleConfig("general", "incremental_type_fields");
        if ($this->_incremental_type_fields == "stockprice") {
            $this->_magmi_mode = "update";
            $this->_inventory_only = true;
        } else {
            $this->_inventory_only = false;
        }

        if ($this->_incremental_type_fields == "selectedfields") {
            $selected_functions_config = $this->_helpers->getModuleConfig("general", "functions");
            $this->_selected_functions = explode(',', $selected_functions_config);
        } else {
            $this->_selected_functions = [];
        }

        $this->_coreRegistry = $registry;
        $this->_cacheTypeList = $cacheTypeList;
        $this->importJob = $importJob;
    }

    public function execute(\Magento\Cron\Model\Schedule $schedule = null, $is_totale = false)
    {
        $types = ['config','config_integration','config_webservice'];
        foreach ($types as $type) {
            $this->_cacheTypeList->cleanType($type);
        }

        $cronologia_id = $this->newCronologia("Inizio import incrementale", "Import");

        $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        // fwrite($f, "[$data_inizio] - Stock/Qty - current url:".$url->getCurrentUrl()." -\n");
        if (stripos($url->getCurrentUrl(), "///") !== false) {
            $running_jobs = $this->checkRunningJobs($schedule->getId());
            if (in_array("quick_heavy_import", array_keys($running_jobs))) {
                if ($running_jobs["quick_heavy_import"]["messages"] == "IMPORTED") {
                    $data_inizio = date("Y-m-d H:i:s");
                    // fwrite($f, "[$data_inizio] - Stock/Qty (light) Import - Found Complete import running job IMPORTED. Status Updated\n");
                    $new_status = "'success'";
                    $new_message = "'IMPORTED - Job interrupted by LightImporter, because it was still running even if the import was terminated'";
                    $query_res = $this->updateJobStatus($running_jobs["quick_heavy_import"]["schedule_id"], $new_status, null);
                } else {
                    $executed_at = date($running_jobs["quick_heavy_import"]['executed_at']);
                    $current_date = date("Y-m-d H:i:s");
                    $date = new \DateTime($executed_at);
                    $date2 = new \DateTime($current_date);
                    $diff = $date2->getTimestamp() - $date->getTimestamp();
                    if ($diff >= 10800) {
                        $new_status = "'error'";
                        $new_message = "'Job interrupted by LightImporter, because it was running for more than 1 hour'";
                        $query_res = $this->updateJobStatus($running_jobs["quick_heavy_import"]["schedule_id"], $new_status, $new_message);
                        // fwrite($f, "[$data_inizio] Complete import stopped because running for more than 1hour. LightImport starts in a while\n");
                    } else {
                        // fwrite($f, "[$data_inizio] Stock/Qty import SKIPPED - a complete import is running\n");
                        //fclose($f);
                        try {
                            $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
                            if (stripos($url->getCurrentUrl(), "http") === false) {
                                $schedule->setMessages("Stock/Qty import skipped because a complete import is running");
                                $schedule->save();
                            }
                        } catch (\Exception $e) {
                        }
                        $this->updateCronologia("Import totale in lavorazione!");
                        return $this;
                    }
                }
            }

            // check also if is running a light import
            if (in_array("quick_light_import", array_keys($running_jobs))) {
                if ($running_jobs["quick_light_import"]["messages"] == "IMPORTED") {
                    $data_inizio = date("Y-m-d H:i:s");
                    // fwrite($f, "[$data_inizio] - Stock/Qty (light) Import - Found Complete import running job IMPORTED. Status Updated\n");
                    $new_status = "'success'";
                    $new_message = "'IMPORTED - Job interrupted by LightImporter, because it was still running even if the import was terminated'";
                    $query_res = $this->updateJobStatus($running_jobs["quick_light_import"]["schedule_id"], $new_status, null);
                } else {
                    $executed_at = date($running_jobs["quick_light_import"]['executed_at']);
                    $current_date = date("Y-m-d H:i:s");
                    $date = new \DateTime($executed_at);
                    $date2 = new \DateTime($current_date);
                    $diff = $date2->getTimestamp() - $date->getTimestamp();
                    if ($diff >= 10800) {
                        $new_status = "'error'";
                        $new_message = "'Job interrupted by LightImporter, because it was running for more than 1 hour'";
                        $query_res = $this->updateJobStatus($running_jobs["quick_light_import"]["schedule_id"], $new_status, $new_message);
                        // fwrite($f, "[$data_inizio] Complete import stopped because running for more than 1hour. LightImport starts in a while\n");
                    } else {
                        // fwrite($f, "[$data_inizio] Stock/Qty import SKIPPED - a complete import is running\n");
                        //fclose($f);
                        try {
                            $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
                            if(stripos($url->getCurrentUrl(),"http") === false){
                                $schedule->setMessages("Stock/Qty import skipped because a complete import is running");
                                $schedule->save();
                            }
                        } catch (\Exception $e) {
                        }
                        $this->updateCronologia("Import incrementale in lavorazione!");
                        return $this;
                    }
                }
            }

            if (!$this->_enabled_cron) {
                $this->updateCronologia("Cron Disabilitato!");
                return $this;
            }
        }



        //composeApiInfo is inherited by Import class
        $api_url_final = $this->composeApiInfo(true, $this->_inventory_only);
        // fwrite($f, "[$data_inizio] - downloading\n");

        $this->importJob->loadAndImportProducts($api_url_final, '','',$cronologia_id);

        $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
        if(stripos($url->getCurrentUrl(),"///") !== false){
            $schedule->setMessages("IMPORTED");
            $schedule->save();
        }
        //$this->_logger->debug('Quick Light Import Cron run successfully');
        $data_inizio = date("Y-m-d H:i:s");
        //fwrite($f, "[$data_inizio] - End Stock/Qty Update\n");
        //fclose($f);
        return $this;
    }

    public function checkRunningJobs($schedule_id)
    {
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('cron_schedule'); //gives table name with prefix

        //Select Data from table
        $status = "'running'";

        $sql = "Select schedule_id, job_code, messages, executed_at, finished_at FROM " . $tableName . " where status = " . $status

            ." and schedule_id not like $schedule_id";

        $results = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
        $running_jobs = [];
        foreach ($results as $result) {
            //$running_jobs[] = $result['job_code'];
            $running_jobs[$result['job_code']]['messages'] = $result['messages'];
            $running_jobs[$result['job_code']]['executed_at'] = $result['executed_at'];
            $running_jobs[$result['job_code']]['finished_at'] = $result['finished_at'];
            $running_jobs[$result['job_code']]['schedule_id'] = $result['schedule_id'];
        }
        return $running_jobs;
    }
    public function updateJobStatus($target_id, $new_status, $message)
    {
        $connection = $this->_resource->getConnection();
        $tableName = $this->_resource->getTableName('cron_schedule'); //gives table name with prefix

        //Select Data from table
        $sql = "UPDATE " . $tableName . " SET status = " . $new_status . " WHERE schedule_id = " . $target_id;
        $result = $connection->query($sql);

        if ($message) {
            $sql = "UPDATE " . $tableName . " SET messages = " . $message . " WHERE schedule_id = " . $target_id;
            $connection->query($sql);
        }
        return $result;
    }

}
