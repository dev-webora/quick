<?php
/////////mod per mag2 - berto

//modulo sda
namespace Frame\Quick\Controller\Adminhtml\MassOrder;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
//use \Magento\Directory\Model\ResourceModel\Country\Collection;

class GenerateCSV {
	private $_orderIds;
	private $_collectionOrders;
	private $_contentCSV;
	private $_orderRepository;
	private $_scopeConfig;
		
	public function __construct($ordersId, ScopeConfigInterface $scopeConfig, OrderRepositoryInterface $orderRepository,RegionFactory $regionFactory) {
	   $this->_orderIds = $ordersId;
	   $this->_scopeConfig = $scopeConfig;
	   $this->_orderRepository = $orderRepository;
	   $this->_regionFactory = $regionFactory;
	}
	private function _loadOrderObjects() {
		$this->_collectionOrders = array ();
		$_collectionOrders = array();
				
		foreach ( $this->_orderIds as $id ) {
			$order = $this->_orderRepository->get($id);
			array_push ( $_collectionOrders, $order );
		}
		 $this->_collectionOrders = $_collectionOrders;
	}

	private function _prepareData() {
		
		$arrayNomi = array (
				"TITOLO",
				"COGNOME_RAGSOC",
				"NOME",
				"INFOUTILIXCONSEGNA",
				"VIA",
				"PIANOINTERNO",
				"CAP",
				"FRAZIONE",
				"COMUNE",
				"PROVINCIA",
				"NOTE",
				"IMPORTO_CONTRASSEGNO",
				"TAGLIA",
				"CELLULARE",
				"TELEFONO",
				"EMAIL",
				"RIFERIMENTO_CLIENTE",
				"CENTRO_COSTO",
				"MITTENTE",
				"MITTENTE_NOME",
				"MITTENTE_VIA",
				"MITTENTE_CAP",
				"MITTENTE_COMUNE",
				"MITTENTE_PROVINCIA",
				"SERVIZIO_SMS",
				"SERVIZIO_CONSEGNA_SABATO" 
		);
			$header = "";
			$this->_contentCSV ="";
			//$this->_contentCSV ="NomeCognome;;indirizzo;cap;comune;provincia;codiceNazione;telefono;;;mail;;codservizio;tipopagamento;;codiceordine;colli;peso;contrassegno;;tipocontenuto;;;;;;;;;;\n";

			foreach ( $this->_collectionOrders as $order ) {

			///dettagli sul cliente
			$idcliente = $order->getCustomerId(); 
			
			/////va preso lo ShippingAddress, ma nel magento di test gli ordini inseriti hanno solo il billingAddress/////
			$_shippingAddress = $order->getShippingAddress();
			if($_shippingAddress == NULL){
			$_shippingAddress = $order->getBillingAddress(); 
			}
			//////////////////////////////////////////////
			$NOME = $_shippingAddress->getFirstname(); 
			$COGNOME_RAGSOC = $_shippingAddress->getLastname();
			$EMAIL = $order->getCustomerEmail();
			$VIA =  $_shippingAddress->getStreet(); 
			$CAP =  $_shippingAddress->getPostcode(); 
			$COMUNE =  $_shippingAddress->getCity(); 
			$PROVINCIA = $_shippingAddress->getRegion();
			
			$country_id = $_shippingAddress->getCountryId();
			$r = $this->_regionFactory->create();
			$region = $r->loadByName($PROVINCIA, $country_id);//r è di tipo Region.
			$PROVINCIA_SIGLA = $region->getCode();
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$country = $objectManager->create('Magento\Directory\Model\ResourceModel\Country\Collection')
                        ->addFieldToFilter('country_id', ['eq' => $country_id])
                        ->getFirstItem();
			/*$country = $this->_collectionCountry->getItemById($country_id);
			echo $country->getName();*/
			$COUNTRY_CODE = $country->getIso3_code(); 
			if($COUNTRY_CODE=="ITA") $COUNTRY_CODE="";
			
		    $CELLULARE = $TELEFONO = $_shippingAddress->getTelephone(); 
			
			//dettagli pagamento
			$payment = $order->getPayment ();
		    $additionalData = $payment->getAdditionalData();
			$paymethod = $order->getPayment ()->getMethod();
					
			if ($paymethod == "Contrassegno" || strpos ( $additionalData, "Contrassegno" ) !== false || $paymethod == "cashondelivery" || strpos ( $additionalData, "cashondelivery" ) !== false) {
				$IMPORTO_CONTRASSEGNO = number_format($order->getGrandTotal(),2);
			} else {
				$IMPORTO_CONTRASSEGNO = "";
			}
					
		
		    //dettagli ordine
     		$weight = $order->getWeight(); //berto
			//$codiceordine = $order->getRealOrderId();
			$codiceordine = $order->getIncrementId();
			$RIFERIMENTO_CLIENTE = $order->getIncrementId();//non utilizzato.
			$PORTO = "F";
			$A = "0";
			$NUMERO_COLLI = "1";
			
			//valori fissi (configurazione modulo)
            $codice_servizio = $this->_scopeConfig->getValue('export/general/codice_servizio',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$tipo_pagamento = $this->_scopeConfig->getValue('export/general/tipo_pagamento',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
			$tipo_contenuto = $this->_scopeConfig->getValue('export/general/tipo_contenuto',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	
			$VIA = str_replace ( ";",' ', $VIA );
			$VIA = str_replace ( "\n",' ', $VIA );
			$VIA = str_replace ( "\t",' ', $VIA );
			$VIA = str_replace ( "\r",' ', $VIA );
			$VIA = $VIA[0];
			
			$valori = array (
			ucfirst(strtolower($NOME))." ".ucfirst(strtolower($COGNOME_RAGSOC)), //ragione sociale
			"", // referente
			$VIA , // indirizzo
			$CAP, // cap
			$COMUNE, // localita
			$PROVINCIA_SIGLA, //$PROVINCIA, // provincia
			$COUNTRY_CODE, // nazione
			$TELEFONO,
			"", //fax
			"", //cell
			$EMAIL,
			"", //id fiscale
			$codice_servizio, // codice servizio
			$tipo_pagamento, //Cod ACC
			"", //codice S
			$codiceordine,// rif. interno
			$NUMERO_COLLI,
			$weight, //peso
			$IMPORTO_CONTRASSEGNO,
			"", // metodo
			$tipo_contenuto, //tipo contenuto
			"", //DESC CONT

			"", //TIPO IMBALLO	
			"", //ALTEZZA	
			"", //LARGH COLLO 	
			"", //PROFONDITA'	
			"", //PAG. ONERI	
			"", //VALORE DIC	
			"", //ASS.	
			"" //NOTE
			);
			$header = "";
			$header = implode(";",$valori);
			$this->_contentCSV .= $header . "\n";
		}
	}

	public function call() {
		$this->_loadOrderObjects ();
		$this->_prepareData ();
		return $this->_contentCSV;
	
	}
	
	}