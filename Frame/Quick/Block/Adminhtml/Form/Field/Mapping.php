<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

use \Frame\Quick\Helper\Data;
use \Frame\Quick\Block\Adminhtml\Form\Field\Checkbox;

class Mapping extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * @var $_attributesRenderer \Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    protected $_activation;
    protected $_activation_checkbox;
    protected $_activation_update;

    private $_helper;

    /**
     * Get activation options.
     *
     * @return \Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    protected function _getActivationRenderer()
    {
        if (!$this->_activation) {
            $this->_activation = $this->getLayout()->createBlock(
                '\Frame\Quick\Block\Adminhtml\Form\Field\Activation',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->_activation;
   }
    protected function _getActivationRendererUpdate()
    {
        if (!$this->_activation_update) {
            $this->_activation_update = $this->getLayout()->createBlock(
                '\Frame\Quick\Block\Adminhtml\Form\Field\ActivationUpdate',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->_activation_update;
   }
   
       /**
     * Prepare to render.
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn('name', ['label' => __('Campo Quick'), 'style' => 'width: 100px']);
        $this->addColumn(
            'activation_attribute',
            [
                'label' => __('Campo Magento'),
                'renderer' => $this->_getActivationRenderer()
            ]
        );
        $object_manager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_helper = $object_manager->get('\Frame\Quick\Helper\Data');
        $stores_array = $this->_helper->getStoreViews(false);

        foreach ($stores_array as $code => $name){
            $this->addColumn($code, ['label' => __($name), 'style' => 'width: 100px']);
        }

        //$this->addColumn('update', ['label' => __('Update') , 'renderer' => $this->_getActivationRendererCheckbox()]);
		$this->addColumn('update', ['label' => __('Incrementale') , 'renderer' => $this->_getActivationRendererUpdate()]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object.
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $options2 = [];
        $customAttribute = $row->getData('activation_attribute');

        $key = 'option_' . $this->_getActivationRenderer()->calcOptionHash($customAttribute);
        $options[$key] = 'selected="selected"';
		
		$customAttribute = $row->getData('update');
        $key = 'option_' . $this->_getActivationRendererUpdate()->calcOptionHash($customAttribute);
		$options[$key] = 'selected="selected"';
        $row->setData('option_extra_attrs', $options);
		
		/*$customAttribute = $row->getData('update');

        $key = 'option_' . $this->_getActivationRendererUpdate()->calcOptionHash($customAttribute);
        $options2[$key] = 'selected="selected"';
        $row->setData('option_extra_attrs', $options2);*/
    }
}