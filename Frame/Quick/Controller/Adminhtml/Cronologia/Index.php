<?php

namespace Frame\Quick\Controller\Adminhtml\Cronologia;

class Index extends \Magento\Backend\App\Action
{
    protected $_pagefactory;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->_pagefactory = $resultPageFactory;
        return parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/quickimport_logger_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $resultPage = $this->_pagefactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Cronologia'));
        /*$resultPage->setActiveMenu('Frame_Quick::gridList_name');
        $resultPage->addBreadcrumb(__('Grid Name Process'), __('Grid Name List'));
        $this->_addContent($this->_view->getLayout()->createBlock('Frame\Quick\Block\Adminhtml\Logger\Grid'));
        $this->_view->renderLayout();*/
        return $resultPage;
    }
}
