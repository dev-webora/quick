<?php


namespace Frame\Quick\Helper;
use Magento\Framework\App\Helper\AbstractHelper;

class Serializer extends AbstractHelper
{
    /**
     * Unserialize value
     * @param $string
     * @param bool $associative
     * @return mixed
     */
    public function unserialize($string , $associative = true)
    {
        $array = json_decode($string , $associative);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return [];
        }
        return $array;
    }
}