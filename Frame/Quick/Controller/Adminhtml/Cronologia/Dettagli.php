<?php

namespace Frame\Quick\Controller\Adminhtml\Cronologia;

use Frame\Quick\Block\Display;

class Dettagli extends \Magento\Backend\App\Action
{
    protected $_pagefactory;
    private $_cronologia;
    private $_display;
    private $coreRegistry;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory

    )
    {
        $this->_pagefactory = $resultPageFactory;
        $this->_cronologia = $cronologiaFactory;
        $this->coreRegistry = $coreRegistry;
        return parent::__construct($context);
    }

    /**
     * Load the page defined in view/adminhtml/layout/quickimport_logger_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $rowId = (int) $this->getRequest()->getParam('id');
        if ($rowId) {
            $rowData = $this->_cronologia->create()->load($rowId)->getData();
            $arr['tipo'] = $rowData['tipo'];
            $tipo = $rowData['tipo'];
            $contenuto = json_decode($rowData['contenuto'], true);
            $arr['contenuto'] = $contenuto;
        }

        $this->_view->loadLayout();
        $this->coreRegistry->register('tipo', $tipo);
        $this->coreRegistry->register('contenuto', $contenuto);
        $resultPage = $this->_pagefactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Dettagli Cronologia'));
        return $resultPage;
    }
}
