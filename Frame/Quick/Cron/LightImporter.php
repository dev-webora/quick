<?php

namespace Frame\Quick\Cron;

class LightImporter 
{
    protected $_logger;
    protected $_importer;
    protected $_coreRegistry;
    protected $_resource;
    //helper funciotns
    protected $_helpers;
    protected $_import_type;
    protected $_codifica;
    protected $_custom_query_enabled;
    protected $_custom_query_ref;
    protected $_custom_query_inv;

    public function __construct(
       \Psr\Log\LoggerInterface $logger,
	   \Frame\Quick\Model\Import\Product $importer,
	   \Magento\Framework\Registry $coreRegistry,
      //Helper functions
       \Frame\Quick\Helper\Data $helpers,
       \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->_logger = $logger;
        $this->_importer = $importer;
        $this->_coreRegistry = $coreRegistry;
        $this->_resource = $resource;
        //helpers
        $this->_helpers = $helpers;

        $this->_import_type =  $this->_helpers->getModuleConfig("general","import_type");
        $this->_codifica = $this->_helpers->getModuleConfig("api","encoding");
        $this->_custom_query_enabled =  $this->_helpers->getModuleConfig("custom_query", "enable");
        $this->_custom_query_ref =  $this->_helpers->getModuleConfig("custom_query", "query_ref");
        $this->_custom_query_inv =  $this->_helpers->getModuleConfig("custom_query", "query_inv");
    }


   public function execute(\Magento\Cron\Model\Schedule $schedule = null, $is_totale = false)
   {
       if($this->_custom_query_enabled){
           $api_url_final = $this->_custom_query_inv;
       } else {
           $api_pieces['url'] = $this->_helpers->getModuleConfig("api", "url");
           $api_pieces['safetykey'] = $this->_helpers->getModuleConfig("api", "safetykey");
           if($is_totale){
               $api_pieces['tableinventory'] = $this->_helpers->getModuleConfig("api", "tablereferences");
               $api_pieces['timediff'] = null;
           } else {
               $api_pieces['tableinventory'] = $this->_helpers->getModuleConfig("api", "tableinventory");
               $api_pieces['timediff'] = $this->_helpers->getModuleConfig("api", "timediff");
           }

           $api_url_final = $api_pieces['url'] . '/table?NOME=' . $api_pieces['tableinventory'];
           if ($api_pieces['timediff'])
               $api_url_final = $api_url_final . '&WHERE="DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=' . $api_pieces['timediff'] . '"';
           if ($api_pieces['safetykey'])
               $api_url_final = $api_url_final . '&safekey=' . $api_pieces['safetykey'];
       }

       $ch = curl_init($api_url_final);
       //$ch = curl_init("http://apifantasia.teknosis.link:4567/table?safekey=frame&NOME=frame_inventory_day&WHERE=\"DATEDIFF( MINUTE FROM DATETIME TO CURRENT_TIMESTAMP)<=5\"");

       $f = fopen("./import_log.log", "a");
       $data_inizio = date("Y-m-d H:i:s");
       fwrite($f, "[$data_inizio] - Stock/Qty (light) Import\n");
       $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
	   fwrite($f, "[$data_inizio] - Stock/Qty - current url:".$url->getCurrentUrl()." -\n");
       //if (stripos($url->getCurrentUrl(), "http") === false) {
       if (stripos($url->getCurrentUrl(), "///") !== false) {
           $running_jobs = $this->checkRunningJobs();
           if (in_array("quick_heavy_import", array_keys($running_jobs))) {
               if ($running_jobs["quick_heavy_import"]["messages"] == "IMPORTED") {
                   $data_inizio = date("Y-m-d H:i:s");
                   fwrite($f, "[$data_inizio] - Stock/Qty (light) Import - Found Complete import running job IMPORTED. Status Updated\n");
                   $new_status = "'success'";
                   $new_message = "'IMPORTED - Job interrupted by LightImporter, because it was still running even if the import was terminated'";
                   $query_res = $this->updateJobStatus($running_jobs["quick_heavy_import"]["schedule_id"], $new_status, null);
               } else {
                   $executed_at = date($running_jobs["quick_heavy_import"]['executed_at']);
                   $current_date = date("Y-m-d H:i:s");
                   $date = new \DateTime($executed_at);
                   $date2 = new \DateTime($current_date);
                   $diff = $date2->getTimestamp() - $date->getTimestamp();
                   if ($diff >= 10800) {
                       $new_status = "'error'";
                       $new_message = "'Job interrupted by LightImporter, because it was running for more than 1 hour'";
                       $query_res = $this->updateJobStatus($running_jobs["quick_heavy_import"]["schedule_id"], $new_status, $new_message);
                       fwrite($f, "[$data_inizio] Complete import stopped because running for more than 1hour. LightImport starts in a while\n");
                   } else {
                       fwrite($f, "[$data_inizio] Stock/Qty import SKIPPED - a complete import is running\n");
                       fclose($f);
                       try {
                           $url = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
                           if(stripos($url->getCurrentUrl(),"http") === false){
                               $schedule->setMessages("Stock/Qty import skipped because a complete import is running");
                               $schedule->save();
                           }
                       } catch (\Exception $e) {
                       }
                       return $this;
                   }
               }
           }
   }

        fwrite($f, "[$data_inizio] - downloading\n");
       	//$ch = curl_init("http://apifantasia.teknosis.link:4567/table?safekey=frame&nome=frame_inventory_all&first=20");

		$ch = curl_init($api_url_final);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ordineweb));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TRANSFERTEXT, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($ordineweb))));
		$token = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//var_dump($token);
        if($this->_codifica != "no_conv"){
            $json = mb_convert_encoding($token, $this->_codifica);
        } else {
            $json = $token;
        }
		$json = preg_replace('/[[:cntrl:]]/','',$json);
		//print_r($json);
		$products = json_decode($json, true);
        $data_inizio = date("Y-m-d H:i:s");
        fwrite($f, "[$data_inizio] - importing\n");

        if(!$products){
           fwrite($f, "[$data_inizio] no products to import - it might be an error in getting products from Quickweb\n");
           fclose($f);
           return $this;
        }
        if($this->_import_type == "inventory")
            $mode = "create";
        else
            $mode = "update";

        $res = $this->_importer->runImport($products, $mode);
		$this->_coreRegistry->register('res', "Light Importer. ".$res);
	   
       //$this->_logger->debug('Quick Light Import Cron run successfully');
       $data_inizio = date("Y-m-d H:i:s");
	   fwrite($f, "[$data_inizio] - End Stock/Qty Update\n");
	   fclose($f);
       return $this;
   }

   public function checkRunningJobs(){
       $connection = $this->_resource->getConnection();
       $tableName = $this->_resource->getTableName('cron_schedule'); //gives table name with prefix

       //Select Data from table
       $status = "'running'";
       $sql = "Select schedule_id, job_code, messages, executed_at, finished_at FROM " . $tableName . " where status = " . $status;
       $results = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
       $running_jobs = array();
       foreach ($results as $result){
           //$running_jobs[] = $result['job_code'];
           $running_jobs[$result['job_code']]['messages'] = $result['messages'];
           $running_jobs[$result['job_code']]['executed_at'] = $result['executed_at'];
           $running_jobs[$result['job_code']]['finished_at'] = $result['finished_at'];
           $running_jobs[$result['job_code']]['schedule_id'] = $result['schedule_id'];

       }
       return $running_jobs;
   }
   public function updateJobStatus($target_id,$new_status,$message){
       $connection = $this->_resource->getConnection();
       $tableName = $this->_resource->getTableName('cron_schedule'); //gives table name with prefix

       //Select Data from table
       $sql = "UPDATE ". $tableName . " SET status = ". $new_status . " WHERE schedule_id = ". $target_id;
       $result = $connection->query($sql);

       if($message){
           $sql = "UPDATE ". $tableName . " SET messages = ". $message . " WHERE schedule_id = ". $target_id;
           $connection->query($sql);
       }
       return $result;
   }
}