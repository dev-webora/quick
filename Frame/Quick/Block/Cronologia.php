<?php
namespace Frame\Quick\Block;
class Cronologia extends \Magento\Framework\View\Element\Template
{
    //dettagli cronologia
    private $_tipo;
    private $_contenuto;
    private $_cronologia;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory
    )
    {
        parent::__construct($context);
        $this->_cronologia = $cronologiaFactory;
    }

    public function loadData(){
        $rowId = (int) $this->getRequest()->getParam('id');
        if ($rowId) {
            $rowData = $this->_cronologia->create()->load($rowId)->getData();
            $this->_tipo = $rowData['tipo'];
            $this->_contenuto = json_decode($rowData['contenuto'], true);
        }
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function setTipo($newTipo){
        $this->_tipo = $newTipo;
    }

    public function getTipo(){
        return $this->_tipo;
    }

    public function setContenuto($newContenuto){
        $this->_contenuto = $newContenuto;
    }

    public function getContenuto(){
        return $this->_contenuto;
    }
}