<?php

namespace Frame\Quick\Model\Config\Source;

class OrderNumber implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory) {
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr =  [
            ['value' => 'marketplace', 'label' => __('Marketplace Order ID')],
            ['value' => 'magento', 'label' => __('Magento Order ID')],
        ];
        ksort($arr);
        return $arr;
    }
}
