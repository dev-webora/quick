<?php

  namespace Frame\Quick\Controller\Adminhtml\Home;

  class ImportIncremental extends \Magento\Backend\App\Action
  {
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	protected $resultPageFactory;
	protected $_importer;
	protected $_coreRegistry;
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	protected $dp;
    //helper funciotns
    protected $_helpers;

      /**
       * Constructor
       *
       * @param \Magento\Backend\App\Action\Context $context
       * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
       * @param \Magento\Framework\Registry $coreRegistry
       * @param \Frame\Quick\Helper\Data $helpers
       * @param \Frame\Quick\Cron\ImportIncremental $importer
       */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $coreRegistry,
        \Frame\Quick\Helper\Data $helpers,
		\Frame\Quick\Cron\ImportIncremental $importer
		
	) {
		 parent::__construct($context);
		 $this->resultPageFactory = $resultPageFactory;
		 $this->_importer = $importer;
		 $this->_coreRegistry = $coreRegistry;
         //helpers
         $this->_helpers = $helpers;

	}

	/**
	 * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$this->_importer->execute();
        return  $resultPage = $this->resultPageFactory->create();
	}
  }
?>