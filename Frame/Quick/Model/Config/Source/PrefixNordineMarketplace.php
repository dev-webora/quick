<?php


namespace Frame\Quick\Model\Config\Source;


class PrefixNordineMarketplace {

    public function toOptionArray()
    {
        $arr =  [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'customer_group', 'label' => __('Customer Group ID')],
            ['value' => 'shipping', 'label' => __('Fetch by Shipping Type')],
        ];
        return $arr;
    }
}