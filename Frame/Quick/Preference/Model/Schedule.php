<?php


namespace Frame\Quick\Preference\Model;


use Magento\Cron\Model\DeadlockRetrierInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Intl\DateTimeFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Schedule extends \Magento\Cron\Model\Schedule
{

    /**
     * @var TimezoneInterface
     */
    protected $timezoneConverter;

    /**
     * @var DateTimeFactory
     */
    protected $dateTimeFactory;

    /**
     * @var DeadlockRetrierInterface
     */
    protected $retrier;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [],
        TimezoneInterface $timezoneConverter = null,
        DateTimeFactory $dateTimeFactory = null,
        DeadlockRetrierInterface $retrier = null
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data, $timezoneConverter, $dateTimeFactory, $retrier);
        $this->timezoneConverter = $timezoneConverter ?: ObjectManager::getInstance()->get(TimezoneInterface::class);
        $this->dateTimeFactory = $dateTimeFactory ?: ObjectManager::getInstance()->get(DateTimeFactory::class);
        $this->retrier = $retrier ?: ObjectManager::getInstance()->get(DeadlockRetrierInterface::class);
    }


    /**
     * Sets a job to STATUS_RUNNING only if it is currently in STATUS_PENDING.
     *
     * Returns true if status was changed and false otherwise.
     *
     * @return boolean
     */
    public function tryLockJob()
    {
        /** @var \Magento\Cron\Model\ResourceModel\Schedule $scheduleResource */
        $scheduleResource = $this->_getResource();

        if (!in_array($this->getJobCode(), ['quick_light_import', 'quick_heavy_import'])) {
            // Change statuses from running to error for terminated jobs
            $this->retrier->execute(
                function () use ($scheduleResource) {
                    return $scheduleResource->getConnection()->update(
                        $scheduleResource->getTable('cron_schedule'),
                        ['status' => self::STATUS_ERROR],
                        ['job_code = ?' => $this->getJobCode(), 'status = ?' => self::STATUS_RUNNING]
                    );
                },
                $scheduleResource->getConnection()
            );
        }


        // Change status from pending to running for ran jobs
        $result = $this->retrier->execute(
            function () use ($scheduleResource) {
                return $scheduleResource->trySetJobStatusAtomic(
                    $this->getId(),
                    self::STATUS_RUNNING,
                    self::STATUS_PENDING
                );
            },
            $scheduleResource->getConnection()
        );

        if ($result) {
            $this->setStatus(self::STATUS_RUNNING);
            return true;
        }
        return false;
    }

}
