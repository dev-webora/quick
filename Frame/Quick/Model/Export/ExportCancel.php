<?php
namespace Frame\Quick\Model\Export;

use Frame\Quick\Model\ExportOrderQueueFactory;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection as OptionCollection;
use Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\App\Response\Http\FileFactory;
use \Magento\Framework\App\Response\RedirectInterface;
use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Magento\Backend\App\Action\Context;
use \Magento\Ui\Component\MassAction\Filter;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Directory\Model\RegionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Message\ManagerInterface;

class ExportCancel extends Export
{

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        \Frame\Quick\Helper\Data $helpers,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ExportOrderQueueFactory $exportOrderQueue,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
    )
    {
        parent::__construct($context,
            $collectionFactory,
            $helpers,
            $cronologiaFactory,
            $messageManager,
            $redirect,
            $productRepository,
            $exportOrderQueue,
            $groupRepository,
            $resourceConnection,
            $orderModel,
            $moduleManager,
            $objectManager);
    }
    /* (vedi slack quickm2)
     * 1. manda stato A per l’Nordine in ordineweb
     * 2. manda stato A sull’nordine e codice_art in detordiniweb
     * 3. manda stato A sull’norrdine e CODICE_ART":"SPESE TRASPORTO" in detordiniweb
     */
    public function cancel($orderIdsList)
    {
        parent::elaborateOrderQueue();
        parent::generateQuickOrder($orderIdsList, parent::TYPE_CANCEL);
        $this->updateCronologia("Ordine cancellato con Successo!");
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }

}
