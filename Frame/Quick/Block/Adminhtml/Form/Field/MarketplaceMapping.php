<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

class MarketplaceMapping extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{

    protected $_activation;


    protected function _getActivationRenderer()
    {
        if (!$this->_activation) {
            $this->_activation = $this->getLayout()->createBlock(
                '\Frame\Quick\Block\Adminhtml\Form\Field\MarketplaceAttribute',
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->_activation;
    }

    protected function _prepareToRender()
    {
        $this->addColumn('marketplace_value', ['label' => __('Valore campo Marketplace'), 'style' => 'width: 150px']);
        $this->addColumn(
            'marketplace_magento_attribute',
            [
                'label' => __('Attributo Magento'),
                'renderer' => $this->_getActivationRenderer()
            ]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * Prepare existing row data object.
     *
     * @param \Magento\Framework\DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $customAttribute = $row->getData('marketplace_magento_attribute');

        $key = 'option_' . $this->_getActivationRenderer()->calcOptionHash($customAttribute);
        $options[$key] = 'selected="selected"';
        $row->setData('option_extra_attrs', $options);
    }
}