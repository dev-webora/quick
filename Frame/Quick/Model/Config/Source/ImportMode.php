<?php

namespace Frame\Quick\Model\Config\Source;

class ImportMode implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory $attributeFactory)
    {
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr = [
        [
            'value' => 'json_file',
            'label' => 'json file',
        ],
        [
            'value' => 'csv_file',
            'label' => 'csv file',
        ],
        [
            'value' => 'csv_api',
            'label' => 'Csv api',
        ],
        [
            'value' => 'json_api',
            'label' => 'Json api',
        ]];
        return $arr;
    }
}
