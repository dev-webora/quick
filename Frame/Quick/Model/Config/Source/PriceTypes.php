<?php
namespace Frame\Quick\Model\Config\Source;

class PriceTypes implements \Magento\Framework\Option\ArrayInterface
{
    private $productRepository;
    private $scopeConfig;
    private $taxCalculation;

    public function __construct(\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Tax\Api\TaxCalculationInterface $taxCalculation) {
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->taxCalculation = $taxCalculation;
    }

    //https://gielberkers.com/get-product-price-including-excluding-tax-magento-2/
    //https://magento.stackexchange.com/questions/208496/magento-2-get-product-price-including-tax
    public function toOptionArray()
    {
        $arr =  [
            ['value' => 'price_incl_tax', 'label' => __('Price Including Tax')],
            ['value' => 'base_price_incl_tax', 'label' => __('Base Price Excluding Tax')],
            ['value' => 'price', 'label' => __('Price')],
            ['value' => 'base_price', 'label' => __('Base Price')]
        ];
        ksort($arr);
        return $arr;
    }
}