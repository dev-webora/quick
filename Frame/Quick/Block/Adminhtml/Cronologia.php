<?php
namespace Frame\Quick\Block\Adminhtml;

use \Magento\Backend\Block\Widget\Grid\Container;

class Cronologia extends Container {

    protected $_controller;
    protected $_blockGroup;
    protected $_headerText;
    protected $_addButtonLabel;

    protected function _construct()
    {
        $this->_controller = 'adminhtml_cronologia';
        $this->_blockGroup = 'quickimport';
        $this->_headerText = __('Cronologia');
        //$this->_addButtonLabel = __('Aggiungi');
        parent::_construct();
        $this->buttonList->remove('add'); //to remove add button
    }
}