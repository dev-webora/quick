<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class IncrementalTypeFields implements \Magento\Framework\Option\ArrayInterface
{


    public function __construct() {

    }

    public function toOptionArray()
    {
        $arr = array();

        $arr[] = ['label' => "Importa prezzi e quantità" , 'value' => "stockprice"];
        $arr[] = ['label' => "Importa tutti i campi" , 'value' => "all"];
        $arr[] = ['label' => "Solo campi selezionati" , 'value' => "selectedfields"];

        return $arr;
    }
}
