<?php

  namespace Frame\Quick\Controller\Adminhtml\Home;
  //include dirname(__FILE__) . '/../../../Model/Import/Product.php';
  // use \Frame\Quick\Model\Import\Product;

  class Import extends \Magento\Backend\App\Action
  {
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	protected $resultPageFactory;
	protected $_importer;
	protected $_coreRegistry;
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	protected $dp;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Frame\Quick\Model\Import\Product $importer,
		//Product $importer,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $coreRegistry/*,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRep, 
		\Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
		\Magento\Catalog\Model\ResourceModel\Product $productResource*/
		//\Frame\Quick\Model\Import\Product $products_importer
	) {
		 parent::__construct($context);
		 $this->resultPageFactory = $resultPageFactory;
		 //$this->_products_importer = new Product($productRep,$productFactory,$productResource);
		 $this->_importer = $importer;
		 $this->_coreRegistry = $coreRegistry;
		/* $this->_productRep = $productRep;
		 $this->_productFactory = $productFactory;
		 $this->_productResource = $productResource;*/
		 
	}

	/**
	 * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		
		//$ch = curl_init("http://commerciale.teknosis.link:444/TABELLA?NOME=INVENTORY_DAY");
		$ch = curl_init("http://apifantasia.teknosis.link:4567/table?safekey=frame&nome=frame_inventory_all&first=20");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ordineweb));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TRANSFERTEXT, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($ordineweb))));
		$token = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		//var_dump($token);
		$json = $token;
		$json = preg_replace('/[[:cntrl:]]/','',$json);
		//print_r($json);
		$products = json_decode($json);
		//$products = json_decode(urldecode($token));
		//var_dump($products);
		//echo "<br><br>Product";
		//print_r( $products[0]->ID_VARIANTE );
		
		/*try {
		$product = $this->_productRep->get($products[0]->ID_VARIANTE);
		$isNew = false;
		} catch (NoSuchEntityException $e) {
				$product = $this->_productFactory->create();
				$isNew = true;
		}
		
		$product->setSku($products[0]->ID_VARIANTE);
		$productResource->saveAttribute($product, 'sku');
		$description = "Description TEST"; 
		$product->setDescription($description);
		$productResource->saveAttribute($product, 'description');
		
		
		//$products = array();*/
		$res = $this->_importer->runImport($products,false); //false - force import totale disabilitato 
			//$res = "Product Imported: => ".$products[0]->ID_VARIANTE;
		$this->_coreRegistry->register('res', $res);
	
		
		 return  $resultPage = $this->resultPageFactory->create();
	}
  }
?>