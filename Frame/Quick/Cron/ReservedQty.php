<?php

namespace Frame\Quick\Cron;

use Frame\Quick\Model\ReservedQtyFactory;
use Frame\Quick\Model\Export\ExportReservedQty;

class ReservedQty
{
    const STATE_UNKNOWN  = 0;
    const STATE_PLACED   = 1;
    const STATE_RELEASED = 2;
    const STATE_CANCELED = 3;

    protected $_helpers;
    protected $_reservedFactory;
    private $_resource;
    private $_connection;
    private $_export_reserved_qty;
    private $_product_repo;
    private $_mis_map;
    private $_active;
    private $_idsuper_map;

    public function __construct(
        \Frame\Quick\Helper\Data $helpers,
        ExportReservedQty $exportReservedQty,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ReservedQtyFactory $reservedFactory
    )
    {
        $this->_helpers = $helpers;
        $this->_reservedFactory = $reservedFactory;
        $this->_export_reserved_qty = $exportReservedQty;
        $this->_product_repo = $productRepository;
        $this->_mis_map = $this->_helpers->getModuleConfig("export","mis_map");
        $this->_idsuper_map = $this->_helpers->getModuleConfig("export","idsuper_map");
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $this->_connection = $this->_resource->getConnection();
        $this->_active = $this->_helpers->getModuleConfig("m2epro","enable_reserved_qty");
    }

    public function execute(\Magento\Cron\Model\Schedule $schedule = null)
    {
        if(!$this->_active)
            return false;

        $reservedItems = $this->_reservedFactory->create();
        $collection = $reservedItems->getCollection()
            ->addFieldToFilter('processed', ['in' => array(0, 1)]);
        $quantita_riservate = array();
        foreach($collection as $item){

            $quantita_riservate = array();
            $id_ordine_marketplace = $item['marketplace_order_id'];
            $id_ordine_m2epro = $item['m2epro_order_id'];
            $tipo_marketplace = $item['marketplace'];

            if ($item['processed']==1 && $item["reservation_state"]==self::STATE_PLACED) {
                continue;
            }


            if($item['processed']==0 && $item["reservation_state"]==self::STATE_PLACED){
                if($tipo_marketplace == "amazon") {
                    $query = "SELECT `amazon_order_id`, `amazon_order_item_id`, `sku`, `price`, `qty_purchased` ".
                        "FROM `m2epro_amazon_order_item` as item INNER JOIN `m2epro_amazon_order` as ordermarket ON item.order_item_id IN (SELECT `id` FROM `m2epro_order_item` WHERE `order_id` = '$id_ordine_m2epro') and ordermarket.order_id = '$id_ordine_m2epro'";
                } else {
                    $query = "SELECT `ebay_order_id`, `order_item_id`, `sku`, `price`, `qty_purchased`, `variation_details` ".
                        "FROM `m2epro_ebay_order_item` as item INNER JOIN `m2epro_ebay_order` as ordermarket ON item.order_item_id IN (SELECT `id` FROM `m2epro_order_item` WHERE `order_id` = '$id_ordine_m2epro') and ordermarket.order_id = '$id_ordine_m2epro'";
                }
                $result = $this->_connection->fetchAll($query);
                foreach ($result as $order_marketplace){
                    $order_item_id = $this->getOrderItemId($order_marketplace, $tipo_marketplace);
                    if($tipo_marketplace == "amazon"){
                        $sku = $order_marketplace['sku'];
                        $prod = $this->_product_repo->get($sku);
                        $id_super = $prod->getData($this->_idsuper_map);
                        $size = $prod->getAttributeText($this->_mis_map);
                    } else {
                        $variation_details = $order_marketplace["variation_details"];
                        $simple_detail = json_decode($variation_details, true);
                        $id_super = $order_marketplace['sku'];
                        $sku = $simple_detail["sku"];
                        $prod = $this->_product_repo->get($sku);
                        $size = $prod->getAttributeText($this->_mis_map);
                    }
                    $qty = $order_marketplace['qty_purchased'];
                    $price = $order_marketplace['price'];
                    $quantita_riservate[] = array(
                        "marketplace_order_id" => $order_item_id,
                        "sku" => strtoupper($sku),
                        "id_super" => strtoupper($id_super),
                        "size" => $size,
                        "qty" => $qty,
                        "price" => $price,
                        "status"=> self::STATE_PLACED
                    );
                }
                //aggiorna frame_quick_reserved_qty
                $item->setData("marketplace_order_id", $order_item_id);
                $item->setData("sku", $sku);
                if($tipo_marketplace == "ebay")
                    $item->setData("product_details", $variation_details);
                $item->setData("qty_reserved", $qty);
                $item->setData("price", $price);
                $item->setData("processed", 1);
                $item->save();
            } else if($item['processed']==1 && $item["reservation_state"]==self::STATE_CANCELED){
                $sku = $item['sku'];
                $prod = $this->_product_repo->get($sku);
                $id_super = $prod->getData("codice_quick");
                $size = $prod->getAttributeText('size');
                $qty = $item['qty_reserved'];
                $price = $item['price'];
                $quantita_riservate[] = array(
                    "marketplace_order_id" => $id_ordine_marketplace,
                    "sku" => strtoupper($sku),
                    "id_super" => strtoupper($id_super),
                    "size" => $size,
                    "qty" => $qty,
                    "price" => $price,
                    "status"=> self::STATE_CANCELED
                );
                $item->setData("processed", 2);
                $item->save();
            } else if($item['processed']==1 && $item["reservation_state"]==self::STATE_RELEASED){
                //in caso di NORDINE = ID MAGENTO ORDER
                //effettuare la cancellazione dell'ordine con NORDINE = ORDER MARKETPLACE
                //Creato dall'ordine effettuato dalla reserved qty
            }
            if (!empty($quantita_riservate)) {


                $this->_export_reserved_qty->export($quantita_riservate);

            }
        }
        return $this;
    }

    /**
     * @param $order_marketplace
     * @param $tipo_marketplace
     * @return string
     */
    protected function getOrderItemId($order_marketplace, $tipo_marketplace): string
    {
        $prefix = $this->_helpers->getModuleConfig('m2epro', "{$tipo_marketplace}_reserve_prefix");
        if (!empty($prefix)) {
            $prefix = $prefix . '_';
            $order_item_id = $prefix . $order_marketplace[$tipo_marketplace."_order_id"];
        } else {
            $order_item_id = $order_marketplace[$tipo_marketplace."_order_id"];
        }
        return $order_item_id;
    }

}