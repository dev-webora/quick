<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

use \Frame\Quick\Model\Config\Source\Fields;

class OrderImage extends \Magento\Framework\View\Element\Html\Select
{
    protected $_enableDisable;
    protected $_attributes_model;
    protected $_helpers;
    protected $_serialize;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Config\Model\Config\Source\Enabledisable $enableDisable,
        \Frame\Quick\Helper\Data $helpers,
        //json serialize
        \Magento\Framework\Serialize\Serializer\Json $serialize,
        Fields $attributes_model,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_enableDisable = $enableDisable;
        $this->_attributes_model = $attributes_model;
        //helpers
        $this->_helpers = $helpers;
        $this->_serialize = $serialize;

    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function _toHtml()
    {
        if (!$this->getOptions()) {

            $levels = array();
            for ($i = 1; $i < 11; $i++) {
                $levels[$i] = "$i";
            }

            foreach ($levels as $code => $label) {
                $this->addOption($code, $label);
            }
        }
        $this->setExtraParams('style="width:150px;"');

        return parent::_toHtml();
    }
}