<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

use \Frame\Quick\Model\Config\Source\Fields;

class ActivationUpdate extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * Model Enabledisable
     *
     * @var \Magento\Config\Model\Config\Source\Enabledisable
     */
    protected $_enableDisable;
	protected $_attributes_model;

    /**
     * Activation constructor.
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Config\Model\Config\Source\Enabledisable $enableDisable $enableDisable
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Config\Model\Config\Source\Enabledisable $enableDisable,
		array $data = []
    ) {
        parent::__construct($context, $data);

        $this->_enableDisable = $enableDisable;
    }

    /**
     * @param string $value
     * @return Magently\Tutorial\Block\Adminhtml\Form\Field\Activation
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Parse to html.
     *
     * @return mixed
     */
    public function _toHtml()
    {
		
        if (!$this->getOptions()) {
            $attributes = $this->_enableDisable->toOptionArray();
			//$params = array();
			//$params = 'style="max-width:90%"';
            foreach ($attributes as $attribute) {
                $this->addOption($attribute['value'], $attribute['label']);
            }
			/*foreach ($attributes as $code => $label) {
                $this->addOption($code, $label);
            }*/
        }

        return parent::_toHtml();
    }
}