<?php 
namespace Frame\Quick\Setup;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface{

    protected $triggerFactory;

    public function __construct(
        \Magento\Framework\DB\Ddl\TriggerFactory $triggerFactory
    )
    {
        $this->triggerFactory = $triggerFactory;
    }

	public function upgrade(SchemaSetupInterface $setup,ModuleContextInterface $context){
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->upgradeTov101($setup);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->upgradeTov102($setup);
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->upgradeTov103($setup);
        }if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->upgradeTov104($setup);
        }
        $setup->endSetup();
	}

	private function upgradeTov101($setup) {
        if($setup->tableExists('frame_quick_logger'))
            return;
        $table = $setup->getConnection()->newTable(
            $setup->getTable('frame_quick_logger')
        )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'ID'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'Product Sku'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addColumn(
                'qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Imported Quantity'
            )->addColumn(
                'price',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Imported Price'
            )->addColumn(
                'altro',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [],
                'altro'
            )->setComment('Tabella Logger Modulo Quick');
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
            $setup->getTable('frame_quick_logger'),
            $setup->getIdxName(
                $setup->getTable('frame_quick_logger'),
                ['sku', 'price', 'altro'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['sku', 'price', 'altro'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
    }

    private function upgradeTov102($setup) {
        if($setup->tableExists('frame_quick_reserved_qty'))
            return;
        $table = $setup->getConnection()->newTable(
            $setup->getTable('frame_quick_reserved_qty')
        )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'ID'
            )->addColumn(
                'marketplace_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Marketplace Order ID'
            )
            ->addColumn(
                'm2epro_order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'M2ePro Order ID'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Product SKU'
            )->addColumn(
                'product_details',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Product Details'
            )->addColumn(
                'marketplace',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Marketplace'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addColumn(
                'qty_reserved',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Reserved Quantity'
            )->addColumn(
                'price',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Imported Price'
            )->addColumn(
                'reservation_state',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Reserve Status'
            )->addColumn(
                'processed',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => 0],
                '0 => not processed 1=>Qty Send 2=>Cancelled/Order Complete'
            )->setComment('Tabella Quantita Riservata Modulo Quick');
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
            $setup->getTable('frame_quick_reserved_qty'),
            $setup->getIdxName(
                $setup->getTable('frame_quick_reserved_qty'),
                ['sku', 'marketplace_order_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['sku', 'marketplace_order_id'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
    }

    private function upgradeTov103($setup) {
        //creazione trigger
        $triggerAfterInsert = 'after_m2epro_order';
        $triggerAfterUpdate = 'after_m2epro_order_update';
        $eventAfterInsert = \Magento\Framework\DB\Ddl\Trigger::EVENT_INSERT;
        $eventAfterUpdate = \Magento\Framework\DB\Ddl\Trigger::EVENT_UPDATE;

        $triggerAfterInsert = $this->triggerFactory->create()
            ->setName($triggerAfterInsert)
            ->setTime(\Magento\Framework\DB\Ddl\Trigger::TIME_AFTER)
            ->setEvent($eventAfterInsert)
            ->setTable($setup->getTable('m2epro_order'));

        $triggerAfterInsert->addStatement($this->buildTriggerM2e($eventAfterInsert));
        $setup->getConnection()->dropTrigger($triggerAfterInsert->getName());
        $setup->getConnection()->createTrigger($triggerAfterInsert);

        $triggerAfterUpdate = $this->triggerFactory->create()
            ->setName($triggerAfterUpdate)
            ->setTime(\Magento\Framework\DB\Ddl\Trigger::TIME_AFTER)
            ->setEvent($eventAfterUpdate)
            ->setTable($setup->getTable('m2epro_order'));

        $triggerAfterUpdate->addStatement($this->buildTriggerM2e($eventAfterUpdate));
        $setup->getConnection()->dropTrigger($triggerAfterUpdate->getName());
        $setup->getConnection()->createTrigger($triggerAfterUpdate);
    }

    private function buildTriggerM2e($event)
    {
        switch ($event) {
            case \Magento\Framework\DB\Ddl\Trigger::EVENT_INSERT:
                $triggerSql = "INSERT INTO `frame_quick_reserved_qty`(`m2epro_order_id`, `reservation_state`, `marketplace`) ".
                    "VALUES (NEW.id, NEW.reservation_state, NEW.component_mode)";
                return $triggerSql;
            case \Magento\Framework\DB\Ddl\Trigger::EVENT_UPDATE:
                $triggerSql = "UPDATE `frame_quick_reserved_qty` SET `reservation_state` = NEW.reservation_state ".
                    "WHERE `m2epro_order_id` = NEW.id";
                return $triggerSql;
            default:
                return '';
        }
    }

    private function upgradeTov104($setup) {
        if($setup->tableExists('frame_quick_cronologia'))
            return;
        $table = $setup->getConnection()->newTable(
            $setup->getTable('frame_quick_cronologia')
        )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'ID'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Updated At'
            )
            ->addColumn(
                'tipo',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Tipologia Cronologia: import o export'
            )->addColumn(
                'contenuto',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                512,
                [],
                'Imported Price'
            )
            ->setComment('Tabella Log informazioni ed errori Modulo di Quick');
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->addIndex(
            $setup->getTable('frame_quick_cronologia'),
            $setup->getIdxName(
                $setup->getTable('frame_quick_cronologia'),
                ['tipo', 'contenuto'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['tipo', 'contenuto'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
    }
	
    /**
     * @param SchemaSetupInterface $setup
     * @return void
     */
    private function addStatus(SchemaSetupInterface $setup)
    {
        /*
        $setup->getConnection()->addColumn(
            $setup->getTable('data_example'),
            'status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'nullable' => true,
                'default' => 1,
                'comment' => 'Status'
            ]
        );*/
    }
}