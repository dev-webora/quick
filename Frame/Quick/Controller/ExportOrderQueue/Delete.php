<?php

namespace Frame\Quick\Controller\ExportOrderQueue;

use Frame\Quick\Model\ExportOrderQueueFactory;

class Delete extends \Magento\Framework\App\Action\Action {

    private $_exportOrderFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,   
        ExportOrderQueueFactory $exportOrderFactory
    ) {
        $this->_exportOrderFactory = $exportOrderFactory;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    public function execute() {
        $id = $this->getRequest()->getParam('queue_id');
        try {
            $model = $this->_exportOrderFactory->create();
            $model->load($id);
            $model->delete();
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

}
