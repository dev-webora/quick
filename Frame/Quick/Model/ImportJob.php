<?php
/**
 * Created by PhpStorm.
 * User: alessandro
 * Date: 2021-02-09
 * Time: 12:53
 */

namespace Frame\Quick\Model;

use Frame\Quick\Cron\Import;
use Frame\Quick\Helper\Data;

class ImportJob extends Import
{
    protected $_cacheTypeList;
    protected $_coreRegistry;
    protected $_source_type;
    protected $_magmi_mode;
    protected $_incremental_type_fields;
    protected $_inventory_only;
    protected $_selected_functions;
    /**
     * @var Data
     */
    protected $_quickHelper;



    public function __construct(
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Frame\Quick\Model\Import\Product $importer,
        \Frame\Quick\Model\CronologiaFactory $cronologiaFactory,
        Data $helpers,
        \Magento\Framework\Registry $registry,
        Data $quickHelper,
        \Frame\Quick\Model\CronologiaFactory $cronologia
    ) {
        parent::__construct($importer, $cronologiaFactory, $helpers);
        $this->_source_type =  $this->_helpers->getModuleConfig("general", "import_type");
        $this->_magmi_mode = $this->_helpers->getModuleConfig("general", "incremental_type_magmi");
        $this->_incremental_type_fields = $this->_helpers->getModuleConfig("general", "incremental_type_fields");
        if ($this->_incremental_type_fields == "stockprice") {
            $this->_magmi_mode = "update";
            $this->_inventory_only = true;
        } else {
            $this->_inventory_only = false;
        }

        if ($this->_incremental_type_fields == "selectedfields") {
            $selected_functions_config = $this->_helpers->getModuleConfig("general", "functions");
            $this->_selected_functions = explode(',', $selected_functions_config);
        } else {
            $this->_selected_functions = [];
        }

        $this->_coreRegistry = $registry;
        $this->_cacheTypeList = $cacheTypeList;

        $this->_quickHelper = $quickHelper;

        $this->_cronologia = $cronologia;
    }

    /**
     * @param \Magento\Cron\Model\Schedule $schedule
     * @param bool $is_totale
     */
    protected function execute(\Magento\Cron\Model\Schedule $schedule = null, $is_totale = false)
    {
    }

    /**
     * @param $api_url_final
     * @param string $season
     * @param string $gender
     * @param bool|int $cronologia_id
     * @return void
     * @throws \Exception
     */
    public function loadAndImportProducts($api_url_final, $season = '', $gender = '', $cronologia_id = false)
    {
        /**
         *
         * $api_url_final =
         *  [
         *    0 => [ 'url' => reference ]
         *	  1 => [ 'url' => inventory ]
         *  ]
         */

        if ($cronologia_id) {
            $this->_cronologia_id = $cronologia_id;
        }

        $import_info = [];
        $import_info["functions"] = $this->_selected_functions;
        $import_info["type"] = $this->_incremental_type_fields;

        $importMode = $this->_helpers->getModuleConfig("api", "import_mode");


        switch ($importMode) {
            case Data::IMPORT_MODE_CSV_FILE:
            case Data::IMPORT_MODE_JSON_FILE:
                $products= [];
                $products = $this->extractProductsFromMultiTable($products, true, $this->_inventory_only);
//                $import_info["type"] = "stockprice";
                break;
            case Data::IMPORT_MODE_JSON_API:
            case Data::IMPORT_MODE_CSV_API:
                $products = $this->runApiCall($api_url_final, $import_info);
                if ($this->_source_type == "multitable") {
                    $products = $this->extractProductsFromMultiTable($products, false, $this->_inventory_only);
                }
                break;

        }

        $data_inizio = date("Y-m-d H:i:s");
        //fwrite($f, "[$data_inizio] - importing\n");

        if (!$products) {
            //fwrite($f, "[$data_inizio] no products to import - it might be an error in getting products from Quickweb\n");
            //fclose($f);
            throw new \Exception("No products to import");
        }

        $this->updateCronologia("Inizio Importazione di  " . count($products) . " prodotti - stagione $season, sesso $gender");

        /*if($this->_source_type == "singletable")
            $mode = "create";
        else
            $mode = "update";*/

        $res = $this->_importer->runImport($products, $this->_magmi_mode, $this->_cronologia_id, $import_info);

        if ($this->_coreRegistry->registry('res')) {
            $this->_coreRegistry->unregister('res');
        }

        $this->_coreRegistry->register('res', "Light Importer. " . $res);

        if ($this->_last_time_custom_query && $this->_last_time_custom_query != 0) {
            $this->_helpers->setModuleConfig("custom_query", "last_time", 0);
        }
    }
}
