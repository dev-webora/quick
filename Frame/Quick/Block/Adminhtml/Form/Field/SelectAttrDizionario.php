<?php

namespace Frame\Quick\Block\Adminhtml\Form\Field;

use \Frame\Quick\Model\Config\Source\Fields;
use Magento\Setup\Exception;

class SelectAttrDizionario extends \Magento\Framework\View\Element\Html\Select
{
    /**
     * Model Enabledisable
     *
     * @var \Magento\Config\Model\Config\Source\Enabledisable
     */
    protected $_enableDisable;
    protected $_attributes_model;
    protected $_helpers;
    protected $_serialize;
    private $_unserialized_map;
    private $_attributeFactory;

    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Magento\Config\Model\Config\Source\Enabledisable $enableDisable,
        \Frame\Quick\Helper\Data $helpers,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        //json serialize
        \Magento\Framework\Serialize\Serializer\Json $serialize,
        Fields $attributes_model,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_enableDisable = $enableDisable;
        $this->_attributes_model = $attributes_model;
        $this->_attributeFactory = $attributeFactory;
        //helpers
        $this->_helpers = $helpers;
        $this->_serialize = $serialize;
        $map_configs = $this->_helpers->getMappingsConfig();
        try {
            $this->_unserialized_map = $this->_serialize->unserialize($map_configs);
        } catch (\Exception $e) {
            $this->_unserialized_map = NULL;
        }
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $attributi_mappati = array();
            $arr = array();
            if ($this->_unserialized_map) {
                foreach ($this->_unserialized_map as $key => $attributes) {
                    $attributi_mappati[] = $attributes['activation_attribute'];
                }

                $attributeInfo = $this->_attributeFactory->getCollection();
                foreach ($attributeInfo as $attributes) {
                    $attributeId = $attributes->getAttributeCode();
                    if (in_array($attributeId, $attributi_mappati)) {
                        $attributeName = $attributes->getStoreLabel();
                        //$arr[] = ['label' => $attributeName, 'value' => $attributeId];
                        $this->addOption($attributeId, $attributeName);
                    }
                }
            }
        }
        $this->setExtraParams('style="width:150px;"');
        return parent::_toHtml();
    }
}