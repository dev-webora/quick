<?php
namespace Frame\Quick\Model\Config\Source;

class QuickAttributes implements \Magento\Framework\Option\ArrayInterface
{
    private $_attributeFactory;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory) {
        $this->_attributeFactory = $attributeFactory;
    }

    public function toOptionArray()
    {
        $arr = array();
        $attributeInfo = $this->_attributeFactory->getCollection();
        foreach($attributeInfo as $attributes)
        {
            $attributeId = $attributes->getAttributeCode();
            $arr[$attributeId] = $attributeId;
        }
        ksort($arr);
        return $arr;
    }
}
