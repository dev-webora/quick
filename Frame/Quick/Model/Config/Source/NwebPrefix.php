<?php


namespace Frame\Quick\Model\Config\Source;


class NwebPrefix
{

    public function toOptionArray()
    {
        $arr =  [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'customer_group', 'label' => __('Customer Group ID')],
            ['value' => 'shipping', 'label' => __('Fetch by Shipping Type')],
            ['value' => 'text', 'label' => __('CustomText')],
        ];
        return $arr;
    }
}