<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Frame\Quick\Model\Config\Source;

/**
 * Source model for element with enable and disable variants.
 * @api
 * @since 100.0.2
 */
class MagentoCategories implements \Magento\Framework\Option\ArrayInterface
{
	protected $_categoryFactory;

	public function __construct(
		// \Magento\Framework\View\Element\Context $context,
         \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    ) {
		//parent::__construct($context);
        $this->_categoryFactory = $categoryCollectionFactory;
    }
	 
    public function toOptionArray()
    {
		$arr = [];
        $collection = $this->_categoryFactory->create();
        $collection->addAttributeToSelect('name');
        $level = 3;
        $collection->addLevelFilter($level);

        foreach ($collection as $category) {
            //$arr[$category->getName()] = $category->getName();
            $path = $category->getPathIds();
            $categories_from_path = $this->_categoryFactory->create()
                ->addAttributeToSelect('name')
                ->addFieldToFilter(
                    'entity_id',
                    ['in' => $path]
                );
            $path_array = array();
            foreach ($categories_from_path as $cat){
                if($cat->getId() != 1 and $cat->getId() != 2)
                    $path_array[] = $cat->getName();
            }
            $path_str = implode(">",$path_array);
            //$arr[$category->getName()] = $path_str;
            $arr[] = ['label' => $path_str, 'value' => $path_str];
        }
		ksort($arr);
		return $arr;
		
    }
}
