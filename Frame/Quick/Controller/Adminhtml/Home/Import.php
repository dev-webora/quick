<?php

  namespace Frame\Quick\Controller\Adminhtml\Home;
  //include dirname(__FILE__) . '/../../../Model/Import/Product.php';
  // use \Frame\Quick\Model\Import\Product;

  class Import extends \Magento\Backend\App\Action
  {
	/**
	* @var \Magento\Framework\View\Result\PageFactory
	*/
	protected $resultPageFactory;
	protected $_importer;
	protected $_coreRegistry;
	protected $_productRep;
	protected $_productFactory;
	protected $_productResource;
	protected $dp;
	protected $incremental;
    //helper funciotns
    protected $_helpers;

	/**
	 * Constructor
	 *
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Frame\Quick\Model\Import\Product $importer,
		//Product $importer,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\Registry $coreRegistry,
        //Helper functions
        \Frame\Quick\Helper\Data $helpers,
		\Frame\Quick\Cron\ImportIncremental $incremental
		
	) {
		 parent::__construct($context);
		 $this->resultPageFactory = $resultPageFactory;
		 //$this->_products_importer = new Product($productRep,$productFactory,$productResource);
		 $this->_importer = $importer;
		 $this->_coreRegistry = $coreRegistry;
		 $this->incremental = $incremental;
         //helpers
         $this->_helpers = $helpers;

	}

	/**
	 * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
	 *
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$this->incremental->execute();
        return  $resultPage = $this->resultPageFactory->create();
	}
  }
?>