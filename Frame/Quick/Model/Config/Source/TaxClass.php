<?php


namespace Frame\Quick\Model\Config\Source;

use \Magento\Tax\Model\TaxClass\Source\Product as ProductTaxClassSource;

class TaxClass
{
    protected $productTaxClassSource;


    public function __construct(ProductTaxClassSource $productTaxClassSource) {
        $this->productTaxClassSource = $productTaxClassSource;
    }

    public function toOptionArray()
    {
        var_dump($this->productTaxClassSource->getAllOptions());
        return array();
    }

}